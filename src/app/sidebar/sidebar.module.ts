import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar.component';
import { TranslateLoader, TranslateModule,} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient} from '@angular/common/http';
// AoT requires an exported function for factories

export function childLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "./assets/i18n/", ".json");
}

@NgModule({
    imports: [ RouterModule, CommonModule,
        TranslateModule.forChild({
            loader: {
              provide: TranslateLoader,
              useFactory: childLoaderFactory,
              deps: [HttpClient]
            },
            extend: true,
          }),
    
    ],
    declarations: [ SidebarComponent ],
    exports: [ SidebarComponent ]
})

export class SidebarModule {}
