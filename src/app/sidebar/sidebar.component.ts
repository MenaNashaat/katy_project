import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import {Title} from "@angular/platform-browser";
import { GeneralInfoService   } from '../services';

export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard',     title: 'Dashboard',         icon:'fas fa-th-large',       class: '' },
    { path: '/registration',         title: 'registration',             icon:'fas fa-user',    class: '' },
     { path: '/contact',     title: 'contact',         icon:'fas fa-file-signature',       class: '' },
    { path: '/branch',     title: 'branch',         icon:'fas fa-code-branch',       class: '' },
    // { path: '/dashboard',     title: 'Request',         icon:'fas fa-th-large',       class: '' },
    // { path: '/dashboard',     title: 'Courses',         icon:'fas fa-th-large',       class: '' },
    // { path: '/dashboard',     title: 'Help',         icon:'fas fa-th-large',       class: '' },
    // { path: '/dashboard',     title: 'Setting',         icon:'fas fa-th-large',       class: '' },


];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    contact:string;
    color: any;
    str: any;
    res: any;
    tabLogo: any;
    title: any;
    favIcon: HTMLLinkElement = document.querySelector('#appIcon');
    
    constructor(
        private translate :TranslateService, 
        private GeneralInfoService:GeneralInfoService, 
        private titleService:Title,
      ) {
      }
    ngOnInit() {
        this.translate.get('backend.world').subscribe((text:string) => this.contact = text);
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        this.getGeneralInfo()
        this.GeneralInfoService.sharedMessage.subscribe(message => {
            
            this.color = message
            this.getGeneralInfo()
            console.log("message"+message)
            
          })
    }
    getGeneralInfo(){
        this.GeneralInfoService.settings().subscribe(data => {
            this.str = JSON.stringify(data.data)
            this.res = this.str.replace(/-/g, "_");
            console.log(this.res)
            this.res = JSON.parse(this.res)
            this.color =  this.res.color_dashboard.en
            console.log("getGeneralInfo"+ this.color)
            this.tabLogo = this.res.tab_logo.en
            this.favIcon.href = this.tabLogo.replace(/_/g, "-");
            this.title = this.res.title.en;
            this.titleService.setTitle(this.title);   
    })
    }
    getcolor(){
        this.GeneralInfoService.sharedMessage.subscribe(message => {
            // this.lang = message;
            console.log("yarb" + message)
            
          })
    }
}
