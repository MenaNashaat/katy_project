import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { JsonPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import {ListApplicationsService} from '../../services/list-applications.service';
import {StudentService} from '../../services/student/student.service';
import { listAplications} from '../../models/listAplications'
import { WattingService , SpinnerService } from '../../services';
import {  ToastService} from '../../services/toast/toast.service';
import { TranslateService } from "@ngx-translate/core";
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-edit-application',
  templateUrl: './edit-application.component.html',
  styleUrls: ['./edit-application.component.css']
})
export class EditApplicationComponent implements OnInit {

  toppings = new FormControl();

  //Nationalities: string[] = ['Algeria', 'Bahrain', 'the Comoros', 'Djibouti', 'Egypt', 'Iraq', 'Jordan', 'Kuwait', 'Lebanon', 'Libya', 'Mauritania', 'Morocco', 'Oman', 'Palestine', 'Qatar', 'Saudi Arabia', 'Somalia', 'Sudan', 'Syria', 'Tunisia', ' United Arab Emirates', 'and Yemen'];
  //Languages: string[] = ['Chinese', 'Spanish', 'English', 'Hindi', 'Arabic', ' Bengali', 'Portuguese', 'Kuwait', 'Russian', 'Japanese', 'german', 'french'];
  Nationalities:any;
  Languages:any;
  date: { year: number, month: number };
  model: NgbDateStruct;
  AddChildForm: FormGroup;
  loading = false;
  submitted = false;
  arr: any;
  name: string;
  success:string;
  searchText = '';
  characters:any = [];
  selectedNationality:any;
  selectedLanguages:any ;
 
  dateAPI:any
  dateOfBirth :any;
  year: any;
  month:any;
  day:any;
  Medical;
  medicalArr = [];
  medicalPartAr : boolean;
  medicalPartEn :  boolean;
  applicationInfo: any;
  applicationDetails:any;
  lang: any
  currentUrl = "";
  str: string;
  nn: string;
  n: any;
  res: string;
  updateSuccesfuly: any;
  constructor( 
    private calendar: NgbCalendar,
    private formBuilder: FormBuilder,
    private router: Router,
    private ActivatedRoute: ActivatedRoute,
    private route:ActivatedRoute ,private StudentService:StudentService, 
    private translate:TranslateService, private ListApplicationsService: ListApplicationsService,
     private toastService: ToastService , private WattingService:WattingService ,
     private SpinnerService:SpinnerService) {

  }


  ngOnInit() {
   
    this.AddChildForm = this.formBuilder.group({

      child_first_name: ['', Validators.required],
      child_middle_name: ['', Validators.required],
      child_last_name: ['', Validators.required],
      child_date_of_birth: ['', Validators.required],
      nationality: [''],
      languagess: [''],
      gender: ['', ],
      Ability_eat: [''],
      Ability_hand: [''],
      Ability_toilet: [''],
      Emergency_first_name: ['', Validators.required],
      Emergency_last_name: ['', Validators.required],
      Emergency_mobile: ['', Validators.required],
      Emergency_Alternative_Mobile: ['', Validators.required],
      Emergency_Relation_Family: ['', Validators.required],
      Emergency_Address: ['', Validators.required],
      Emergency_Permission: [''],
      Mother_name: ['', Validators.required],
      Mother_Mobile: ['', Validators.required],
      Mother_email: ['', [Validators.required, Validators.email]],
      hour: [''],
      mother_job: ['', Validators.required],
      father_name: ['', Validators.required],
      father_job: ['', Validators.required],
      father_Mobile: ['', Validators.required],
      father_email: ['', [Validators.required, Validators.email]],
      name_ar: ['', Validators.required],
      lives_with: ['', Validators.required],
      parents_are: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue],
      IDIqama: [''],
      address: [''],

    });
    this.ListApplicationsService.getLanguage().subscribe(data => { this.Languages = data.data;  });
    this.ListApplicationsService.getNationality().subscribe(data => { this.Nationalities = data.data;  });
    this.str = window.location.pathname;
    this.n = this.str.search("/Child");
    this.res = this.str.slice(0, this.n);
    this.nn = this.res.replace("/Edit_Application/", "");
    this.ListApplicationsService.getRegisterById(this.nn).subscribe(result => {
      this.applicationInfo = result.data.children[0];
      this.applicationDetails = result.data;
      console.log("this.applicationDetails.can_eat_alone"+this.applicationDetails.can_eat_alone)
      this.Medical = this.applicationInfo.medical
      if(localStorage.getItem("language") == "en")
      {
        this.medicalPartAr = false
        this.medicalPartEn =  true
      }  
      else
      {
        this.medicalPartAr = true 
        this.medicalPartEn = false 
      }
      //  
        let control = new FormControl('');
      for (let input of this.Medical) {
        console.log("input.valid " +input.valid)
        this.AddChildForm.addControl(input.input, new FormControl(input.comment));
        this.AddChildForm.addControl(input.radio, new FormControl(input.valid));
      }
      // 
      console.log(JSON.stringify(this.applicationInfo));
      console.log("this.applicationInfo.language" +JSON.stringify(this.applicationInfo.language));
      console.log("this.applicationInfo.nationality" +JSON.stringify(this.applicationInfo.nationality));
      this.AddChildForm.patchValue({
        child_first_name: this.applicationInfo.first_name , 
        child_middle_name: this.applicationInfo.middle_name , 
        child_last_name: this.applicationInfo.last_name , 
        nationality :  this.applicationInfo.nationality,
         languagess :  this.applicationInfo.language ,
        gender :  this.applicationInfo.gender,

        Ability_eat: this.applicationDetails.can_eat_alone ,
        Ability_hand: this.applicationDetails.use_right_hand,
        Ability_toilet: this.applicationDetails.go_toilet_alone,
        Emergency_first_name: this.applicationDetails.contact.first_name,
        Emergency_last_name: this.applicationDetails.contact.last_name,
        Emergency_mobile: this.applicationDetails.contact.mobile,
        Emergency_Alternative_Mobile: this.applicationDetails.contact.alt_mobile,
        Emergency_Relation_Family: this.applicationDetails.contact.relation,
        Emergency_Address: this.applicationDetails.contact.relation,
        Emergency_Permission: this.applicationDetails.contact.can_pick_child,

        Mother_name:  this.applicationInfo.mother_name,
        Mother_Mobile: this.applicationInfo.mother_phone,
        Mother_email:  this.applicationInfo.mother_email,
        // hour:  this.applicationInfo.hour,
        mother_job:  this.applicationInfo.mother_job,
        father_name:  this.applicationInfo.father_name,
        father_job: this.applicationInfo.father_job,
        father_Mobile:  this.applicationInfo.father_phone,
        father_email:  this.applicationInfo.father_email,
        name_ar: this.applicationInfo.name_ar,
        lives_with:  this.applicationInfo.lives_with,
        parents_are:  this.applicationInfo.parents_are,
        // acceptTerms: [ this.applicationInfo.,,
        //  IDIqama:  this.applicationDetails.parent.national_id,
        // address: this.applicationDetails.parent.address,

      });
      
       this.dateAPI =   this.applicationInfo.date_of_birth
       this.year = parseInt(this.dateAPI.slice(0,4));
       this.month =  parseInt(this.dateAPI.slice(5,7));
       this.day =  parseInt(this.dateAPI.slice(8,10));
       this.dateOfBirth =  { "year":   this.year , "month": this.month, "day":  this.day };

  

    })
    this.ListApplicationsService.sharedMessage.subscribe(message => {
      this.lang = message;
     
      if (this.lang == "ar") {
        console.log("ifff")
        this.medicalPartAr = true 
        this.medicalPartEn = false 
      }
      else if (this.lang == "en") {
        console.log("else")
        this.medicalPartAr = false
        this.medicalPartEn =  true
      }
    })
    this.translate.get('backend.editChild').subscribe((text:string) => this.success = text);
  }

  // convenience getter for easy access to form fields
  get f() { return this.AddChildForm.controls; }
  showSuccess(message) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(message) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 3000 });
  }

  onSubmit() {
    console.log("this.AddChildForm.value" + JSON.stringify(this.AddChildForm.value))
    let date_of_birth = this.f.child_date_of_birth.value
    if (date_of_birth != null && date_of_birth != '') {
      if (date_of_birth.month.toString().length < 2) {
        date_of_birth.month = "0" + date_of_birth.month;
      }
      else {
        date_of_birth.month = date_of_birth.month;
      }
      if (date_of_birth.day.toString().length < 2) {
        date_of_birth.day = "0" + date_of_birth.day;
      }
      else {
        date_of_birth.day = date_of_birth.day;
      }
    }
    let date = date_of_birth.year + "-" + date_of_birth.month + "-" + date_of_birth.day
    this.submitted = true;
    this.medicalArr = []
    for (var i = 0; i < this.Medical.length; i++) {
      console.log("this.Medical[i].id"+this.Medical[i])
      var idInpu =  this.Medical[i].id
      var input =  'this.f.input_'+this.Medical[i].id+'.value'
     var radio =  'this.f.radio_'+this.Medical[i].id+'.value'
     this.medicalArr.push( { "id": idInpu, "valid": eval(radio), "comment": eval(input) })
      
    }
    // if (this.Medical.length == 2) {
    //   this.medicalArr.push({ "id": 1, "valid": this.f.radio_1.value, "comment": this.f.input_1.value },
    //     { "id": 2, "valid": this.f.radio_2.value, "comment": this.f.input_2.value })
    // }
    // if (this.Medical.length == 3) {
    //   this.medicalArr.push({ "id": 1, "valid": this.f.radio_1.value, "comment": this.f.input_1.value },
    //     { "id": 2, "valid": this.f.radio_2.value, "comment": this.f.input_2.value },
    //     { "id": 3, "valid": this.f.radio_3.value, "comment": this.f.input_3.value })
    // }
    // if (this.Medical.length == 4) {
    //   this.medicalArr.push({ "id": 1, "valid": this.f.radio_1.value, "comment": this.f.input_1.value },
    //     { "id": 2, "valid": this.f.radio_2.value, "comment": this.f.input_2.value },
    //     { "id": 3, "valid": this.f.radio_3.value, "comment": this.f.input_3.value },
    //     { "id": 4, "valid": this.f.radio_4.value, "comment": this.f.input_4.value })
    // }
    // if (this.Medical.length == 5) {
    //   this.medicalArr.push({ "id": 1, "valid": this.f.radio_1.value, "comment": this.f.input_1.value },
    //     { "id": 2, "valid": this.f.radio_2.value, "comment": this.f.input_2.value },
    //     { "id": 3, "valid": this.f.radio_3.value, "comment": this.f.input_3.value },
    //     { "id": 4, "valid": this.f.radio_4.value, "comment": this.f.input_4.value },
    //     { "id": 5, "valid": this.f.radio_5.value, "comment": this.f.input_5.value })
    // }
    // if (this.Medical.length == 6) {
    //   this.medicalArr.push({ "id": 1, "valid": this.f.radio_1.value, "comment": this.f.input_1.value },
    //     { "id": 2, "valid": this.f.radio_2.value, "comment": this.f.input_2.value },
    //     { "id": 3, "valid": this.f.radio_3.value, "comment": this.f.input_3.value },
    //     { "id": 4, "valid": this.f.radio_4.value, "comment": this.f.input_4.value },
    //     { "id": 5, "valid": this.f.radio_5.value, "comment": this.f.input_5.value },
    //     { "id": 6, "valid": this.f.radio_6.value, "comment": this.f.input_6.value })
    // }

    let count = 0;
    console.log("this.medicalArr" + JSON.stringify(this.medicalArr))
    this.medicalArr.forEach((val, i) => { if (!val.comment) delete val["comment"]; });
    console.log("this.medicalArr" + JSON.stringify(this.medicalArr))
    this.arr = { first_name: this.f.child_first_name.value, middle_name: this.f.child_middle_name.value, last_name: this.f.child_last_name.value,  date_of_birth: date,
      language: this.f.languagess.value , nationality:this.f.nationality.value ,  gender:  this.f.gender.value, 
      lives_with: this.f.lives_with.value, parents_are: this.f.parents_are.value, mother_name: this.f.Mother_name.value , mother_job: this.f.mother_job.value, father_job: this.f.father_job.value, 
      father_name: this.f.father_name.value, father_phone: this.f.father_Mobile.value, 
      father_email:  this.f.father_email.value, mother_email:  this.f.Mother_email.value, 
      mother_phone:  this.f.Mother_Mobile.value, name_ar: this.f.name_ar.value,
      
      medical: this.medicalArr ,
      contact_first_name: this.f.Emergency_first_name.value, contact_last_name: this.f.Emergency_last_name.value, contact_mobile: this.f.Emergency_mobile.value, contact_alt_mobile: this.f.Emergency_Alternative_Mobile.value,
      contact_relation: this.f.Emergency_Relation_Family.value, contact_address: this.f.Emergency_Address.value, can_pick_child: this.f.Emergency_Permission.value,
      can_eat_alone: this.f.Ability_eat.value , use_right_hand :this.f.Ability_hand.value, go_toilet_alone: this.f.Ability_toilet.value} 
    // stop here if form is invalid
    // if (this.AddChildForm.invalid) {
    //   return;
    // }
console.log("res Arr"+  JSON.stringify(this.arr))
    this.loading = true;
    const id = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.ListApplicationsService.editChild( this.nn, id , this.arr)
      .pipe(first())
      .subscribe(
        data => {
          this.translate.get('backend.USuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
        
          this.showSuccess(this.updateSuccesfuly)
          this.loading = false
          // this.alertService.success('Create Application successful', true);
          // this.router.navigate(['/']);
        },
        error => {
          this.showDanger(error.error.data.errors);
          // console.log("no" + JSON.stringify(error))
          // this.alertService.error(error.error.data.errors);
          this.loading = false;
        });
  }






}
