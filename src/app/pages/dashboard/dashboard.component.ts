import { Component, OnInit } from '@angular/core';
import {DashboardService , SpinnerService , BranchService} from '../../services'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {Statistics} from '../../models'

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit{
    statistics: Statistics;
    branchForm: FormGroup;
    branch: any;
    loading = false;
    submitted = false;
    constructor( private SpinnerService:SpinnerService,
        private formBuilder: FormBuilder,
        private BranchService:BranchService, private DashboardService:DashboardService) { }

    ngOnInit(){
        this.getStatistics()
        this.getBranch()
        this.branchForm = this.formBuilder.group({
            name: ['', Validators.required],
            level: []
      
          });
    }
     // convenience getter for easy access to form fields
  get f() { return this.branchForm.controls; }

    getStatistics(){
        this.DashboardService.getStatistics().subscribe(data => this.statistics = data.data)
    }
    getBranch() {
        this.BranchService.getBranch().subscribe(data => this.branch = data.data)
      }
      onSubmit() {
        this.submitted = true;
    
        // stop here if form is invalid
        if (this.branchForm.invalid) {
          return;
        }

        this.loading = true;
        this.DashboardService.getStatisticsById(this.f.name.value)
          .pipe(first())
          .subscribe(
            data => {
              this.statistics = data.data; 
              
              this.loading = false
            },
    
            error => {
              alert(error)
              //this.showDanger(error.error.data.errors)
              // this.alertService.error(this.error);
              this.loading = false;
            });
    
      }  


}
