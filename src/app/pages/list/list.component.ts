import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { EmailTemplateService, SpinnerService  , ListService} from '../../services';
import { Tabel } from '../../models';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  //  pagination
  pagination: Tabel;
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  per_page: Tabel;
  total: Tabel;
  previousPage: any;

  closeResult = '';
  meals: Meal;
  loading = false;
  submitted = false;
  questionForm: FormGroup;
  addForm: FormGroup;
  branchForm: FormGroup;
  addFormLists: FormGroup;
  assignEvaluation: FormGroup;
  editList: FormGroup;
  deleteSucc: string;
  users: any;
  updateSuccesfuly: string;
  Lists: any;
  QuestionDetails: any;
  grade: boolean = false;
  questionOption: boolean = false;
  gradeEdit: boolean = false;
  questionOptionEdit: boolean = false;
  optionORgrade: string;
  arr: any;
  levels: any;

  Branch: any;
  level: any;
  formDetails: any;
  brannchValue: any;
  levelsInsideForm: any;
  idAssign: any[];
  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private EmailTemplateService: EmailTemplateService,
    private ListService:ListService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private SpinnerService: SpinnerService) { }

  ngOnInit(): void {
    this.getAllLists()
    this.addFormLists = this.formBuilder.group({
      name: ['', Validators.required],
      // subject: ['', Validators.required],
      // body: ['', Validators.required],
    });
    this.editList = this.formBuilder.group({
      name: ['', Validators.required],
      // subject: ['', Validators.required],
      // body: ['', Validators.required],
    });
  }

  get k() { return this.addFormLists.controls; }
  get e() { return this.editList.controls; }

  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllLists() {
   this.ListService.getAllLists().subscribe(data => {
      this.Lists = data.data;
      this.pagination = data.meta;
      this.total = data.meta.total;
      this.per_page = data.meta.per_page
    })
  }
  addFormlist(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

      this.ListService.createList({name: this.k.name.value})
        .pipe(first())
        .subscribe(
          data => {
            this.getAllLists()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });

            this.showSuccess(this.updateSuccesfuly)
            this.addFormLists.reset()

          },
          error => {
            console.log(JSON.stringify(error))
            this.showDanger(error.error.data.errors)
            this.loading = false;

          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  editListForm(content, formId) {

    this.ListService.getListByID(formId).subscribe(data => {
      this.formDetails = data.data;
      this.editList.patchValue({
        name: this.formDetails.name,
        
       })
    })


    // });

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.ListService.editList(formId, { name: this.e.name.value})
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });

            this.showSuccess(this.updateSuccesfuly)
            this.getAllLists()

            // this.showSuccess(this.updateSuccesfuly)
            //this.LevelForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            // this.showDanger(error.error.data.errors)
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  delete(content, id) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.ListService.deleteList(id)
        .pipe(first())
        .subscribe(
          data => {

            this.getAllLists()
            this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
            this.showDanger(this.deleteSucc)
          },

          error => {
            alert(error.error.data.errors)
            //this.showDanger(error.error.data.errors)
            // this.alertService.error(this.error);

          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
