import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { BranchService  , SpinnerService } from '../../services';
import { Branch } from '../../models';
import { first } from 'rxjs/operators';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {
  closeResult = '';
  branchForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  branch:Branch;
  branchName:string;
  BranchId: Branch;
  // localization
  updateSuccesfuly:string;
  addSuccesfuly:string;

  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private BranchService: BranchService,
    private translate:TranslateService,
    private SpinnerService:SpinnerService
  ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.branchForm = this.formBuilder.group({
      name: ['', Validators.required],
      url: ['', Validators.required],
      phone: ['', Validators.required],
    });
    this.getAllBranch()
    this.translate.get('backend.USuccesfuly').subscribe((accept:string) => {this.updateSuccesfuly = accept});
    this.translate.get('backend.ASuccesfuly').subscribe((Succesfuly:string) => {this.addSuccesfuly = Succesfuly});

  }
  // convenience getter for easy access to form fields
  get f() { return this.branchForm.controls; }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllBranch() {
    this.BranchService.getBranch().subscribe(data => this.branch = data.data)

  }

  open(edit, id) {
   
    this.branchForm.reset();
    this.BranchService.getBranchById(id).subscribe(data => {this.BranchId = data.data ;
      this.branchForm.patchValue({
        name: this.BranchId.name,
        phone: this.BranchId.phone,
        url: this.BranchId.url
      });
       console.log("this.BranchId"+ JSON.stringify(this.BranchId))})
  
    this.modalService.open(edit, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      
      this.BranchService.editBranch(this.branchForm.value, id)
        .pipe(first())
        .subscribe(
          data => {
            
            this.getAllBranch()

            this.showSuccess(this.updateSuccesfuly)
            this.branchForm.reset()
          },
          error => {
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
 
  show(content) {
    this.branchForm.reset();
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.BranchService.postBranch(this.branchForm.value)
        .pipe(first())
        .subscribe(
          data => {

            this.getAllBranch()
            
            this.showSuccess(this.addSuccesfuly)
            this.branchForm.reset()

            // this.router.navigate([this.returnUrl]);
          },
          error => {
            this.showDanger(error.error.data.errors)
            // this.alertService.error(error.error.data.errors);
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
