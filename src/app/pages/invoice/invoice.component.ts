import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { BillService, SpinnerService } from '../../services'
import { ToastService } from '../../services/toast/toast.service';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  date: { year: number, month: number };
  model: NgbDateStruct;
  plans: any;
  addSuccesfuly: string;
  applicationId: string;
  invoice: any;
  children: any;
  InvoiceItem: any;
  InvoiceName: any;
  serial: any;
  description: any;
  email: any;
  phone: any;
  status: any;
  registration_number: any;
  address: any;
  company_name: any;
  parent: any;
  city: any;
  zip_code: any;
  title: any;
  vat: any;
  amount: any;
  valueTax: number;
  discount_value: any;
  discount_percent: any;
  totalPrice: any;
  is_vat_included: any;
  discountPercentToValue: any;
  invoicelogo: any;
  removePaidBtn: boolean = true
  descriptionPlan: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private BillService: BillService,
    private toastService: ToastService,
    private translate: TranslateService,
    private SpinnerService: SpinnerService
  ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getInvoice()

    this.getSetting()
    this.getInvoiceItem()
  }
  goBack() {
    window.history.back();
  }
  showSuccess(test) {
    this.toastService.show(test, { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(test) {
    this.toastService.show(test, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getSetting() {
    this.BillService.getSettings().subscribe(data => {

      this.email = data.data.email.en
      this.phone = data.data.phone.en
      this.address = data.data.address.ar
      this.city = data.data.city.ar
      this.zip_code = data.data.zip_code.ar
      this.title = data.data.title.en
      this.company_name = data.data.company_name.ar
      this.registration_number = data.data.registration_number.en
      // this.vat = data.data.vat.en,
      this.invoicelogo = data.data.invoice_logo.en
    });
  }

  getInvoice() {
    const Id = this.route.snapshot.paramMap.get('id');

    this.BillService.getInvoice(Id).subscribe(data => {
      this.invoice = data.data;
      if (this.invoice.status == 'Paid') {
        this.removePaidBtn = false
      }
      this.serial = data.data.serial
      this.status = data.data.status
      // this.applicationId =this.invoice.application.id;
      this.description = this.invoice.description
      this.children = this.invoice.application.children[0];
      this.parent = this.invoice.parent
    })


  }
  getInvoiceItem() {
    const Id = this.route.snapshot.paramMap.get('id');
    this.BillService.getInvoiceItem(Id).subscribe(data => {
      this.getSetting();

      if (data.data.message == 'No plan for custom bills') {
        this.BillService.getInvoice(Id).subscribe(data => {
          this.invoice = data.data;
          this.InvoiceItem = '';
          this.descriptionPlan = this.invoice.description;
          this.InvoiceName = '';
          this.is_vat_included = this.invoice.is_vat_included
          this.discount_percent = 0;
          this.discount_value = 0;
          this.amount = this.invoice.due_amount
          this.discountPercentToValue = 0
          this.vat = this.invoice.vat
        })

        this.BillService.getSettings().subscribe(data => {


          if (this.is_vat_included == '0') {
            this.valueTax = parseInt(this.amount) * (parseInt(this.vat) / 100);
            this.totalPrice = parseInt(this.amount) + this.valueTax

          }
          else {

            this.valueTax = Math.round(parseInt(this.amount) - (parseInt(this.amount) / (1 + (parseInt(this.vat) / 100))));
            this.totalPrice = parseInt(this.amount)

          }

        });
      }
      else {
        this.InvoiceItem = data.data;
        this.descriptionPlan = data.data.description;
        this.InvoiceName = data.data.name;
        this.is_vat_included = this.InvoiceItem.is_vat_included
        this.discount_percent = this.InvoiceItem.discount_percent;
        this.discount_value = this.InvoiceItem.discount_value;
        this.amount = data.data.amount
        this.discountPercentToValue = 0


        this.BillService.getInvoice(Id).subscribe(data => {

          this.vat = (data.data.vat) * 100

        })
        this.BillService.getSettings().subscribe(data => {
          //this.vat = data.data.vat.en

          if (this.is_vat_included == '0') {
            this.valueTax = parseInt(this.amount) * (parseInt(this.vat) / 100);
            this.totalPrice = parseInt(this.amount) + this.valueTax
            if (this.discount_percent != '0') {
              this.discountPercentToValue = parseInt(this.amount) * (parseInt(this.discount_percent) / 100);
              this.totalPrice = parseInt(this.totalPrice) - (parseInt(this.amount) * (parseInt(this.discount_percent) / 100));
            }
            else if (this.discount_value != '0') {
              this.discountPercentToValue = this.discount_value
              this.totalPrice = parseInt(this.totalPrice) - parseInt(this.discount_value)
            }
          }
          else {
            this.valueTax = Math.round(parseInt(this.amount) - (parseInt(this.amount) / (1 + (parseInt(this.vat) / 100))));
            this.totalPrice = parseInt(this.amount)
            if (this.discount_percent != '0') {
              this.discountPercentToValue = parseInt(this.amount) * (parseInt(this.discount_percent) / 100);
              this.totalPrice = parseInt(this.totalPrice) - (parseInt(this.amount) * (parseInt(this.discount_percent) / 100));
            }
            else if (this.discount_value != '0') {
              this.discountPercentToValue = this.discount_value
              this.totalPrice = parseInt(this.totalPrice) - parseInt(this.discount_value)
            }
          }

        });
      }





    })

  }

  paidInvoice() {


    this.loading = true;
    const Id = this.route.snapshot.paramMap.get('id');
    this.BillService.paid(Id, { amount: this.invoice.due_amount })
      .pipe(first())
      .subscribe(
        data => {
          this.translate.get('backend.ASuccesfuly').subscribe((Succesfuly: string) => { this.addSuccesfuly = Succesfuly; console.log(this.addSuccesfuly) });
          this.showSuccess(this.addSuccesfuly)
          this.removePaidBtn = false
          // this.router.navigate(['/bill']);
          // this.router.navigate([this.returnUrl]);
        },
        error => {
          this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);
          this.loading = false;
        });
  }

}
