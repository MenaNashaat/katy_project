import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import {AssignToPlanService , SpinnerService} from '../../services'
import {  ToastService} from '../../services/toast/toast.service';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-assign-to-plan',
  templateUrl: './assign-to-plan.component.html',
  styleUrls: ['./assign-to-plan.component.css']
})
export class AssignToPlanComponent implements OnInit {
  assignForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  date: { year: number, month: number };
  model: NgbDateStruct;
  plans: any;
  addSuccesfuly: string;
  applicationId: string;
  constructor(  private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private AssignToPlanService: AssignToPlanService,
    private toastService: ToastService,
    private translate: TranslateService, 
    private SpinnerService:SpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.applicationId = this.route.snapshot.paramMap.get('id');
    this.getAllPlan()
    this.assignForm = this.formBuilder.group({
      plan: ['', Validators.required],
      date: ['', Validators.required],
    });
  }
  showSuccess(test) {
    this.toastService.show( test, { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(test) {
    this.toastService.show(test , { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllPlan() {
    this.AssignToPlanService.getPlans().subscribe(data => { this.plans = data.data; console.log(JSON.stringify(this.plans)) })
  }

  // convenience getter for easy access to form fields
  get f() { return this.assignForm.controls; }
  onSubmit() {
    // this.submitted = true;

    // // stop here if form is invalid
    // if (this.assignForm.invalid) {
    //   return;
    // }

    this.loading = true;
    let date_of_birth = this.f.date.value
    if (date_of_birth != null && date_of_birth != '') {
      if (date_of_birth.month.toString().length < 2) {
        date_of_birth.month = "0" + date_of_birth.month;
      }
      else {
        date_of_birth.month = date_of_birth.month;
      }
      if (date_of_birth.day.toString().length < 2) {
        date_of_birth.day = "0" + date_of_birth.day;
      }
      else {
        date_of_birth.day = date_of_birth.day;
      }
    }

    let date = date_of_birth.year + "-" + date_of_birth.month + "-" + date_of_birth.day
    const BranchId = this.route.snapshot.paramMap.get('id');
    this.AssignToPlanService.assignPlan({plan_start_date: date , plan_id: this.f.plan.value, application_id: BranchId })
      .pipe(first())
      .subscribe(
        data => {
          this.translate.get('backend.ASuccesfuly').subscribe((Succesfuly: string) => { this.addSuccesfuly = Succesfuly; console.log(this.addSuccesfuly) });
          this.showSuccess(this.addSuccesfuly)
          this.loading = false;
          this.assignForm.reset()
          //this.router.navigate(['/home']);
         // this.router.navigate([this.returnUrl]);
        },
        error => {
          this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);
          this.loading = false;
        });
  }

}
