import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssignToPlanComponent } from './assign-to-plan.component';


const routes: Routes = [{ path: '', component: AssignToPlanComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignToPlanRoutingModule { }
