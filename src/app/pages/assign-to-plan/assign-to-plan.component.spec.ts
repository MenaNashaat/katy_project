import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignToPlanComponent } from './assign-to-plan.component';

describe('AssignToPlanComponent', () => {
  let component: AssignToPlanComponent;
  let fixture: ComponentFixture<AssignToPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignToPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignToPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
