import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { ManagementService , SpinnerService} from '../../services';
import { Tabel } from '../../models';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  page1 = 1;
  page2 = 1;
  pagination: Tabel;
  per_page: Tabel;
  total: Tabel;
  pagination1: Tabel;
  per_page1: Tabel;
  total1: Tabel;
  pagination2: Tabel;
  per_page2: Tabel;
  total2: Tabel;
  previousPage: any;
  previousPage1: any;
  previousPage2: any;
  closeResult = '';
  meals: Meal;
  loading = false;
  submitted = false;
  userForm: FormGroup;
  addUser: FormGroup;
  assignForm: FormGroup;
  selectedFile: File;
  deleteSucc: string;
  users: any;
  updateSuccesfuly: string;
  usersDetails: any;
  Admin: any;
  Teacher: any;
  Parent: any;
  allClass: any;
  classes: any;
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private ManagementService: ManagementService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private SpinnerService:SpinnerService
  ) { 
   
  }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllAdmin()
    this.getAllTeacher()
    this.getAllParent()
    this.userForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      mobile: ['', Validators.required],
      national_id: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      password_confirmation: ['', Validators.required],
      occupation: ['', Validators.required],
      degree: ['', Validators.required],
      
    });
    this.addUser = this.formBuilder.group({
      first_name: ['', Validators.required],
      Middle_name: ['', Validators.required],
      last_name: ['', Validators.required],
      mobile: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      password_confirmation: ['', Validators.required],
      
    });
    
    this.assignForm = this.formBuilder.group({
      class_id: ['', Validators.required],
  
    });
  }
   // convenience getter for easy access to form fields
   get f() { return this.userForm.controls; }
   get g() { return this.assignForm.controls; }
   get r() { return this.addUser.controls; }
   
 
   loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;

       this.getAllParent()
    }
  }
  loadPageTecher(page1: number) {
    if (page1 !== this.previousPage1) {
      this.previousPage1 = page1;
      this.getAllTeacher();
    }
  }
  loadPageAdmin(page2: number) {
    if (page2 !== this.previousPage2) {
      this.previousPage2 = page2;
      this.getAllAdmin();
    }
  }
  // 
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllAdmin() {
    this.ManagementService.getAdmin(this.previousPage2).subscribe(data => {
      this.Admin = data.data;
      this.pagination2 = data.meta;
      this.total2 = data.meta.total;
      this.per_page2 = data.meta.per_page
      console.log("this.meals" + JSON.stringify(data))
    })
  }
  getAllTeacher() {
    this.ManagementService.getTeacher(this.previousPage1).subscribe(data => {
      this.Teacher = data.data;
      this.classes = data.data.classes; 
      this.pagination1 = data.meta;
      this.total1 = data.meta.total;
      this.per_page1 = data.meta.per_page
      console.log("techerrrrrr" + JSON.stringify(data))
    })
  }
  getAllParent() {
    this.ManagementService.getParent(this.previousPage).subscribe(data => {
      this.Parent = data.data; 
      this.pagination = data.meta;
      this.total = data.meta.total;
      this.per_page = data.meta.per_page
      console.log("this.meals" + JSON.stringify(data))
    })
  }
  delete(content, id) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.ManagementService.deleteUser(id)
      .pipe(first())
      .subscribe(
        data => {
          this.getAllAdmin()
          this.getAllTeacher()
          this.getAllParent()
          this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
          this.showDanger(this.deleteSucc)
        },

        error => {
          alert(error.error.data.errors)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);

        });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content  , role) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.ManagementService.createUser({first_name: this.f.first_name.value, mobile: this.f.mobile.value, national_id: this.f.national_id.value, email: this.f.email.value, 
        password: this.f.password.value , password_confirmation:this.f.password_confirmation.value ,
         role: role ,  degree:this.f.degree.value , occupation:this.f.occupation.value } )
        .pipe(first())
        .subscribe(
          data => {
            this.getAllTeacher()
            this.getAllAdmin()
            this.getAllParent()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
             this.showSuccess(this.updateSuccesfuly)
            this.userForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  register(content ) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.ManagementService.addUser({first_name: this.r.first_name.value,
        Middle_name: this.r.Middle_name.value,last_name: this.r.last_name.value,
         mobile: this.r.mobile.value,  email: this.r.email.value, 
        password: this.r.password.value , password_confirmation:this.r.password_confirmation.value ,
         role: "Parent"    } )
        .pipe(first())
        .subscribe(
          data => {
           
            this.getAllParent()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
             this.showSuccess(this.updateSuccesfuly)
            this.addUser.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  edit(content, id) {
    
    this.ManagementService.getUserById(id).subscribe(data => {
      this.usersDetails = data.data;
      this.userForm.patchValue({
      first_name:   this.usersDetails.first_name  , 
      mobile:  this.usersDetails.mobile 
     
    })
  })
 

    // });

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      

      this.ManagementService.editUser(this.userForm.value , id)
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
            this.showSuccess(this.updateSuccesfuly)
            this.getAllAdmin()
            this.getAllTeacher()
            this.getAllParent()
            // this.showSuccess(this.updateSuccesfuly)
            //this.LevelForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            // this.showDanger(error.error.data.errors)
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  assign(content, id) {
    
    this.ManagementService.getClass().subscribe(data => {
      this.allClass = data.data;
     
  })
 

    // });

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      

      this.ManagementService.assign({class_id: this.g.class_id.value , ids: [id] })
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
            this.showSuccess(this.updateSuccesfuly)
          
            this.getAllTeacher()
           
            // this.showSuccess(this.updateSuccesfuly)
            this.assignForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            // this.showDanger(error.error.data.errors)
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
