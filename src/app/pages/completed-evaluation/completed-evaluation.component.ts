import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { EvaluationService , SpinnerService} from '../../services';
import { Tabel } from '../../models';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-completed-evaluation',
  templateUrl: './completed-evaluation.component.html',
  styleUrls: ['./completed-evaluation.component.css']
})
export class CompletedEvaluationComponent implements OnInit {
//  pagination
pagination: Tabel;
itemsPerPage: number;
totalItems: any;
page = 1;
per_page: Tabel;
total: Tabel;
previousPage: any;
page2 = 1;
per_page2: Tabel;
total2: Tabel;
previousPage2: any;

closeResult = '';
meals: Meal;
loading = false;
submitted = false;
  ApprovalEvaluations: any;
  UnApprovalEvaluations: any;
  updateSuccesfuly: any;

constructor(private modalService: NgbModal,
  private formBuilder: FormBuilder,
  private ToastService: ToastService,
  private EvaluationService: EvaluationService,
  private translate: TranslateService,
  private ActivatedRoute: ActivatedRoute, 
  private SpinnerService:SpinnerService) { }

  ngOnInit(): void {
   
    this.getAllApprovalEvaluation()
    this.getAllUnApprovalEvaluation()
  }
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
 
       this.getAllApprovalEvaluation()
    }
  }
  loadPage2(page2: number) {
    if (page2 !== this.previousPage2) {
      this.previousPage2 = page2;
 
       this.getAllApprovalEvaluation()
    }
  }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllApprovalEvaluation() {
    this.EvaluationService.getAllApprovalEvaluation( this.previousPage).subscribe(data => {
      this.ApprovalEvaluations = data.data; 
      this.pagination = data.meta;
      this.total = data.meta.total;
      this.per_page = data.meta.per_page
    })
  }
  getAllUnApprovalEvaluation() {
    this.EvaluationService.getAllUnApprovalEvaluation( this.previousPage2).subscribe(data => {
      this.UnApprovalEvaluations = data.data; 
      this.pagination = data.meta;
      this.total2 = data.meta.total;
      this.per_page2 = data.meta.per_page
    })
  }
  Approve(id){
    this.EvaluationService.approve(id).subscribe(data => {
      this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });

      this.showSuccess(this.updateSuccesfuly)
      this.getAllUnApprovalEvaluation()
      this.getAllApprovalEvaluation()
    })
  }

}
