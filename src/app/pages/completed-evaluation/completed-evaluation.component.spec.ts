import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedEvaluationComponent } from './completed-evaluation.component';

describe('CompletedEvaluationComponent', () => {
  let component: CompletedEvaluationComponent;
  let fixture: ComponentFixture<CompletedEvaluationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedEvaluationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedEvaluationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
