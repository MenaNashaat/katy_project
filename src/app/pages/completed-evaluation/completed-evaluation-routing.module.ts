import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompletedEvaluationComponent } from './completed-evaluation.component';


const routes: Routes = [{ path: '', component: CompletedEvaluationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompletedEvaluationRoutingModule { }
