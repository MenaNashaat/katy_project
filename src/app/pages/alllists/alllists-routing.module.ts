import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlllistsComponent } from './alllists.component';

const routes: Routes = [{ path: '', component: AlllistsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlllistsRoutingModule { }
