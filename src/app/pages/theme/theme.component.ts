import { Component, OnInit } from '@angular/core';
import { GeneralInfoService   } from '../../services';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { StudentService } from '../../services';
import { first } from 'rxjs/operators';
import { listAplications, Class, Tabel } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.css']
})
export class ThemeComponent implements OnInit {
  closeResult = '';
  generalForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;

    // localization
    updateSuccesfuly:string;
    addSuccesfuly:string;
  str: any;
  res: any;
  constructor(
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private GeneralInfoService: GeneralInfoService,
    private translate: TranslateService,
  ) { }

  ngOnInit(): void {
   
    this.generalForm = this.formBuilder.group({
      facebook: ['', Validators.required],
     
  
      
    });
  }
  showSuccess() {
    this.toastService.show('Update Succesfully ', { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(test) {
    this.toastService.show(test , { classname: 'bg-danger text-light', delay: 3000 });
  }

  // convenience getter for easy access to form fields
  get f() { return this.generalForm.controls; }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.generalForm.invalid) {
      return;
    }

    this.loading = true;
    this.GeneralInfoService.addSettings({facebook: this.f.facebook.value})
      .pipe(first())
      .subscribe(
        data => {
          this.showSuccess()
        
          
          console.log("ssssssssss"+JSON.stringify(data))
          //this.router.navigate(['/home']);
         // this.router.navigate([this.returnUrl]);
         this.loading = false;
        },
        error => {
         
          this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);
          this.loading = false;
        });
  }
}
