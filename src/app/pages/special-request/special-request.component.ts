import { Component, OnInit } from '@angular/core';
import {SpecialRequestService , SpinnerService} from '../../services';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-special-request',
  templateUrl: './special-request.component.html',
  styleUrls: ['./special-request.component.css']
})
export class SpecialRequestComponent implements OnInit {
  pagination:any;
  itemsPerPage: number;
  totalItems: any;
  page = 1; 
  previousPage: any;
  list:any;
  total= 22;
  per_page =15;
  closeResult = '';
  details : any;
  constructor(private SpecialRequestService:SpecialRequestService, private modalService: NgbModal,
    private SpinnerService:SpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.loadData()
  }
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
       this.loadData();
    }
  }
 
  loadData()
  {
   
    this.SpecialRequestService.getallRequests(this.previousPage).subscribe(data => {
      this.list =data.data ;
      // this.child =data.data.child ;
      // this.parent =data.data.parent ; 
       this.pagination =data.meta   })
  }
  open(content , id) {
    this.SpecialRequestService.getRequestById(id).subscribe(data => {
      this.details =data.data ;  })
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  

}
