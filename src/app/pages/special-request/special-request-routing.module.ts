import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecialRequestComponent } from './special-request.component';


const routes: Routes = [{ path: '', component: SpecialRequestComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialRequestRoutingModule { }
