import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactService } from '../../services/contact/contact.service';
import { SpinnerService, ListService } from '../../services';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ToastService } from '../../services/toast/toast.service';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { MatOption } from '@angular/material/core';

@Component({
  selector: 'app-listdetails',
  templateUrl: './listdetails.component.html',
  styleUrls: ['./listdetails.component.css']
})
export class ListdetailsComponent implements OnInit {
  GuardianForm: FormGroup;
  submitted = false;
  pagination: any;
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  previousPage: any;
  list: any;
  total = 22;
  per_page = 15;
  searchText = '';
  searchTextlang = '';
  characters: any = [];
  closeResult = '';
  details: any;
  addClient: FormGroup;
  addFormLists: FormGroup;
  loading = false;
  AllLists: any;
  updateSuccesfuly: string;
  Languages: any;
  deleteSucc: any;
  @ViewChild('allSelected') private allSelected: MatOption;
  selectAllCustomer: boolean;

  constructor(private ContactService: ContactService, private modalService: NgbModal,
    private SpinnerService: SpinnerService,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private ListService: ListService) { }

  ngOnInit(): void {
    // this.ListService.getLanguage().subscribe(data => { this.Languages = data.data; });
    this.SpinnerService.loader()
    this.loadData()
    this.getAllApplications()
    this.getAllLists()
    this.addClient = this.formBuilder.group({
      level: [],
      name: [],
      language: []
    });
    this.GuardianForm = this.formBuilder.group({
      nationality: ['', Validators.required],
      language: ['', Validators.required],
    });
  }
  toggleAllSelection() {
    if (this.allSelected.selected) {
      this.selectAllCustomer = true
      this.addClient.controls.language
        .patchValue([...this.Languages.map(item => item.email), 0]);
    } else {
      this.selectAllCustomer = false
      this.addClient.controls.language.patchValue([]);
    }
  }
  selectCharacter(char) {
    this.searchText = char
  }
  get f() { return this.addClient.controls; }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }
  get k() { return this.addClient.controls; }
  getAllLists() {
    const id = this.route.snapshot.paramMap.get('id');
    this.ListService.getListByID(id).subscribe(data => {
      this.AllLists = data.data.users;
      var arr = [];
      for (var j = 0; j < this.AllLists.length; j++) {

        console.log(this.AllLists[j].email);
        arr.push(this.AllLists[j].email)
        console.log(arr);
      }
      this.addClient.patchValue({
        language: arr


      })
      //  this.pagination = data.meta;
      //  this.total = data.meta.total;
      //  this.per_page = data.meta.per_page
    })
  }

  getAllApplications() {
    this.ListService.AllUsers().subscribe(data => {
      this.Languages = data.data;
      //  this.pagination = data.meta;
      //  this.total = data.meta.total;
      //  this.per_page = data.meta.per_page
    })
  }
  loadData() {
    this.ContactService.contact(this.previousPage).subscribe(data => {
      this.list = data.data; this.pagination = data.meta
    })
  }
  delete(content, email) {
    console.log("email" + email)
    const id = this.route.snapshot.paramMap.get('id');
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.ListService.deleteUser(id, { emails: email })
        .pipe(first())
        .subscribe(
          data => {

            // this.getAllLists()
            this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
            this.showDanger(this.deleteSucc)
          },

          error => {
            // alert(JSON.stringify(error.error))
            //this.showDanger(error.error.data.errors)
            // this.alertService.error(this.error);

          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  onSubmit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.loading = true;
    var arr = this.f.language.value
    const index = arr.indexOf(0)
    if (index > -1) {
      arr.splice(index, 1)
    }
    else {
    }



    this.ListService.addUserToList(id, { emails: arr })
      .pipe(first())
      .subscribe(
        data => {
          this.getAllLists()
          this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          this.loading = false
          this.showSuccess(this.updateSuccesfuly)
          // this.addFormLists.reset()

        },
        error => {
          console.log(JSON.stringify(error))
          this.loading = false
          this.showDanger(error.error.data.errors)

        });
  }
  addFormlist(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {




      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
