import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListdetailsRoutingModule } from './listdetails-routing.module';
import { ListdetailsComponent } from './listdetails.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from '../../material.module';
import { TranslateLoader, TranslateModule,} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient} from '@angular/common/http';
// AoT requires an exported function for factories
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms'; 
import { FilterPipe } from '../../helpers/filter.pipe';

export function childLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "./assets/i18n/", ".json");
}


@NgModule({
  declarations: [ListdetailsComponent ,FilterPipe],
  imports: [
    CommonModule,
    ListdetailsRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MaterialModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: childLoaderFactory,
        deps: [HttpClient]
      },
      extend: true,
    }),
  ],
  exports:[FilterPipe]
})
export class ListdetailsModule { }
