import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListdetailsComponent } from './listdetails.component';


const routes: Routes = [{ path: '', component: ListdetailsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListdetailsRoutingModule { }
