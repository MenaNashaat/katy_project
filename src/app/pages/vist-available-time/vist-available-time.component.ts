import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbTimepicker } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { VisitService , SpinnerService } from '../../services';
import { first } from 'rxjs/operators';
import { Visit, Tabel } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import { NgbTimeStruct, NgbTimeAdapter } from '@ng-bootstrap/ng-bootstrap';

const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;
@Component({
  selector: 'app-vist-available-time',
  templateUrl: './vist-available-time.component.html',
  styleUrls: ['./vist-available-time.component.css'],
  providers: [{ provide: NgbTimeAdapter, useClass: VistAvailableTimeComponent }]
})
export class VistAvailableTimeComponent implements OnInit {
  arr_addTime: { name: any; available: {}; };
  fromModel(value: string | null): NgbTimeStruct | null {
    if (!value) {
      return null;
    }
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }
  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}:${pad(time.second)}` : null;
    console.log("time" + JSON.stringify(time))
  }
  private timeAsString = '01:00:00';

  times: any;
  closeResult = '';
  level: any;
  addForm: FormGroup;
  editForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  levelID: any
  AvailableTime: any;
  //translation
  Accept: string;
  timeFrom;
  timeTo;

  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private VisitService: VisitService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private SpinnerService:SpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAvailableTime();
    this.addForm = this.formBuilder.group({
      From: ['', Validators.required],
      To: ['', Validators.required],
      max_visits: ['', Validators.required],
    });
    this.editForm = this.formBuilder.group({
      from: ['', Validators.required],
      to: ['', Validators.required],
      max_visits: ['', Validators.required],
    });

  }
  get f() { return this.addForm.controls; }
  get e() { return this.editForm.controls; }


  getAvailableTime() {
    this.VisitService.calendar().subscribe(data => {
      this.times = data.data;
      console.log("this.times" + JSON.stringify(this.times));
    })
  }

  open(content, name) {

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      const jsObjectsAdd = this.times
      var resultTimeadd = jsObjectsAdd.find(obj => {
        return obj.name === name
      })
      var target = this.addForm.value;
      const newObjectAdd = { from: target.From, to: target.To , max_visits: target.max_visits  }
      var source = resultTimeadd.available;

      var arr = []
      for (var j = 0; j < source.length; j++) {

        var append = { "from": source[j].from, "to": source[j].to , "max_visits":source[j].max_visits }
        arr.push(append)
      }


      console.log("newObjectAdd" + JSON.stringify(newObjectAdd));
      console.log("source" + JSON.stringify(source));
      const returnedTarget = arr.push(newObjectAdd)
      console.log("source" + JSON.stringify(source));
      console.log("returnedTarget" + JSON.stringify(returnedTarget));

      this.VisitService.addTime({ name: name, available: arr })
        .pipe(first())
        .subscribe(
          data => {

            this.getAvailableTime()
            console.log("yesssssssssssssssssss")
            // this.showSuccess(this.updateSuccesfuly)
            //this.LevelForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            // this.showDanger(error.error.data.errors)
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  edit(content, id, name) {
    var arr= []
    const jsObjects = this.times
    var resultTime = jsObjects.find(obj => {
      return obj.name === name
    })
    const jsAvailableObj = resultTime.available
    var resultAvailable = jsAvailableObj.find(obj => {
      return obj.id === id
    })

    // var OtherresultAvailable = jsAvailableObj.find(obj => {
    //   return obj.id != id
    // })
    // alert("OtherresultAvailable"+JSON.stringify(OtherresultAvailable))
    // arr.push(OtherresultAvailable)
    this.timeFrom = resultAvailable.from;
    this.timeTo = resultAvailable.to;
    this.editForm.patchValue({
      max_visits: resultAvailable.max_visits
    });

    
    var sourceEdit = resultTime.available;
    var koko = resultTime.available;


    // for( var i = 0; i < sourceEdit.length; i++)
    // { if ( sourceEdit[i] === resultAvailable) { sourceEdit.splice(i, 1); 
    //   console.log("this.tddddddddddddddddddimes" + JSON.stringify(sourceEdit));}}

    
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      // var target = this.addForm.value;
      // const source = resultTime.available;
      for (var i = 0; i < koko.length; i++) {
        if (koko[i].id == id) {
          koko.splice(i, 1);
         
          break;
        }
      }
      console.log("sourceEdit" + JSON.stringify(koko))
      const newObject = this.editForm.value
     
      // arr.push(newObject)



      var arrEdit = []
      for (var x = 0; x < koko.length; x++) {
        var append = { "from": koko[x].from, "to": koko[x].to , 'max_visits' : koko[x].max_visits}
        arrEdit.push(append)
      }

      arrEdit.push(newObject)

     
    

      this.VisitService.addTime({ name: name, available: arrEdit })
        .pipe(first())
        .subscribe(
          data => {

            this.getAvailableTime()

            // this.showSuccess(this.updateSuccesfuly)
            //this.LevelForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            // this.showDanger(error.error.data.errors)
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  delete(content, id, name) {
   

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
     console.log("result" +result)
      const jsObjects = this.times
      var resultTime = jsObjects.find(obj => {
        return obj.name === name
      })
     
      var sourceEdit = resultTime.available;
     
      for (var i = 0; i < sourceEdit.length; i++) {
        console.log("idddddddddd" + id)
        if (sourceEdit[i].id == id) {
          sourceEdit.splice(i, 1);
         
          break;
        }
      }
      // const source = resultTime.available;
      
      var arrEdit = []
      for (var x = 0; x < sourceEdit.length; x++) {
       
        var append = { "from": sourceEdit[x].from, "to": sourceEdit[x].to }
        arrEdit.push(append)
       
      }
      

      this.VisitService.addTime({ name: name, available: arrEdit })
        .pipe(first())
        .subscribe(
          data => {

            this.getAvailableTime()

            // this.showSuccess(this.updateSuccesfuly)
            //this.LevelForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            // this.showDanger(error.error.data.errors)
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


}

export class NgbdTimepickerAdapter {
  time: '00:00:00';
  to: '00:00:00';
}