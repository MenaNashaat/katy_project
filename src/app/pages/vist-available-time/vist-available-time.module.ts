import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VistAvailableTimeRoutingModule } from './vist-available-time-routing.module';
import { VistAvailableTimeComponent } from './vist-available-time.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from '../../material.module';
import { TranslateLoader, TranslateModule,} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient} from '@angular/common/http';
// AoT requires an exported function for factories
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

export function childLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [VistAvailableTimeComponent],
  imports: [
    CommonModule,
    VistAvailableTimeRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    MaterialModule,
    FormsModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: childLoaderFactory,
        deps: [HttpClient]
      },
      extend: true,
    }),
  ]
})
export class VistAvailableTimeModule { }
