import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VistAvailableTimeComponent } from './vist-available-time.component';


const routes: Routes = [{ path: '', component: VistAvailableTimeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VistAvailableTimeRoutingModule { }
