import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistAvailableTimeComponent } from './vist-available-time.component';

describe('VistAvailableTimeComponent', () => {
  let component: VistAvailableTimeComponent;
  let fixture: ComponentFixture<VistAvailableTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistAvailableTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistAvailableTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
