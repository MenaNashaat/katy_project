import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  ElementRef,
  OnInit, AfterViewInit
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from 'date-fns';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { EventService } from '../../services/index';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import {SpinnerService,  LevelService } from '../../services';
import { first } from 'rxjs/operators';
import { TranslateService } from "@ngx-translate/core";
import { environment } from '../../../environments/environment';

import {


  startOfMonth,
  startOfWeek,
  endOfWeek,

  format,
} from 'date-fns';
import { NgbDateStruct, NgbCalendar, NgbTimeAdapter, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';

interface Film {
  id: number;
  title: string;
  release_date: string;
  data: any;
  event_date: string;
  description: string;
  cost: number;
  type: string;
  class:any
}

function getTimezoneOffsetString(date: Date): string {
  const timezoneOffset = date.getTimezoneOffset();
  const hoursOffset = String(
    Math.floor(Math.abs(timezoneOffset / 60))
  ).padStart(2, '0');
  const minutesOffset = String(Math.abs(timezoneOffset % 60)).padEnd(2, '0');
  const direction = timezoneOffset > 0 ? '-' : '+';

  return `T00:00:00${direction}${hoursOffset}:${minutesOffset}`;
}
const pad = (i: number): string => i < 10 ? `0${i}` : `${i}`;
const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'app-event',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
  providers: [{ provide: NgbTimeAdapter, useClass: EventComponent }]

})
export class EventComponent implements OnInit {
  addSuccesfuly: string;
  closeResult = '';
  eventForm: FormGroup;
  timeFrom: any;
  loading = false;
  model: NgbDateStruct;
  submitted = false;
  returnUrl: string;
  error: string;
  class: any;
  view: CalendarView = CalendarView.Month;

  viewDate: Date = new Date();

  events$: Observable<CalendarEvent<{ film: Film }>[]>;

  activeDayIsOpen: boolean = false;
  arr: any;
  showClass: boolean = false;
  SchoolPayment: boolean = false;
  cost: any;
  tripID: number;
  fromModel(value: string | null): NgbTimeStruct | null {
    if (!value) {
      return null;
    }
    const split = value.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }
  toModel(time: NgbTimeStruct | null): string | null {
    return time != null ? `${pad(time.hour)}:${pad(time.minute)}` : null;
    
  }
  private timeAsString = '01:00:00';
  constructor(private http: HttpClient, private modal: NgbModal, private EventService: EventService, private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService, private TranslateService: TranslateService,
    private SpinnerService:SpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.fetchEvents();
    this.getClass();
    this.eventForm = this.formBuilder.group({
      title: ['', Validators.required],
      type: ['', Validators.required],
      description: ['', Validators.required],
      cost: ['', Validators.required],
      event_date: ['', Validators.required],
      from: ['', Validators.required],
      class_id: ['', Validators.required],
      is_trip: [false, Validators.requiredTrue],
    });

  }
  mena() {
    this.events$ = this.http
      .get(`${environment.apiUrl}event?limit=100`)
      .pipe(
        map(({ data }: { data: Film[] }) => {
          return data.map((film: Film) => {
            //       var arr = ,   , "cost": foo[i].cost ,   "type": foo[i].type , actions: this.actions}

            return {
              id: film.id,
              description: film.description,
              cost: film.cost,
              type: film.type,
              title: film.title,
              class: film.class,
              start: new Date(
                film.event_date
              )
            };
          });
        })
      );
  }
  fetchEvents(): void {
    const getStart: any = {
      month: startOfMonth,
      week: startOfWeek,
      day: startOfDay,
    }[this.view];

    const getEnd: any = {
      month: endOfMonth,
      week: endOfWeek,
      day: endOfDay,
    }[this.view];



    this.events$ = this.http
      .get(`${environment.apiUrl}event?limit=100`)
      .pipe(
        map(({ data }: { data: Film[] }) => {
          return data.map((film: Film) => {
            //       var arr = ,   , "cost": foo[i].cost ,   "type": foo[i].type , actions: this.actions}

            return {
              id: film.id,
              description: film.description,
              cost: film.cost,
              type: film.type,
              title: film.title,
              class: film.class,
              start: new Date(
                film.event_date
              )
            };
          });
        })
      );
  }


  dayClicked({
    date,
    events,
  }: {
    date: Date;
    events: CalendarEvent<{ film: Film }>[];
  }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }
  get f() { return this.eventForm.controls; }
  selectChangeHandler($event) {
    if ($event.target.value == 'private') {
      this.showClass = true
      
    }
    else {
      this.showClass = false
    }

  }
  toggleEditable(event) {
    if (event.target.checked) {
      this.SchoolPayment = true;
     
    }
    else
    {
      this.SchoolPayment = false;
     
    }
  }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  delete(content, eventToDelete: CalendarEvent, EventId) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {



      this.EventService.deleteEvent(EventId)
        .pipe(first())
        .subscribe(
          data => {


            this.TranslateService.get('backend.ASuccesfuly').subscribe((Succesfuly: string) => { this.addSuccesfuly = Succesfuly;  });
            this.showSuccess(this.addSuccesfuly)
            this.mena();
          },
          error => {
            this.showDanger(error.error.data.errors)
            // this.loading = false;
          });
      this.mena();
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  getClass() {
    this.EventService.getclass().subscribe(data => { this.class = data.data });
  }
  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      let date_of_event = this.f.event_date.value
      if (date_of_event != null && date_of_event != '') {
        if (date_of_event.month.toString().length < 2) {
          date_of_event.month = "0" + date_of_event.month;
        }
        else {
          date_of_event.month = date_of_event.month;
        }
        if (date_of_event.day.toString().length < 2) {
          date_of_event.day = "0" + date_of_event.day;
        }
        else {
          date_of_event.day = date_of_event.day;
        }
      }

      let date: any = date_of_event.year + "-" + date_of_event.month + "-" + date_of_event.day
      
      if (this.SchoolPayment == true)
      {
        this.cost= this.f.cost.value
      }
      else 
      {
        
        this.cost= '0'
      }
      if (this.f.is_trip.value  == false)
      {
       
       this.tripID = 0
      }
      if (this.f.is_trip.value  == true)
      {
       this.tripID = 1
     
      }
      if (this.f.type.value == 'private')
      {
        
        this.arr = { "event_date": date + ' ' + this.f.from.value, "class_id": this.f.class_id.value, title: this.f.title.value, type: this.f.type.value, cost: this.cost , description: this.f.description.value , is_trip: this.tripID }
      }
      if (this.f.type.value == 'public')
      {
        this.arr = { "event_date": date + ' ' + this.f.from.value,  title: this.f.title.value, type: this.f.type.value, cost: this.cost , description: this.f.description.value , is_trip: this.tripID }
      }
      this.EventService.createEvent(this.arr)
        .pipe(first())
        .subscribe(
          data => {

            this.fetchEvents();

            this.TranslateService.get('backend.ASuccesfuly').subscribe((Succesfuly: string) => { this.addSuccesfuly = Succesfuly;  });
            this.showSuccess(this.addSuccesfuly)
            this.SchoolPayment = false;
            this.showClass = false
            this.eventForm.reset()
          },
          error => {
            this.SchoolPayment = false;
            this.showClass = false
            this.eventForm.reset()
            this.showDanger(error.error.data.errors)
            // this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.eventForm.reset()
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }



}
export class NgbdTimepickerAdapter {
  time: '00:00';
  to: '00:00';
}