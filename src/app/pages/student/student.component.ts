import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { StudentService, SpinnerService } from '../../services';
import { first } from 'rxjs/operators';
import { listAplications, Class, Tabel } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  students: listAplications;
  studentsPending: listAplications;
  studentDetails: listAplications;
  studentsUnassigned: listAplications;
  extraHour: listAplications;
  class: Class;
  closeResult = '';
  level: any;
  assignForm: FormGroup;
  statusForm: FormGroup;
  filterForm: FormGroup;
  filterFormExtra: FormGroup;
  filterFormWinter: FormGroup;
  filterFormSummer: FormGroup;
  filterFormDeactivated: FormGroup;
  filterFormUnassigned: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  levelID: any
  //  pagination
  pagination: Tabel;
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  per_page: Tabel;
  total: Tabel;
  previousPage: any;

  page2 = 1;
  per_page2: Tabel;
  total2: Tabel;
  previousPage2: any;

  page3 = 1;
  per_page3: Tabel;
  total3: Tabel;
  previousPage3: any;

  page4 = 1;
  per_page4: Tabel;
  total4: Tabel;
  previousPage4: any;

  page5 = 1;
  per_page5: Tabel;
  total5: Tabel;
  previousPage5: any;

  page6 = 1;
  per_page6: Tabel;
  total6: Tabel;
  previousPage6: any;
  //translation
  Accept: string;
  updateSuccesfuly: string;
  branchId: any;
  level_Id: any;
  studentsWinterc: any;
  studentsSummerc: any;

  isChecked = true;
  ActiveBTNWinter: FormGroup;
  ActiveBTNSummer: FormGroup;
  monthList: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  showMonthWinter: boolean;
  useDefault: false;
  summerWinterCamp: any;
  showMonthSummer: boolean;
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private StudentService: StudentService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private SpinnerService: SpinnerService,

  ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    // this.getSetting()
    this.getAllStudents()
    this.getAllPending()
    this.getAllUnassigned()
    this.getExtraHour()
    this.filterWinterStudent()
    this.filterSummerStudent()
    this.getWinterCampMonths()
    this.getSummerCampMonths()
    this.StudentService.getLevel().subscribe(data => {
      this.level = data.data;

    });
    this.assignForm = this.formBuilder.group({
      class_id: ['', Validators.required],
    });
    this.statusForm = this.formBuilder.group({
      status: ['', Validators.required],
    });
    this.filterForm = this.formBuilder.group({
      name: ['', Validators.required],
    });
    this.filterFormDeactivated = this.formBuilder.group({
      name: ['', Validators.required],
    });
    this.filterFormUnassigned = this.formBuilder.group({
      name: ['', Validators.required],
    });
    this.filterFormExtra = this.formBuilder.group({
      name: ['', Validators.required],
    });

    this.filterFormWinter = this.formBuilder.group({
      name: ['', Validators.required],
    });
    this.filterFormSummer = this.formBuilder.group({
      name: ['', Validators.required],
    });
    this.ActiveBTNWinter = this.formBuilder.group({
      month: '',
      enableWinterCamp: ['', Validators.requiredTrue]
    });
    this.ActiveBTNSummer = this.formBuilder.group({
      month: '',
      enableSummerCamp: ['', Validators.requiredTrue]
    });
  }


  get f() { return this.assignForm.controls; }
  get s() { return this.statusForm.controls; }
  get a() { return this.filterForm.controls; }
  get d() { return this.filterFormDeactivated.controls; }
  get u() { return this.filterFormUnassigned.controls; }
  get e() { return this.filterFormExtra.controls; }
  get w() { return this.filterFormWinter.controls; }
  get m() { return this.filterFormSummer.controls; }
  get AW() { return this.ActiveBTNWinter.controls; }
  get AS() { return this.ActiveBTNSummer.controls; }

  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllStudents() {
    this.StudentService.getStudent(this.previousPage).subscribe(data => {
      this.students = data.data;
      this.pagination = data.meta;
      this.total = data.meta.total;
      this.per_page = data.meta.per_page
    })

  }
  getExtraHour() {
    this.StudentService.getextraHour(this.previousPage2).subscribe(data => {
      this.extraHour = data.data;

      this.total2 = data.meta.total;
      this.per_page2 = data.meta.per_page
    })

  }
  getAllPending() {
    this.StudentService.getStudentPending(this.previousPage3).subscribe(data => {
      this.studentsPending = data.data;

      this.total3 = data.meta.total;
      this.per_page3 = data.meta.per_page
    })
  }
  filterWinterStudent() {
    this.StudentService.getStudentWinter(this.previousPage5).subscribe(data => {
      this.studentsWinterc = data.data;
      this.total5 = data.meta.total;
      this.per_page5 = data.meta.per_page
    })
  }
  filterSummerStudent() {
   
    this.StudentService.getStudentSummer(this.previousPage6).subscribe(data => {
      this.studentsSummerc = data.data;
      this.total6 = data.meta.total;
      this.per_page6 = data.meta.per_page
    })
  }

  getAllUnassigned() {
    
    this.StudentService.getUnassigned(this.previousPage4).subscribe(data => {
      this.studentsUnassigned = data.data;
      this.total4 = data.meta.total;
      this.per_page4 = data.meta.per_page
    })
  }
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;

      this.getAllStudents();
    }
  }
  loadPageExtra(page2: number) {
    if (page2 !== this.previousPage2) {
      this.previousPage2 = page2;

      this.getExtraHour();
    }
  }
  loadPageDeactive(page3: number) {
    if (page3 !== this.previousPage3) {
      this.previousPage3 = page3;
      this.getAllPending();
    }
  }
  loadPageUnassign(page4: number) {
    if (page4 !== this.previousPage4) {
      this.previousPage4 = page4;
      this.getAllUnassigned();
    }
  }
  filterActive() {
    this.StudentService.filterActiveStudent(this.previousPage, this.a.name.value).subscribe(data => {
      this.students = data.data;
      this.pagination = data.meta;
      this.total = data.meta.total;
      this.per_page = data.meta.per_page
    })
  }
  filterDeactivated() {
    this.StudentService.filterDeactivatedStudent(this.previousPage3, this.d.name.value).subscribe(data => {
      this.studentsPending = data.data;
      this.total3 = data.meta.total;
      this.per_page3 = data.meta.per_page
    })
  }
  filterUnassigned() {
    this.StudentService.filterUnassignedStudent(this.previousPage4, this.u.name.value).subscribe(data => {
      this.studentsUnassigned = data.data; 
      this.total4 = data.meta.total;
      this.per_page4 = data.meta.per_page
    })
  }
  filterExtra() {
    this.StudentService.filterExtraStudent(this.previousPage2, this.e.name.value).subscribe(data => {
      this.extraHour = data.data;
      this.total2 = data.meta.total;
      this.per_page2 = data.meta.per_page
    })
  }
  filterWinter() {
    this.StudentService.filterWinterStudent(this.previousPage2, this.w.name.value).subscribe(data => {
      this.studentsWinterc = data.data;
      this.total5 = data.meta.total;
      this.per_page5 = data.meta.per_page
    })
  }
  filterSummer() {
    this.StudentService.filterSummerStudent(this.previousPage2, this.m.name.value).subscribe(data => {
      this.studentsSummerc = data.data;
      this.total6 = data.meta.total;
      this.per_page6 = data.meta.per_page
    })
  }
  open(content, id) {
    this.StudentService.getStudentById(id).subscribe(data => {
      this.studentDetails = data.data;
      // this.branchId = data.data.child.application.branch_id
      // this.StudentService.getClassByBranch(this.branchId).subscribe(data =>{
      //   this.class = data.data
      // })
      this.level_Id = data.data.child.application.pivot.child_level_id
      this.StudentService.getClass(this.level_Id).subscribe(data => {
        this.class = data.data
      })
      // const jsObjects = this.level
      // var result = jsObjects.find(obj => {
      //   return obj.name === this.studentDetails.child.level
      // })
      // console.log("result.id" + result.id)
      //this.levelID = result.id;
      // this.StudentService.getClass(this.levelID).subscribe(data => { this.class = data.data;  })
    })



    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      const target = this.assignForm.value;
      const source = { "student_id": this.studentDetails.id };

      const returnedTarget = Object.assign(target, source);
      this.translate.get('backend.Studentassign').subscribe((accept: string) => { this.Accept = accept });
      this.StudentService.assignStudent(returnedTarget)
        .pipe(first())
        .subscribe(
          data => {

            this.getAllUnassigned()
            this.getAllStudents();

            this.showSuccess(this.Accept)
            this.assignForm.reset()
          },
          error => {

            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  EditClass(content, level_id, id) {

    this.StudentService.getClass(level_id).subscribe(data => { this.class = data.data; })




    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

      this.StudentService.upgrade({ class_id: this.f.class_id.value, student_ids: [id] })
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });


            this.getAllStudents();

            this.showSuccess(this.updateSuccesfuly)
            this.assignForm.reset()
          },
          error => {

            this.assignForm.reset()
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.assignForm.reset()
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  edit(content, classId, studentId) {

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });

      this.StudentService.studentStatus(studentId, { status: this.s.status.value, class_id: classId })
        .pipe(first())
        .subscribe(
          data => {

            this.getAllStudents();
            this.getAllPending();
            this.getAllUnassigned();
            this.getExtraHour();
            this.showSuccess(this.updateSuccesfuly)
            this.statusForm.reset()
          },
          error => {
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  toggleWinterBtn(event) {

    if (event.checked == true) {

      this.showMonthWinter = true
      this.useDefault = event.checked;
      this.StudentService.wintercampToggle({ 'winterCamp': true })
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });

            this.showSuccess(this.updateSuccesfuly)
           
          },
          error => {
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
    }
    else if (event.checked == false) {

      this.showMonthWinter = false
      this.useDefault = event.checked;
      this.StudentService.wintercampToggle({ 'winterCamp': false })
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });

            this.showSuccess(this.updateSuccesfuly)
           
          },
          error => {
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
    }

  }
  toggleSummerBtn(event) {

    if (event.checked == true) {

      this.showMonthSummer = true
      this.useDefault = event.checked;
      this.StudentService.summercampToggle({ 'summerCamp': true })
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });

            this.showSuccess(this.updateSuccesfuly)
           
          },
          error => {
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
    }
    else if (event.checked == false) {

      this.showMonthSummer = false
      this.useDefault = event.checked;
      this.StudentService.summercampToggle({ 'summerCamp': false })
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });

            this.showSuccess(this.updateSuccesfuly)
            
          },
          error => {
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
    }

  }
  getSetting() {

    this.StudentService.getSetting()
      .pipe(first())
      .subscribe(
        data => {

          var setting = data.data
          this.summerWinterCamp = setting.summerWinterCamp
          if (this.summerWinterCamp == '1') {
            this.showMonthWinter = true
          }
          else {
            this.showMonthWinter = false
          }


        },
        error => {
          this.showDanger(error.error.data.errors)
          this.loading = false;
        });


  }
  getWinterCampMonths() {

    this.StudentService.getWinterCampMonths()
      .pipe(first())
      .subscribe(
        data => {

          var WinterCampMonths = data.data

          var arr = [];
          for (var j = 0; j < WinterCampMonths.length; j++) {

            console.log(WinterCampMonths[j].month);
            arr.push(WinterCampMonths[j].month)
            console.log(arr);
          }
          this.StudentService.getSetting()
            .pipe(first())
            .subscribe(
              data => {

                var setting = data.data
                var campvalue
                this.summerWinterCamp = setting.winterCamp
                if (setting.winterCamp == '1') {
                  this.showMonthWinter = true
                  campvalue = true
                }
                else {
                  this.showMonthWinter = false
                  campvalue = false
                }
                this.ActiveBTNWinter.patchValue({
                  month: arr,
                  enableWinterCamp: campvalue


                })


              },
              error => {
                this.showDanger(error.error.data.errors)
                this.loading = false;
              });


        },
        error => {
          this.showDanger(error.error.data.errors)
          this.loading = false;
        });


  }
  onFormSubmit() {
    // if (this.ActiveBTNWinter.value == true)
    // {

    // }
    this.StudentService.winterCampMonths({ 'months': this.ActiveBTNWinter.value.month })
      .pipe(first())
      .subscribe(
        data => {
          this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });

          this.showSuccess(this.updateSuccesfuly)

        },
        error => {
          this.showDanger(error.error.data.errors)
          this.loading = false;
        });

    //alert(JSON.stringify(this.AW.month.value));

  }
  getSummerCampMonths() {

    this.StudentService.getSummerCampMonths()
      .pipe(first())
      .subscribe(
        data => {

          var SummerCampMonths = data.data

          var arr = [];
          for (var j = 0; j < SummerCampMonths.length; j++) {

            arr.push(SummerCampMonths[j].month)
          }
          this.StudentService.getSetting()
            .pipe(first())
            .subscribe(
              data => {

                var setting = data.data
                var campvalue
                this.summerWinterCamp = setting.summerCamp
                if (setting.summerCamp == '1') {
                  this.showMonthSummer = true
                  campvalue = true
                }
                else {
                  this.showMonthSummer = false
                  campvalue = false
                }
                this.ActiveBTNSummer.patchValue({
                  month: arr,
                  enableSummerCamp: campvalue
                })


              },
              error => {
                this.showDanger(error.error.data.errors)
                this.loading = false;
              });
        },
        error => {
          this.showDanger(error.error.data.errors)
          this.loading = false;
        });


  }
  onFormSubmitSummer() {

    this.StudentService.summerCampMonths({ 'months': this.ActiveBTNSummer.value.month })
      .pipe(first())
      .subscribe(
        data => {
          this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; });

          this.showSuccess(this.updateSuccesfuly)

        },
        error => {
          this.showDanger(error.error.data.errors)
          this.loading = false;
        });

    //alert(JSON.stringify(this.AW.month.value));

  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
