import { Component, OnInit , ViewChild} from '@angular/core';
import {MatAccordion} from '@angular/material/expansion';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { MealService , MedicalService , SpinnerService} from '../../services';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-medical',
  templateUrl: './medical.component.html',
  styleUrls: ['./medical.component.css']
})
export class MedicalComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  closeResult = '';
  meals: Meal;
  loading = false;
  submitted = false;
  questionForm: FormGroup;
  editMedicalForm: FormGroup;
  selectedFile: File;
  deleteSucc: string;

  // selectedFile: File;
  updateSuccesfuly: string;
  addSuccesfuly:string;  panelOpenState = false;
  FAQ: any;
  medicalQ: any;
  medicalDetails: any;
  constructor(
   
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private mealService: MealService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private MedicalService:MedicalService,
    private SpinnerService:SpinnerService
    ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllMedical()
    
    this.questionForm = this.formBuilder.group({
      question: [''],
      ar_question: [''],
    });
    this.editMedicalForm = this.formBuilder.group({
      question: [''],
      ar_question: [''],
    });
    
  }
  

   // convenience getter for easy access to form fields
   get f() { return this.questionForm.controls; }
   get g() { return this.editMedicalForm.controls; }
  
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllMedical() {
    
    this.MedicalService.getMedical().subscribe(data => {
      this.medicalQ = data.data;
      console.log("this.FAQ" + JSON.stringify(this.FAQ))
    })
  }
  
  delete(content, questionID ) {
   

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.MedicalService.deleteMedical(questionID)
      .pipe(first())
      .subscribe(
        data => {
         
          this.getAllMedical()
          this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
          this.showSuccess(this.updateSuccesfuly)
        },

        error => {
          alert(error.error.data.errors)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);
          this.showDanger(error.error.data.errors)


        });
      

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
 

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      
    console.log("{question: this.f.question.value, ar_question: this.f.ar_question.value}"+ JSON.stringify(this.questionForm.value))
      this.MedicalService.createMedical( {'question': this.f.question.value , 'question_ar': this.f.ar_question.value})
        .pipe(first())
        .subscribe(
          data => {
           
            this.getAllMedical()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
            this.showSuccess(this.updateSuccesfuly)
            this.questionForm.reset()
           
          },
          error => {
            console.log(JSON.stringify(error.error.data.errors))
            this.showDanger(error.error.data.errors)
            this.loading = false;
            this.questionForm.reset()
           
          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  edit(content, id) {
    
    this.MedicalService.getMedical().subscribe(data => {
     
      const jsObjects = this.medicalQ   
      var result = jsObjects.find(obj => {
        return  obj.id == id
      }) 
      this.medicalDetails = result
      this.editMedicalForm.patchValue({

        ar_question: this.medicalDetails.question_ar,
        question: this.medicalDetails.question,
       
      })
    })
  
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

      // stop here if form is invalid
      if (this.editMedicalForm.invalid) {
        this.showDanger("Invalid Field")
        return;
      }
      this.MedicalService.editMedical(  {'question': this.g.question.value , 'question_ar': this.g.ar_question.value}, id)
        .pipe(first())
        .subscribe(
          data => {
           
            this.getAllMedical()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
            this.showSuccess(this.updateSuccesfuly)
            this.questionForm.reset()
            this.editMedicalForm.reset()
          },
          error => {
            console.log(JSON.stringify(error.error.data.errors))
            this.showDanger(error.error.data.errors)
            this.loading = false;
            this.questionForm.reset()
            this.editMedicalForm.reset()
          });
      

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
