import { Component, OnInit } from '@angular/core';
import {ContactService} from '../../services/contact/contact.service';
import {SpinnerService} from '../../services';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  pagination:any;
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  previousPage: any;
  list:any;
  total= 22;
  per_page =15;
  closeResult = '';
  details : any;
  constructor(private ContactService:ContactService, private modalService: NgbModal,
    private SpinnerService:SpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.loadData()
  }
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
       this.loadData();
    }
  }
 
  loadData()
  {
    console.log("this.page" +this.page);
    this.ContactService.contact(this.previousPage).subscribe(data => {
      this.list =data.data ;  this.pagination =data.meta   })
  }
  open(content , id) {
    this.ContactService.getContactById(id).subscribe(data => {
      this.details =data.data ;   console.log("data" + JSON.stringify(data))})
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
