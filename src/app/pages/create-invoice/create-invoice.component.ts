import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AssignToPlanService } from '../../services';
import { ListApplicationsService } from '../../services/list-applications.service';
import { ToastService } from '../../services/toast/toast.service';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { NgbTimeAdapter, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.css']
})
export class CreateInvoiceComponent implements OnInit {
  assignForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  date: { year: number, month: number };
  model: NgbDateStruct;
  plans: any;
  addSuccesfuly: string;
  applicationId: string;
  parent_id: string;
  student_id: any;
  students: any;
  studentId:any
  is_vat_includedAdd: number;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private AssignToPlanService: AssignToPlanService,
    private toastService: ToastService,
    private translate: TranslateService,
    private ListApplicationsService: ListApplicationsService) { }

  ngOnInit(): void {
    this.applicationId = this.route.snapshot.paramMap.get('id');
    this.getAllPlan()
    this.getStudentID()
    this.ListApplicationsService.sharedMessage.subscribe(data => this.studentId = data)
    this.assignForm = this.formBuilder.group({
      amount: ['', Validators.required],
      student_id:['', Validators.required],
      event_date:['', Validators.required],
      description: ['', Validators.required],
      acceptTerms: ['', Validators.required],
    });
  }
  showSuccess(test) {
    this.toastService.show(test, { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(test) {
    this.toastService.show(test, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllPlan() {
    this.AssignToPlanService.getPlans().subscribe(data => { this.plans = data.data; console.log(JSON.stringify(this.plans)) })
  }
getStudentID(){

  const id = this.route.snapshot.paramMap.get('id');
  this.ListApplicationsService.getRegisterById(id).subscribe(
    data => {
     
      this.parent_id = data.data.parent.id,

        // get student id
        this.AssignToPlanService.getStudentId(this.parent_id)
          .pipe(first())
          .subscribe(
            data => {
              this.students = data.data
                console.log(" this.students" + JSON.stringify( data))
              
            },
            error => {
              this.showDanger(error.error.data.errors)
              // this.alertService.error(this.error);
              this.loading = false;
            });
      // end


    },
    error => {
      alert(error.error.data.errors)

    });


}
  // convenience getter for easy access to form fields
  get f() { return this.assignForm.controls; }
  onSubmit() {
    // this.submitted = true;

    // // stop here if form is invalid
    // if (this.assignForm.invalid) {
    //   return;
    // }

    this.loading = true;
    let date_of_event = this.f.event_date.value
    if (date_of_event != null && date_of_event != '') {
      if (date_of_event.month.toString().length < 2) {
        date_of_event.month = "0" + date_of_event.month;
      }
      else {
        date_of_event.month = date_of_event.month;
      }
      if (date_of_event.day.toString().length < 2) {
        date_of_event.day = "0" + date_of_event.day;
      }
      else {
        date_of_event.day = date_of_event.day;
      }
    }

    let date: any = date_of_event.year + "-" + date_of_event.month + "-" + date_of_event.day
    if (this.f.acceptTerms.value == true) {
      this.is_vat_includedAdd = 1
    }
    if (this.f.acceptTerms.value == false) {
      this.is_vat_includedAdd = 0
    }
    // this.studentId
    this.AssignToPlanService.customBill({is_vat_included: this.is_vat_includedAdd, bill_date : date , description : this.f.description.value , amount: this.f.amount.value, parent_id: this.parent_id, student_id: this.studentId})
                  .pipe(first())
                  .subscribe(
                    data => {
                      this.translate.get('backend.ASuccesfuly').subscribe((Succesfuly: string) => { this.addSuccesfuly = Succesfuly; console.log(this.addSuccesfuly) });
                      this.showSuccess(this.addSuccesfuly)
                      this.loading = false;
                      this.assignForm.reset()
                      //this.router.navigate(['/home']);
                      // this.router.navigate([this.returnUrl]);
                    },
                    error => {
                      this.showDanger(error.error.data.errors)
                      // this.alertService.error(this.error);
                      this.loading = false;
                    });
    // 

  }

}
