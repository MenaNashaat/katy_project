import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { LevelService , BranchService , SpinnerService} from '../../services';
import { first } from 'rxjs/operators';
import {Level} from '../../models';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-level',
  templateUrl: './level.component.html',
  styleUrls: ['./level.component.css']
})
export class LevelComponent implements OnInit {
  closeResult = '';
  LevelForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  level : Level;
  levelId: Level;
    // localization
    updateSuccesfuly:string;
    addSuccesfuly:string;
  branch: any;
  branchName: any;
  
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private LevelService: LevelService,
    private ActivatedRoute: ActivatedRoute,
    private translate:TranslateService,
    private BranchService:BranchService,
    private SpinnerService:SpinnerService

  ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.LevelForm = this.formBuilder.group({
      name: ['', Validators.required],
      ar_name: ['', Validators.required],
      min_age:[],
      max_age:[],
     
    });
    this.getbranchName()
    this.getAllLevel()
    this.translate.get('backend.USuccesfuly').subscribe((accept:string) => {this.updateSuccesfuly = accept ;  console.log(this.updateSuccesfuly)});
    this.translate.get('backend.ASuccesfuly').subscribe((Succesfuly:string) => {this.addSuccesfuly = Succesfuly ; console.log(this.addSuccesfuly)});
    
  }
  // convenience getter for easy access to form fields
  get f() { return this.LevelForm.controls; }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getbranchName()
  {
    
    this.BranchService.getBranch().subscribe(data => {this.branch =data.data; 
      const jsObjects = this.branch
      const branch__id = this.ActivatedRoute.snapshot.paramMap.get('id')
     console.log("branch__id" +branch__id)
      var result = jsObjects.find(obj => {
        return  obj.id == branch__id
      }) 
      this.branchName = result.name
     console.log("result.name" +JSON.stringify(this.branchName))
    })
  }
  getAllLevel() {
    const BranchId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.LevelService.getLevel(BranchId).subscribe(data => {this.level = data.data ; console.log(JSON.stringify(this.level))})
  }
  open(edit, id) {
    this.LevelForm.reset()
    this.LevelService.getLevelById(id).subscribe(data => {this.levelId = data.data ; 
      
      this.LevelForm.patchValue({
        name: this.levelId.name,
        ar_name: this.levelId.name_ar,
        min_age: this.levelId.min_age,
        max_age: this.levelId.max_age,

      }); 
     });
   
    this.modalService.open(edit, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      const BranchId = this.ActivatedRoute.snapshot.paramMap.get('id');
      const target = this.LevelForm.value;
      const source = {"branch_id": BranchId};

      const returnedTarget = Object.assign(target, source);
      
    
      this.LevelService.editLevel( returnedTarget, id)
        .pipe(first())
        .subscribe(
          data => {
           
            this.getAllLevel()

            this.showSuccess(this.updateSuccesfuly)
            this.LevelForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  addLevel(content) {
    
    
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
     

      const BranchId = this.ActivatedRoute.snapshot.paramMap.get('id');
      const target = this.LevelForm.value;
      const source = {"branch_id": BranchId};
      const returnedTarget = Object.assign(target, source);
      this.LevelService.createLevel(returnedTarget)
        .pipe(first())
        .subscribe(
          data => {

            this.getAllLevel()
            this.showSuccess(this.addSuccesfuly)
            this.LevelForm.reset()

            // this.router.navigate([this.returnUrl]);
          },
          error => {
            this.showDanger(error.error.data.errors)
            // this.alertService.error(error.error.data.errors);
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    
    this.LevelForm.reset()
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
      this.LevelForm.reset()
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
      this.LevelForm.reset()
    } else {
     
      return `with: ${reason}`;
      
    }
  }

}
