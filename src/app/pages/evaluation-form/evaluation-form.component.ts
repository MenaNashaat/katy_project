import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { EvaluationService , SpinnerService} from '../../services';
import { Tabel } from '../../models';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-evaluation-form',
  templateUrl: './evaluation-form.component.html',
  styleUrls: ['./evaluation-form.component.css']
})
export class EvaluationFormComponent implements OnInit {

  //  pagination
  pagination: Tabel;
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  per_page: Tabel;
  total: Tabel;
  previousPage: any;

 closeResult = '';
 meals: Meal;
 loading = false;
 submitted = false;
 questionForm: FormGroup;
 addForm: FormGroup;
 branchForm: FormGroup;
 addFormEvaluation: FormGroup;
 assignEvaluation:FormGroup;
 editEvaluation:FormGroup;
 deleteSucc: string;
 users: any;
 updateSuccesfuly: string;
 Evaluations: any;
 QuestionDetails: any;
 grade: boolean = false;
 questionOption: boolean = false;
 gradeEdit: boolean = false;
 questionOptionEdit: boolean = false;
 optionORgrade: string;
 arr: any;
 levels: any;
 customers = [
   { "id": 1, "name": "text", "orderIds": [1,2]},
   {"id": 2, "name": "option", "orderIds": [3,4]},
   {"id": 3, "name": "grade", "orderIds": [3,4]}];
  Branch: any;
  level: any;
  formDetails: any;
  brannchValue: any;
  levelsInsideForm: any;
  idAssign: any[];
 constructor(private modalService: NgbModal,
   private formBuilder: FormBuilder,
   private ToastService: ToastService,
   private EvaluationService: EvaluationService,
   private translate: TranslateService,
   private ActivatedRoute: ActivatedRoute, 
   private SpinnerService:SpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllevaluation();
    this.allLevel()
    this.addFormEvaluation= this.formBuilder.group({
      name: ['', Validators.required],
      
    });
    this.editEvaluation= this.formBuilder.group({
      name: ['', Validators.required],
      
    });
    this.assignEvaluation= this.formBuilder.group({
      level: ['', Validators.required],
      branch: ['', Validators.required],
    });
    this.questionForm = this.formBuilder.group({
      question: ['', Validators.required],
       type: [''],
       grade_limit: [''],
       Option1: ['', Validators.required],
      Option2: ['', Validators.required],
      Option3: ['', Validators.required],
      Option4: ['', Validators.required],
    });
    this.addForm = this.formBuilder.group({
      question: ['', Validators.required],
      type: ['', Validators.required],
      grade_limit: ['', Validators.required],
      Option1: ['', Validators.required],
      Option2: ['', Validators.required],
      Option3: ['', Validators.required],
      Option4: ['', Validators.required],
      level_id: ['', Validators.required],
    });
    this.branchForm = this.formBuilder.group({
      level: [],
      name: []
    });
    this.getAllBranch()
  }
  // convenience getter for easy access to form fields
  get f() { return this.questionForm.controls; }
  get g() { return this.addForm.controls; }
  get d() { return this.branchForm.controls; }
  get k() { return this.addFormEvaluation.controls; }
  get s(){ return this.assignEvaluation.controls; }
  get e(){ return this.editEvaluation.controls; }

  selectChangeHandler($event) {
    if ($event.target.value == 'grade') {
      this.grade = true
      this.questionOption = false
      this.optionORgrade = 'grade'
    }
    if ($event.target.value == 'option')
    {

      this.grade = false
      this.questionOption = true
      this.optionORgrade = 'option'
    }
    if ($event.target.value == 'text')
    {

      this.grade = false
      this.questionOption = false
      this.optionORgrade = 'text'
    }

  }
  selectBranch($event){
    this.EvaluationService.levelsByID($event.target.value).subscribe(data => {
      this.level = data.data;
   
     })

  }
  
 
  loadPage(page: number) {
   if (page !== this.previousPage) {
     this.previousPage = page;

      this.getAllevaluation()
   }
 }
 allLevel(){
  this.EvaluationService.levels().subscribe(data => {
    this.levels = data.data;
 
   })
 }
 showSuccess(message) {
  this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
}
showDanger(error) {
  this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
}
getAllevaluation() {
  this.EvaluationService.evaluationForm( ).subscribe(data => {
    this.Evaluations = data.data; 
    // this.pagination = data.meta;
    // this.total = data.meta.total;
    // this.per_page = data.meta.per_page
  })
}
getAllBranch() {
  this.EvaluationService.branch().subscribe(data => {
    this.Branch = data.data; 
    console.log(" this.Branch" + JSON.stringify( this.Branch))
    
  })
}

delete(content, id) {
  this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    this.EvaluationService.deleteEvaluation(id)
    .pipe(first())
    .subscribe(
      data => {
       
        this.getAllevaluation()
        this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
        this.showDanger(this.deleteSucc)
      },

      error => {
        alert(error.error.data.errors)
        //this.showDanger(error.error.data.errors)
        // this.alertService.error(this.error);

      });
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}
deleteAssignLevel(content, formID,  LevelId) {
  this.idAssign = [];
  this.EvaluationService.getFormByID(formID).subscribe(data => {
    this.levelsInsideForm = data.data.levels
      for (let i = 0; i <   this.levelsInsideForm.length ; i++) {
        this.idAssign.push(this.levelsInsideForm[i].id); 
      }
      for(var i = 0; i < this.idAssign.length; i++) {
            if(this.idAssign[i] == LevelId) {
              this.idAssign.splice(i, 1);
                break;
            }
        }
     
  })
  this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    console.log("idAssignlast" +this.idAssign)
    this.EvaluationService.assign(formID , {level_ids: this.idAssign} )
    .pipe(first())
    .subscribe(
      data => {
       
        this.getAllevaluation()
        this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
        this.showDanger(this.deleteSucc)
      },

      error => {
        alert(error.error.data.errors)
        //this.showDanger(error.error.data.errors)
        // this.alertService.error(this.error);

      });
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

edit(content, id) {
    
  this.EvaluationService.getAllEvaluationID(id).subscribe(data => {
    this.QuestionDetails = data.data;
    if (this.QuestionDetails.type== 'grade') {
      this.gradeEdit = true
      this.questionOptionEdit = false
      this.optionORgrade = 'grade'
      this.questionForm.patchValue({
        question:  this.QuestionDetails.question ,
        type: this.QuestionDetails.type ,
        grade_limit: this.QuestionDetails.grade_limit ,
       
    })
    }
    if (this.QuestionDetails.type == 'option')
    {

      this.gradeEdit = false
      this.questionOptionEdit = true
      this.optionORgrade = 'option'
      this.questionForm.patchValue({
        question:  this.QuestionDetails.question ,
        type: this.QuestionDetails.type ,
        Option1: this.QuestionDetails.options[0] ,
        Option2:  this.QuestionDetails.options[1] ,
        Option3:  this.QuestionDetails.options[2] ,
        Option4:  this.QuestionDetails.options[3] ,
    })
    }
    if (this.QuestionDetails.type == 'text')
    {
      this.questionForm.patchValue({
        question:  this.QuestionDetails.question ,
        type: this.QuestionDetails.type ,
        
    })
      this.gradeEdit = false
      this.questionOptionEdit = false
      this.optionORgrade = 'text'
    }
    
   
})


  // });

  this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    const levelId = this.ActivatedRoute.snapshot.paramMap.get('id');

    if (this.optionORgrade == 'grade')
    {
      this.arr = { question: this.f.question.value , type: this.f.type.value , grade_limit: this.f.grade_limit.value ,  level_id: levelId}
    }
    else if (this.optionORgrade == 'option')
    {
      this.arr = { question: this.f.question.value , type: this.f.type.value  , options:[this.f.Option1.value , this.f.Option2.value , this.f.Option3.value ,  this.f.Option4.value] ,  level_id: levelId}
    }
    else if (this.optionORgrade == 'text')
    {
      this.arr = { question: this.f.question.value , type: this.f.type.value  ,  level_id: levelId}
    }

    this.EvaluationService.editEvaluation(this.arr , id)
      .pipe(first())
      .subscribe(
        data => {
          this.translate.get('backend.USuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
        
          this.showSuccess(this.updateSuccesfuly)
          this.getAllevaluation()
         
          // this.showSuccess(this.updateSuccesfuly)
          //this.LevelForm.reset()
        },
        error => {
          console.log(JSON.stringify(error))
          // this.showDanger(error.error.data.errors)
          this.loading = false;
        });

    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}
editEvaluationForm(content, formId) {
    
  this.EvaluationService.getFormByID(formId).subscribe(data => {
    this.formDetails = data.data;
    this.editEvaluation.patchValue({
      name:  this.formDetails.name ,
     
  })
    
   
})


  // });

  this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

   

    this.EvaluationService.editForm(formId , {name: this.e.name.value})
      .pipe(first())
      .subscribe(
        data => {
          this.translate.get('backend.USuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
        
          this.showSuccess(this.updateSuccesfuly)
          this.getAllevaluation()
         
          // this.showSuccess(this.updateSuccesfuly)
          //this.LevelForm.reset()
        },
        error => {
          console.log(JSON.stringify(error))
          // this.showDanger(error.error.data.errors)
          this.loading = false;
        });

    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}
open(content , formId) {
  const levelId = this.ActivatedRoute.snapshot.paramMap.get('id');
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    if (this.optionORgrade == 'grade')
    {
      this.arr = {form_id: formId, question: this.g.question.value , type: this.g.type.value , grade_limit: this.g.grade_limit.value ,  level_id: levelId}
    }
    else if (this.optionORgrade == 'option')
    {
      this.arr = {form_id: formId, question: this.g.question.value , type: this.g.type.value  , options:[this.g.Option1.value , this.g.Option2.value , this.g.Option3.value ,  this.g.Option4.value] ,  level_id: levelId}
    }
    else if (this.optionORgrade == 'text')
    {
      this.arr = {form_id: formId, question: this.g.question.value , type: this.g.type.value  ,  level_id: levelId}
    }
    this.EvaluationService.createUser(this.arr )
      .pipe(first())
      .subscribe(
        data => {
         
          this.getAllevaluation()
          this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
        
           this.showSuccess(this.updateSuccesfuly)
          this.addForm.reset()
          this.grade = false
          this.questionOption = false
        },
        error => {
          console.log(JSON.stringify(error))
          this.showDanger(error.error.data.errors)
          this.loading = false;
          this.grade = false
          this.questionOption = false
        });


    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}
addFormEva(content) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
   
    this.EvaluationService.createEvaForm(this.addFormEvaluation.value )
      .pipe(first())
      .subscribe(
        data => { 
         
          this.getAllevaluation()
          this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
        
           this.showSuccess(this.updateSuccesfuly)
          this.addFormEvaluation.reset()
         
        },
        error => {
          console.log(JSON.stringify(error))
          this.showDanger(error.error.data.errors)
          this.loading = false;
         
        });


    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}
assignForm(content , formID) {
  var idAssign = [];
  this.EvaluationService.getFormByID(formID).subscribe(data => {
    this.levelsInsideForm = data.data.levels
    
      for (let i = 0; i <   this.levelsInsideForm.length ; i++) {
        idAssign.push(this.levelsInsideForm[i].id); 
      }
     
  })
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    idAssign.push(this.s.level.value)
    this.EvaluationService.assign(formID , {level_ids: idAssign} )
      .pipe(first())
      .subscribe(
        data => { 
         
          this.getAllevaluation()
          this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
        
           this.showSuccess(this.updateSuccesfuly)
          this.assignEvaluation.reset()
         
        },
        error => {
          console.log(JSON.stringify(error))
          this.showDanger(error.error.data.errors)
          this.loading = false;
         
        });


    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}
FilterLByLevel() {
  // this.submitted = true;

  // // stop here if form is invalid
  // if (this.branchForm.invalid) {
  //   return;
  // }
  // this.loading = true;
  this.brannchValue = this.d.name.value
  this.EvaluationService.getEvaluationFilter(this.brannchValue , this.d.level.value)
    .pipe(first())
    .subscribe(
      data => {
        this.Evaluations = data.data; 
        this.loading = false
        this.branchForm.reset()
      },

      error => {
        alert(error)
        this.branchForm.reset()
        //this.showDanger(error.error.data.errors)
        // this.alertService.error(this.error);
        this.loading = false;
      });

}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return `with: ${reason}`;
  }
}

}
