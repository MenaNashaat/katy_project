import { Component, OnInit, ViewChild, Inject, ElementRef } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { GeneralInfoService, SpinnerService } from '../../services';
import { first } from 'rxjs/operators';
import { Level } from '../../models';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import { DomSanitizer } from '@angular/platform-browser';
import { codeBlockButton } from 'ngx-summernote';
@Component({
  selector: 'app-general-info',
  templateUrl: './general-info.component.html',
  styleUrls: ['./general-info.component.css']
})


export class GeneralInfoComponent implements OnInit {
  @ViewChild('attachments') attachment: any;
  @ViewChild('myInput') myInputVariable: ElementRef;
  fileList: File[] = [];
  listOfFiles: any[] = [];
  @ViewChild('myInputCover') myInputVariableCover: ElementRef;
  fileListCover: File[] = [];
  listOfFilesCover: any[] = [];

  @ViewChild('myInputTabLogo') myInputVariableTabLogo: ElementRef;
  fileListTabLogo: File[] = [];
  listOfFilesTabLogo: any[] = [];

  @ViewChild('myInputInvoice') myInputVariableInvoice: ElementRef;
  fileListInvoice: File[] = [];
  listOfFilesInvoice: any[] = [];

  @ViewChild('myInputTextImage') myInputVariableTextImage: ElementRef;
  fileListTextImage: File[] = [];
  listOfFilesTextImage: any[] = [];

  closeResult = '';
  generalForm: FormGroup;
  invoiceForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  level: Level;
  levelId: Level;
  // localization
  updateSuccesfuly: string;
  addSuccesfuly: string;
  str: any;
  res: any;
  selectedFile: File;
  selectedFileCover: File;
  logo: any;
  webPhoto: any;
  selectedTabLogo: File;
  TabLogo: any;
  dashboardColor: any;
  footerColor: any;
  sitHeaderColor: any;
  selectedinvoicelogo: File;
  invoicelogo: any;
  selectedTextImage: File;
  TextImage: any;
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private toastService: ToastService,
    private GeneralInfoService: GeneralInfoService,
    private ActivatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private sanitizer: DomSanitizer,
    private SpinnerService: SpinnerService
  ) { }
  config: any = {
    airMode: false,
    tabDisable: true,
    popover: {
      table: [
        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
      ],
      image: [
        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']]
      ],
      link: [
        ['link', ['linkDialogShow', 'unlink']]
      ],
      air: [
        [
          'font',
          [
            'bold',
            'italic',
            'underline',
            'strikethrough',
            'superscript',
            'subscript',
            'clear'
          ]
        ],
      ]
    },
    height: '200px',
    uploadImagePath: '/api/upload',
    toolbar: [
      ['misc', ['codeview', 'undo', 'redo', 'codeBlock']],
      [
        'font',
        [
          'bold',
          'italic',
          'underline',
          'strikethrough',
          'superscript',
          'subscript',
          'clear'
        ]
      ],
      ['fontsize', ['fontname', 'fontsize', 'color']],
      ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
      ['insert', ['table', 'picture', 'link', 'video', 'hr']],
      ['customButtons', ['testBtn']]
    ],

    codeviewFilter: true,
    codeviewFilterRegex: /<\/*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|ilayer|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|t(?:itle|extarea)|xml|.*onmouseover)[^>]*?>/gi,
    codeviewIframeFilter: true
  };

  editorDisabled = false;

  get sanitizedHtml() {
    return this.sanitizer.bypassSecurityTrustHtml(this.generalForm.get('about_en').value);
  }
  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getSetting()
    this.generalForm = this.formBuilder.group({
      facebook: ['', Validators.required],
      twitter: ['', Validators.required],
      instagram: ['', Validators.required],
      contact_en: ['', Validators.required],
      contact_ar: ['', Validators.required],
      about_en: ['', Validators.required],
      about_ar: ['', Validators.required],
      title_ar: ['', Validators.required],
      title: ['', Validators.required],
      phone: ['', Validators.required],
      color: ['', Validators.required],
      colorFooter: ['', Validators.required],
      email: ['', Validators.required],
      colorDashboard: ['', Validators.required],
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required])

    });
    this.invoiceForm = this.formBuilder.group({
      email: ['', Validators.required],
      phone: ['', Validators.required],
      address_ar: ['', Validators.required],
      company_name: ['', Validators.required],
      company_name_ar: ['', Validators.required],
      address: ['', Validators.required],
      registration_number: ['', Validators.required],
      vat: ['', Validators.required],
      zip_code: [''],
      city_ar: [''],
      city: [''],

    });


  }


  getSetting() {
    this.GeneralInfoService.settings().subscribe(data => {
      console.log("facebook" + JSON.stringify(data.data))
      this.str = JSON.stringify(data.data)
      this.res = this.str.replace(/-/g, "_");
      console.log(this.res)

      this.res = JSON.parse(this.res)
      this.logo = data.data.logo.en,
        this.invoicelogo = data.data.invoice_logo.en,
        this.sitHeaderColor = data.data.color.en;
      this.footerColor = this.res.color_footer.en;
      this.dashboardColor = this.res.color_dashboard.en;
      this.dashboardColor = this.dashboardColor.replace(/_/g, "-");
      this.TabLogo = this.res.tab_logo.en;
      this.TabLogo = this.TabLogo.replace(/_/g, "-");
      this.webPhoto = this.res.web_photo.en;
      this.webPhoto = this.webPhoto.replace(/_/g, "-");
      this.TextImage = this.res.text_image.en;
      this.TextImage = this.TextImage.replace(/_/g, "-");
      console.log(this.TextImage)
      console.log("webPhoto" + JSON.stringify(this.webPhoto))
      this.invoiceForm = this.formBuilder.group({


        vat: data.data.vat.en,
        address_ar: data.data.address.ar,
        address: data.data.address.en,
        company_name: data.data.company_name.en,
        company_name_ar: data.data.company_name.ar,
        phone: data.data.phone.en,
        email: data.data.email.en,
        registration_number: data.data.registration_number.en,
        zip_code: data.data.zip_code.en,
        city: data.data.city.en,
        city_ar: data.data.city.ar,



      })
      this.generalForm = this.formBuilder.group({
        facebook: data.data.facebook.en,
        twitter: data.data.twitter.en,
        instagram: data.data.instagram.en,
        contact_en: this.res.contact_us.en,
        contact_ar: this.res.contact_us.ar,
        about_en: this.res.about_us.en,
        about_ar: this.res.about_us.ar,
        colorDashboard: this.res.color_dashboard.en,
        colorFooter: this.res.color_footer.en,
        title_ar: this.res.title.ar,
        title: this.res.title.en,
        phone: this.res.phone.en,
        color: this.res.color.en,
        email: this.res.email.en,
        // file: new FormControl('', [this.res.logo]),
        // fileSource: new FormControl('', [Validators.required])


      })
    })

  }
  showSuccess() {
    this.toastService.show('Update Succesfully ', { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(test) {
    this.toastService.show(test, { classname: 'bg-danger text-light', delay: 3000 });
  }

  // convenience getter for easy access to form fields
  get f() { return this.generalForm.controls; }
  get i() { return this.invoiceForm.controls; }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
    var selectedFile = event.target.files[0];
    this.fileList.push(selectedFile);
    this.listOfFiles.push(selectedFile.name)

  }
  // onFileChanged(event: any) {
  //   for (var i = 0; i <= event.target.files.length - 1; i++) {
  //     this.selectedFile = event.target.files[0]
  //     var selectedFile = event.target.files[i];
  //     this.fileList.push(selectedFile);
  //     this.listOfFiles.push(selectedFile.name)
  // }

  // this.attachment.nativeElement.value = '';
  // }



  removeSelectedFile(index) {
    // Delete the item from fileNames list
    this.listOfFiles.splice(index, 1);
    this.myInputVariable.nativeElement.value = "";
    // delete file from FileList
    this.fileList.splice(index, 1);
  }
  // deleteicon(e){
  //   this.myInputimgLogo.nativeElement.value = "";
  // }
  onFileChangedCover(event) {
    this.selectedFileCover = event.target.files[0]
    var selectedFileCover = event.target.files[0];
    this.fileListCover.push(selectedFileCover);
    this.listOfFilesCover.push(selectedFileCover.name)
  }
  removeSelectedFileCover(index) {
    // Delete the item from fileNames list
    this.listOfFilesCover.splice(index, 1);
    this.myInputVariableCover.nativeElement.value = "";
    // delete file from FileList
    this.fileListCover.splice(index, 1);
  }
  onFileChangedinvoicelogo(event) {
    this.selectedinvoicelogo = event.target.files[0]
    var selectedFileInvoice = event.target.files[0];
    this.fileListInvoice.push(selectedFileInvoice);
    this.listOfFilesInvoice.push(selectedFileInvoice.name)
  }
  removeSelectedFileInvoice(index) {
    // Delete the item from fileNames list
    this.listOfFilesInvoice.splice(index, 1);
    this.myInputVariableInvoice.nativeElement.value = "";
    // delete file from FileList
    this.fileListInvoice.splice(index, 1);
  }

  onFileChangedTabLogo(event) {
    this.selectedTabLogo = event.target.files[0]
    var selectedTabLogo = event.target.files[0];
    this.fileListTabLogo.push(selectedTabLogo);
    this.listOfFilesTabLogo.push(selectedTabLogo.name)
  }
  removeSelectedTabLogo(index) {
    // Delete the item from fileNames list
    this.listOfFilesTabLogo.splice(index, 1);
    this.myInputVariableTabLogo.nativeElement.value = "";
    // delete file from FileList
    this.fileListTabLogo.splice(index, 1);
  }

  onFileChangedTextImage(event) {
    this.selectedTextImage = event.target.files[0]
    var selectedTextImage = event.target.files[0];
    this.fileListTextImage.push(selectedTextImage);
    this.listOfFilesTextImage.push(selectedTextImage.name)
  }
  removeSelectedTextImage(index) {
    // Delete the item from fileNames list
    this.listOfFilesTextImage.splice(index, 1);
    this.myInputVariableTextImage.nativeElement.value = "";
    // delete file from FileList
    this.fileListTextImage.splice(index, 1);
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    // if (this.generalForm.invalid) {
    //   return;
    // }

    this.loading = true;
    var formData: any = new FormData();
    // if ( this.selectedFile != '' )
    // {
    //   console.log("yessssssssss" +this.selectedFile)
    // }
    var facebook: any = this.f.facebook.value
    if (facebook === undefined || facebook == null || facebook.length <= 0) {
      var facebook: any = '.'
    }
    var twitter: any = this.f.twitter.value
    if (twitter === undefined || twitter == null || twitter.length <= 0) {
      var twitter: any = '.'
    }
    var instagram: any = this.f.instagram.value
    if (instagram === undefined || instagram == null || instagram.length <= 0) {
      var instagram: any = '.'
    }

    formData.append("facebook", facebook);
    formData.append("twitter", twitter);
    formData.append("instagram", instagram);
    formData.append("contact-us", this.f.contact_en.value);
    formData.append('contact-us-ar', this.f.contact_ar.value);
    formData.append('about-us', this.f.about_en.value);
    formData.append('about-us-ar', this.f.about_ar.value);
    formData.append('title-ar', this.f.title_ar.value);
    formData.append('title', this.f.title.value);
    formData.append('phone', this.f.phone.value);
    formData.append('email', this.f.email.value);
    formData.append('color', this.f.color.value);
    formData.append('color-dashboard', this.f.colorDashboard.value);
    formData.append('color-footer', this.f.colorFooter.value);
    if (typeof (this.selectedFile) != "undefined") {
      formData.append('logo', this.selectedFile, this.selectedFile.name);
    }
    if (typeof (this.selectedFileCover) != "undefined") {
      formData.append('web-photo', this.selectedFileCover, this.selectedFileCover.name);
    }
    if (typeof (this.selectedTabLogo) != "undefined") {
      formData.append('tab-logo', this.selectedTabLogo, this.selectedTabLogo.name);
    }
    if (typeof (this.selectedTextImage) != "undefined") {
      formData.append('text-image', this.selectedTextImage, this.selectedTextImage.name);
    }


    this.GeneralInfoService.addSettings(formData)
      .pipe(first())
      .subscribe(
        data => {
          this.showSuccess()
          this.getSetting()
          this.GeneralInfoService.nextMessage(this.f.colorDashboard.value)
          this.GeneralInfoService.changeMessage(this.f.colorDashboard.value)

          //this.router.navigate(['/home']);
          // this.router.navigate([this.returnUrl]);
          this.loading = false;
        },
        error => {

          this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);
          this.loading = false;
        });
  }
  onSubmitInvoice() {
    var formdata: any = new FormData();
    formdata.append('email', this.i.email.value);
    formdata.append('phone', this.i.phone.value);
    formdata.append('address-ar', this.i.address_ar.value);
    formdata.append('company_name', this.i.company_name.value);
    formdata.append('company_name_ar', this.i.company_name_ar.value);
    formdata.append('registration_number', this.i.registration_number.value);
    formdata.append('address', this.i.address.value);
    formdata.append('vat', this.i.vat.value);
    formdata.append('city', this.i.city.value);
    formdata.append('city-ar', this.i.city_ar.value);
    formdata.append('zip_code', this.i.zip_code.value);
    if (typeof (this.selectedinvoicelogo) != "undefined") {
      formdata.append('invoice_logo', this.selectedinvoicelogo, this.selectedinvoicelogo.name);
    }

    this.GeneralInfoService.addSettingInvoice(formdata)
      .pipe(first())
      .subscribe(
        data => {
          this.showSuccess()


          //this.router.navigate(['/home']);
          // this.router.navigate([this.returnUrl]);
          this.loading = false;
        },
        error => {

          this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);
          this.loading = false;
        });
  }

}
