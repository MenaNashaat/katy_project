import { Component, OnInit, ViewChild, ChangeDetectionStrategy, TemplateRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ListApplicationsService } from '../../services/list-applications.service';
import { SpinnerService } from '../../services';
import { listAplications } from '../../models/listAplications';
import { FormControl } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Branch, Tabel } from '../../models';
import { first } from 'rxjs/operators';
import { ToastService } from '../../services/toast/toast.service';
import { TranslateService } from "@ngx-translate/core";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-registration',

  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})


export class RegistrationComponent implements OnInit {

  // ssssssssssssssssssssss
  closeResult = '';
  branchForm: FormGroup;
  filterByName: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  branch: Branch;
  toppings = new FormControl();

  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  //  pagination
  pagination: Tabel;
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  per_page: Tabel;
  total: Tabel;
  previousPage: any;

  list: any;
  deleteSucc: string;
  element: HTMLElement;

 filterlevelName = true;
 filterlevelId = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  pageChange: any;
  brannchValue: any;
  levels: any;
  first_name: any;
  constructor(private ListApplicationsService: ListApplicationsService,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private toastService: ToastService,
    private modalService: NgbModal,
    private SpinnerService:SpinnerService) {
  }
  ngOnInit() {
     this.SpinnerService.loader()
    this.loadData();
    this.level()
    this.getUniqueLevel()
    this.branchForm = this.formBuilder.group({
      name: ['', Validators.required],
      ParentName: ['', Validators.required],
      levelName:[],
      level: []

    });
    this.filterByName = this.formBuilder.group({
      name: ['', Validators.required],
      ParentName: ['', Validators.required],
    });
  }
  selectBranch($event) {
    if  ( $event.target.value == 'all')
    {
     
      this.filterlevelName= true;
      this.filterlevelId = false
      this.getUniqueLevel()
    }
    else{
      this.filterlevelName= false;
      this.filterlevelId = true
      this.ListApplicationsService.getLevel($event.target.value).subscribe(data => this.levels = data.data)

    }
  }
  getUniqueLevel(){
    this.ListApplicationsService.getuniqueLevel().subscribe(data => this.levels = data.data)
  }
  fristname: any;
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
    
      this.loadData();
    }
  }

  getColor(Status) {
    switch (Status) {
      case 'Active':
        return 'green';
      case 'Pending':
        return '#ffc107';
      case 'Rejected':
        return 'red';
    }
  }
  showDanger(error) {
    this.toastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
 
  delete(content, id) {
   

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.ListApplicationsService.deleteApplication(id)
      .pipe(first())
      .subscribe(
        data => {
          this.loadData()
          this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
          this.showDanger(this.deleteSucc)
        },

        error => {
          alert(error.error.data.errors)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);

        });
     
      

      

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  cancel(content, id) {
   

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.ListApplicationsService.cancelApplication(id)
      .pipe(first())
      .subscribe(
        data => {
          this.loadData()
          this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
          this.showDanger(this.deleteSucc)
        },

        error => {
          alert(error.error.data.errors)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);

        });
     
      

      

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  loadData() {
   
    if (this.brannchValue)
    {
      console.log("branch name exist")
      if (this.f.name.value != 'all')
      {
        console.log("select branch name")
        this.ListApplicationsService.applicationFilter(this.previousPage, this.f.name.value, this.f.level.value, this.f.ParentName.value)
        .pipe(first())
        .subscribe(
          data => {
            this.list = data.data; this.pagination = data.meta;
            this.total = data.meta.total;
            this.per_page = data.meta.per_page;
            this.loading = false
          },
  
          error => {
            alert(error)
            this.loading = false;
          });
      }
      else
      {
        console.log("select all")
        this.ListApplicationsService.application(this.previousPage).subscribe(data => {
          this.list = data.data; this.pagination = data.meta;
          this.total = data.meta.total;
          this.per_page = data.meta.per_page;
        
        })
      }
    }
    else
    {
      console.log(" no branch name")
      this.ListApplicationsService.application(this.previousPage).subscribe(data => {
        this.list = data.data; this.pagination = data.meta;
        this.total = data.meta.total;
        this.per_page = data.meta.per_page;
      })
    }

    
  }
  level() {
    this.ListApplicationsService.getBranch().subscribe(data => this.branch = data.data)
  }
  // convenience getter for easy access to form fields
  get f() { return this.branchForm.controls; }
  get e() { return this.filterByName.controls; }

  onSubmit() {
    // this.submitted = true;

    // // stop here if form is invalid
    // if (this.branchForm.invalid) {
    //   return;
    // }
   this.brannchValue = this.f.name.value
    this.loading = true;
    // console.log(this.previousPage, this.f.name.value , this.f.level.value , this.f.levelName.value, this.f.ParentName.value)
    this.ListApplicationsService.applicationFilter(this.previousPage, this.f.name.value , this.f.level.value , this.f.ParentName.value)
      .pipe(first())
      .subscribe( 
        data => {
          this.list = data.data; this.pagination = data.meta;
          this.total = data.meta.total;
          this.per_page = data.meta.per_page;
          this.loading = false
        },

        error => {
          alert(error)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);
          this.loading = false;
        });

  }
  // filterName() {
  //  this.brannchValue = this.f.name.value
  //   this.loading = true;
  //   this.ListApplicationsService.applicationFilterName(this.previousPage, this.e.name.value )
  //     .pipe(first())
  //     .subscribe(
  //       data => {
  //         this.list = data.data; this.pagination = data.meta;
  //         this.total = data.meta.total;
  //         this.per_page = data.meta.per_page;
  //         this.loading = false
  //       },

  //       error => {
  //         alert(error)
  //         //this.showDanger(error.error.data.errors)
  //         // this.alertService.error(this.error);
  //         this.loading = false;
  //       });

  // }


}
