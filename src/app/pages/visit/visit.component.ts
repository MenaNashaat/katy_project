import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { VisitService, SpinnerService } from '../../services';
import { first } from 'rxjs/operators';
import {Visit  , Tabel} from '../../models';
import {  ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-visit',
  templateUrl: './visit.component.html',
  styleUrls: ['./visit.component.css']
})
export class VisitComponent implements OnInit {
  visits :any;
  closeResult = '';
  level: any ;
  assignForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  levelID:any
 //  pagination
 pagination:Tabel;
 itemsPerPage: number;
 totalItems: any;
 page = 1;
 per_page: Tabel;
 total :Tabel;
 previousPage: any;
 //translation
 Accept:string;
   
  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private VisitService: VisitService,
    private translate:TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private SpinnerService:SpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllVisit()
  }
  getAllVisit() {
    this.VisitService.getVisits(this.previousPage).subscribe(data => {this.visits = data.data ; 
      console.log("this.visits" +JSON.stringify(this.visits));
      this.pagination =data.meta ;
      this.total = data.meta.total;
      this.per_page= data.meta.per_page})
  }
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
    
       this.getAllVisit();
    }
  }

}
