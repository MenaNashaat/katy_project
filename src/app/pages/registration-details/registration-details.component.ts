import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {ListApplicationsService} from '../../services/list-applications.service';
import {StudentService} from '../../services/student/student.service';
import { listAplications} from '../../models/listAplications'
import { WattingService , SpinnerService } from '../../services';
import {  ToastService} from '../../services/toast/toast.service';
import { TranslateService } from "@ngx-translate/core";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-registration-details',
  templateUrl: './registration-details.component.html',
  styleUrls: ['./registration-details.component.css']
})
export class RegistrationDetailsComponent implements OnInit {
  details : listAplications;
  childrens:any;
  children :any;
  parent :listAplications;
  parent_two :listAplications;
  status: any;
  Accept:string;
  Reject:string;
  branchName:any;
  contact:any
  info:any
  medicalPartAr : boolean;
  medicalPartEn :  boolean;
  lang:any;
  // action
  notActive:boolean = true;
  removeRejectBtn:boolean = true;
  allBranch: any;
  branchId: any;
  levels: any;
  levelInfo: any;
  current_enroll_number: any;
  max_students_number:number;
  applicationId: any;
  nationality: any;
  language: any;
  branchs: any;
  levelID: any;
  removeWaitingBtn:boolean = true;
  childrenId: any;
  constructor(private route:ActivatedRoute ,private StudentService:StudentService, 
    private translate:TranslateService, private ListApplicationsService: ListApplicationsService,
     private toastService: ToastService , private WattingService:WattingService ,
     private SpinnerService:SpinnerService
     ) { }
 
  ngOnInit(): void {
   this.SpinnerService.loader()

        this.getdetails()
        
   this.translate.get('REGISTRATION-DETAILS.Accept').subscribe((acceptText:string) => {this.Accept = acceptText });
    this.translate.get('REGISTRATION-DETAILS.Reject').subscribe((text:string) => {this.Reject= text ; console.log(this.Reject)});
    this.StudentService.sharedMessage.subscribe(message => {
      this.lang = message;
      
      if (this.lang == "ar") {
       
        this.medicalPartAr = true 
        this.medicalPartEn = false 
      }
      else if (this.lang == "en") {
        
        this.medicalPartAr = false
        this.medicalPartEn =  true
      }
    })
  }
  showSuccess(message) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 3000 });
  }

  showDanger(error) {
    this.toastService.show(error , { classname: 'bg-danger text-light', delay: 3000 });
  }
  getColor(Status) { 
    switch (Status) {
      case 'Active':
        return 'green';
      case 'Pending':
        return '#ffc107';
      case 'Rejected':
        return 'red';
      case 'Waiting':
        return 'yellow';
    }
  }
  sendID(){
    this.ListApplicationsService.changeMessage(this.children.student.id)
  }
  goBack() {
    window.history.back();
  }
  getdetails(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.ListApplicationsService.getRegisterById(id)
    .subscribe(
      data => {
        this.details =data,
        this.info =data.data,
        this.applicationId =data.data.id,
        this.branchName = this.details.data.branch,
        this.childrens =this.details.data.children,
        this.children = this.childrens[0],
        this.childrenId = this.childrens[0].id,
        this.language =  this.children.language,
        this.nationality =  this.children.nationality,
        this.contact  = this.details.data.contact 
        this.max_students_number = this.childrens[0].max_students_number
        this.parent =this.details.data.parent,
        this.parent_two =this.details.data.parent_two,
        this.status = this.details.data.status;
        if(localStorage.getItem("language") == "en")
      {
        this.medicalPartAr = false
        this.medicalPartEn =  true
      }  
      else
      {
        this.medicalPartAr = true 
        this.medicalPartEn = false 
      }

        if ( this.status == 'Active')
        {
          this.notActive = false
        }else if (this.status == 'Rejected') {
          this.removeRejectBtn = false
        } 
        else if (this.status == 'Waiting') {
          this.removeWaitingBtn = false
        } 
        
        // max of Level
        this.ListApplicationsService.getBranch().subscribe(
          data =>{ this.allBranch= data.data ; console.log(JSON.stringify(this.allBranch)) 
           
           const jsObjects = this.allBranch
           var result = jsObjects.find(obj => {
             return  obj.name === this.branchName
           }) 
          console.log("result.id" + JSON.stringify(result.id))
          this.branchId =result.id
          this.ListApplicationsService.getLevel(this.branchId).subscribe(res => {this.levels = res.data ;
            
           const jsObjectsLevel = this.levels
           var resultLevel = jsObjectsLevel.find(obj => {
             return  obj.name === this.children.level
           })
           this.levelInfo = resultLevel.max_students_number
           this.current_enroll_number = resultLevel.current_enroll_number
           this.levelID = resultLevel.id
        
   
         })
         
         }
        )
        // end max of level

         
      },
      error => {
       alert(error)
        
      });
  }
  reject()
  {
    const id = this.route.snapshot.paramMap.get('id');
    this.ListApplicationsService.Reject(id)
    .subscribe(
      data => {
       this.removeRejectBtn = false,
         this.getdetails() ,
         this.showDanger(this.Reject)
      },
      error => {
        this.showDanger(error.error.data.errors)
        
        
      });    
  }
  accept()
  {
    
    const id = this.route.snapshot.paramMap.get('id');
   
    this.ListApplicationsService.accept(id)
    .subscribe(
      data => {
        this.notActive = false;
        
         this.getdetails(),
         this.showSuccess(this.Accept)
      },
      error => {
        this.showDanger(error.error.data.errors)
        
      });    
  }
  waiting()
  {
    
    // this.WattingService.createUser({parent_name: this.children.father_name ,child_name: this.children.first_name + this.children.middle_name ,
    //   mobile: this.children.father_phone ,email: this.children.father_email ,level_id: this.levelID  })
      const id = this.route.snapshot.paramMap.get('id');
    this.WattingService.addToWaitingList(id  )
    .subscribe(
      data => {
        this.removeWaitingBtn= false
        this.WattingService.createUser({parent_name: this.children.father_name ,
          child_name: this.children.first_name + this.children.middle_name ,
            mobile: this.children.father_phone ,email: this.children.father_email ,level_id: this.levelID  })
            .subscribe( res =>{
             
              this.getdetails(),
              this.showSuccess(this.Accept)
        })
        // this.WattingService.createUser().subscribe()
        
         
      },
      error => {
        this.showDanger(error.error.data.errors)
        
      });    
  }

}
