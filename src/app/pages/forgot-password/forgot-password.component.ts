import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../services';
import { ToastService } from '../../services/toast/toast.service';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  
  forgotForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  sendSuccesfuly: string;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private ToastService: ToastService,
    private AuthenticationService:AuthenticationService
    // private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.forgotForm.controls; }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  onSubmit() {
    this.submitted = true;
       
    // stop here if form is invalid
    if (this.forgotForm.invalid) {
      return;
    }

    this.loading = true;
    this.AuthenticationService.forgotPassword( this.forgotForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.translate.get('backend.SSuccesfuly').subscribe((acceptText: string) => { this.sendSuccesfuly = acceptText });

          this.showSuccess("Send Succesfuly")
          this.loading = false;
        },
        error => {
         
          this.loading = false;
        });
  }

}
