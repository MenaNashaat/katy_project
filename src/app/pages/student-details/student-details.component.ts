import { Component, OnInit } from '@angular/core';
import { ToastService } from '../../services/toast/toast.service';
import { StudentService } from '../../services';
import { first } from 'rxjs/operators';
import { listAplications, Class, Tabel } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {
  students: any;
  status: any;
  class:any;
  children:any;
  current_enroll_number: any;
  max_students_number: any;
  levelName: any;
  branchName: any;
  first_name: any;
  Classdetails: any;
  medical: any;
  application: any;
  medicalPartAr : boolean;
  medicalPartEn :  boolean;
  language: any;
  nationality: any;
  applicationId:any 
  constructor(
   
    private ToastService: ToastService,
    private StudentService: StudentService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.getdetails()
  }
  getColor(Status) { 
    switch (Status) {
      case 'Active':
        return 'green';
      case 'Pending':
        return '#ffc107';
      case 'Rejected':
        return 'red';
    }
  }
  getdetails(): void {
    const id = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.StudentService.getStudentDetails(id)
    .subscribe(
      data => {

        this.students =data.data,
        this.class =this.students.class,
        this.applicationId =data.data.id,
        this.children =this.students.child,
        this.language =  this.students.child.language,
        this.nationality =  this.students.child.nationality,
        // this.first_name =this.children.child.first_name,
        this.max_students_number = this.children.max_students_number,
        this.current_enroll_number = this.children.current_enroll_number,
        this.branchName = this.children.application.branch,
        this.levelName = this.children.level,
        this.medical = this.children.medical
        this.application = this.children.application;
        if(localStorage.getItem("language") == "en")
        {
          this.medicalPartAr = false
          this.medicalPartEn =  true
        }  
        else
        {
          this.medicalPartAr = true 
          this.medicalPartEn = false 
        }
  
       
        console.log("this.this.medicalthis.medical" + JSON.stringify(this.medical))

       
      

         
      },
      error => {
       alert(error)
        
      });
  }
 

}
