import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { PlanService , SpinnerService} from '../../services';
import { first } from 'rxjs/operators';
import { Plan } from '../../models';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {
  closeResult = '';
  PlanForm: FormGroup;
  VATForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  isVisible: boolean = false;
  disVal: string;
  repetationX  = 0;
  durationX = 0 ;
  gapX = 0 ;

  // localization
  updateSuccesfuly: string;
  addSuccesfuly: string;
  plans: any
  percent: any;
  value: number;
  disvalEdit: any;
  valueOfRadio: string;
  is_vat_includedRes: any;
  is_vat_includedAdd: any;
  deleteSucc: string;
  vat: any;
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private PlanService: PlanService,
    private ActivatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private SpinnerService:SpinnerService
  ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllPlan()
    this.PlanForm = this.formBuilder.group({
      name: ['', Validators.required],
      amount: ['', Validators.required],
      discount_value: ['', Validators.required],
      discount_percent: ['', Validators.required],
      discountRadio: [''],
      discount_input: [''],
      repetation: ['', Validators.required],
      duration: ['', Validators.required],
      gap: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue],
      description: ['', Validators.required]
    });
    
    this.VATForm = this.formBuilder.group({
      vat: ['', Validators.required],
    });


  }
  // convenience getter for easy access to form fields
  get f() { return this.PlanForm.controls; }
  get v() { return this.VATForm.controls; }

  selectChangeHandler($event) {
    this.isVisible = true
    this.disVal = $event.target.value
  }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllPlan() {
    this.PlanService.getPlans().subscribe(data => { this.plans = data.data; console.log(JSON.stringify(this.plans)) })
  }
  delete(content, id) {
   

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.PlanService.deletePlan(id)
      .pipe(first())
      .subscribe(
        data => {
          this.getAllPlan()
          this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
          this.showDanger(this.deleteSucc)
        },

        error => {
          alert(error.error.data.errors)
          this.showDanger(error.error.data.errors)
         
          //  this.alertService.error(this.error);

        });
     
      

      

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  addPlan(content) {
    console.log("open" + JSON.stringify(this.PlanForm.value))
    // this.submitted = true;

    // // stop here if form is invalid
    // if (this.PlanForm.invalid) {
    //   return;
    // }

    // this.loading = true;

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      if (this.disVal == "percent") {
        this.percent = this.f.discount_input.value
        this.value = 0
      }
      else if (this.disVal == "value") {
        this.value = this.f.discount_input.value
        this.percent = 0
      }
      if (this.disVal == undefined) {
        this.value = 0
        this.percent = 0
      }
      if (this.f.discount_input.value == '') {
        console.log("empty")
        this.value = 0
        this.percent = 0
      }
      if (this.f.discount_input.value == null) {
        console.log("null")
        this.value = 0
        this.percent = 0
      }
      if (this.f.acceptTerms.value == true) {
        this.is_vat_includedAdd = 1
      }
      if (this.f.acceptTerms.value == false) {
        this.is_vat_includedAdd = 0
      }

      console.log("discound" + this.percent + "value" + this.value)
      console.log("this.f.discount_input.value" + this.f.discount_input.value)
      console.log("this.disVal" + this.disVal)
      console.log("this.f.acceptTerms.value"+this.f.acceptTerms.value)
      this.PlanService.createPlan({ is_vat_included: this.is_vat_includedAdd, amount: this.f.amount.value, description: this.f.description.value, discount_percent: this.percent, discount_value: this.value, duration: this.f.duration.value, gap: this.f.gap.value, name: this.f.name.value, repetation: this.f.repetation.value })
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.ASuccesfuly').subscribe((Succesfuly: string) => { this.addSuccesfuly = Succesfuly; console.log(this.addSuccesfuly) });
            this.getAllPlan()
            this.showSuccess(this.addSuccesfuly)
            this.PlanForm.reset()
            this.isVisible = false
            // this.router.navigate([this.returnUrl]);
          },
          error => {
            this.showDanger(error.error.data.errors)
            // this.alertService.error(error.error.data.errors);
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  open(content, id) {
    const jsObjects = this.plans
    var result = jsObjects.find(obj => {
      return obj.id === id
    })
    if (result.discount_value != 0)
    {
        this.disvalEdit = result.discount_value,
        this.isVisible= true,
        this.valueOfRadio = 'value'
    }
    if (result.discount_percent != 0)
    {
        this.disvalEdit = result.discount_percent,
        this.isVisible= true,
        this.valueOfRadio = 'percent'
    }
    if (result.discount_percent == 0 && result.discount_value == 0)
    {
        this.isVisible= false
    }
    if (result.is_vat_included == 1) {
      this.is_vat_includedRes = true
    }
    if (result.is_vat_included == 0) {
      this.is_vat_includedRes = false
    }
    this.PlanForm.patchValue({
      name: result.name,
      amount: result.amount,
      discount_input: this.disvalEdit,
      discountRadio:  this.valueOfRadio,
      repetation: result.repetation,
      duration: result.duration,
      gap: result.gap,
      acceptTerms:  this.is_vat_includedRes,
      description: result.description
    });
    console.log("result" + JSON.stringify(result))


    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      if (this.f.discountRadio.value == "percent") {
        this.percent = this.f.discount_input.value
        this.value = 0
      }
      else if (this.f.discountRadio.value  == "value") {
        this.value = this.f.discount_input.value
        this.percent = 0
      }
      if (this.f.discountRadio.value  == undefined) {
        this.value = 0
        this.percent = 0
      }
      if (this.f.discount_input.value == '') {
        console.log("empty")
        this.value = 0
        this.percent = 0
      }
      if (this.f.discount_input.value == null) {
        console.log("null")
        this.value = 0
        this.percent = 0
      } 
      if (this.f.acceptTerms.value == true) {
        this.is_vat_includedAdd = 1
      }
      if (this.f.acceptTerms.value == false) {
        this.is_vat_includedAdd = 0
      }
      console.log("this.f.acceptTerms.value"+this.f.acceptTerms.value)
      console.log("this.f.acceptTerms.value"+this.is_vat_includedAdd)
      this.PlanService.editPlan({ is_vat_included: this.is_vat_includedAdd, amount: this.f.amount.value, description: this.f.description.value, discount_percent: this.percent, discount_value: this.value, duration: this.f.duration.value, gap: this.f.gap.value, name: this.f.name.value, repetation: this.f.repetation.value }, id)
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.updateSuccesfuly = accept; console.log(this.updateSuccesfuly) });
            this.getAllPlan()
            this.showSuccess(this.updateSuccesfuly)
            this.PlanForm.reset()

            // this.router.navigate([this.returnUrl]);
          },
          error => {
            this.showDanger(error.error.data.errors)
            // this.alertService.error(error.error.data.errors);
            this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  addVAT(content) {
    console.log("open" + JSON.stringify(this.VATForm.value))
    this.PlanService.settings().subscribe(data => {this.vat  = data.data.vat;
      console.log("this.vat " + JSON.stringify(this.vat ))
      this.VATForm.patchValue({
        vat: this.vat 
      })
    })
     
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      
      this.PlanService.editVAT(this.VATForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((Succesfuly: string) => { this.updateSuccesfuly = Succesfuly; console.log(this.addSuccesfuly) });
            this.getAllPlan()
            this.showSuccess(this.updateSuccesfuly)
            this.VATForm.reset()
           
            // this.router.navigate([this.returnUrl]);
          },
          error => {
            this.showDanger(error.error.data.errors)
            // this.alertService.error(error.error.data.errors);
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {

    this.PlanForm.reset()
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
      this.PlanForm.reset()
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
      this.PlanForm.reset()
    } else {

      return `with: ${reason}`;

    }
  }

}
