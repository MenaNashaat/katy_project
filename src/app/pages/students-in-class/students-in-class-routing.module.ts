import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentsInClassComponent } from '../students-in-class/students-in-class.component';


const routes: Routes = [{ path: '', component: StudentsInClassComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsInClassRoutingModule { }
