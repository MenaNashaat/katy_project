import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsInClassComponent } from './students-in-class.component';

describe('StudentsInClassComponent', () => {
  let component: StudentsInClassComponent;
  let fixture: ComponentFixture<StudentsInClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsInClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsInClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
