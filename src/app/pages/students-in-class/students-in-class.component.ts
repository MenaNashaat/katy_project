import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { StudentInClassService , SpinnerService} from '../../services';
import { first } from 'rxjs/operators';
import { listAplications, Tabel } from '../../models';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-students-in-class',
  templateUrl: './students-in-class.component.html',
  styleUrls: ['./students-in-class.component.css']
})
export class StudentsInClassComponent implements OnInit {
  getbranchId: any;
  students: any;
  classDetails: any
  assignForm: FormGroup;
  //  pagination
  pagination: Tabel;
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  per_page: Tabel;
  total: Tabel;
  previousPage: any;
  levels: any;
  closeResult: string;
  class: any;
  update: string;
  branchName: any;
  levelName: any;
  levelId: any;
  Teacher: any;
  Teacher_frist: any;
  deleteSucc: string;
  TeacherID:any
  levelinfo: any;
  branchinfo: any;
  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private StudentInClassService: StudentInClassService,
    private ActivatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private SpinnerService:SpinnerService
  ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllStudents()
    this.getClassName()
    this.assignForm = this.formBuilder.group({
      class_id: ['', Validators.required],
      level: ['', Validators.required],
    });
  }
  get f() { return this.assignForm.controls; }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  selectClass($event) {
    console.log("$event.target.value" + $event.target.value)
    this.StudentInClassService.getClass($event.target.value).subscribe(
      data => {
        this.class = data.data
      },
      error => {
        console.log(JSON.stringify(error))
        // this.showDanger(error.error.data.errors)
      });

  }
  selected = []

  messages = [];

  // check if the item are selected
  checked(item) {
    if (this.selected.indexOf(item) != -1) {
      return true;
    }
  }

  // when checkbox change, add/remove the item from the array
  onChange(checked, item) {
    if (checked) {
      this.selected.push(item);
    } else {
      this.selected.splice(this.selected.indexOf(item), 1)
    }
  }

  save() {
    this.messages.push(this.selected.sort());
  }
  getClassName() {
    const ClassId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.StudentInClassService.getClassDetails(ClassId).subscribe(data =>
       {this.classDetails = data.data.name;
        this.levelinfo = data.data.level ; 
        this.branchinfo = data.data.branch ; 
        this.Teacher_frist = data.data.teachers;
        this.TeacherID = [];
          for (let i = 0; i < this.Teacher_frist.length ; i++) {
            this.TeacherID.push(
                 this.Teacher_frist[i].id,
                  
              ); 
          }
        console.log("MenaMenaMena"+JSON.stringify(this.TeacherID))
      })
  }
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      console.log("ssasasasa" + page) 
      this.getAllStudents();
    }
  }
  getAllStudents() {
    const BranchId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.StudentInClassService.getStudent(BranchId, this.previousPage).subscribe(data => {
      this.students = data.data;
      this.branchName = data.data[0].class.branch.name;
      this.levelName = data.data[0].class.level.name;
      this.levelId = data.data[0].class.level.id;
      console.log("this.students" + JSON.stringify(this.students))
      this.getbranchId = data.data[0].class.branch;
      this.pagination = data.meta;
      this.total = data.meta.total;
      this.per_page = data.meta.per_page
      console.log(JSON.stringify(this.getbranchId))
    })
  }
 
  delete(content) {
    const ClassId = this.ActivatedRoute.snapshot.paramMap.get('id');
    console.log("this.TeacherID"+this.TeacherID)
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.StudentInClassService.unassign({class_id: ClassId , ids: [this.TeacherID] })
      .pipe(first())
      .subscribe(
        data => {
          this.getClassName()
          this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
          this.showDanger(this.deleteSucc)
        },

        error => {
          alert(error.error.data.errors)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);

        });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  open(content) {
    this.StudentInClassService.getLevel(this.getbranchId.id).subscribe(data => {
      this.levels = data.data;
      console.log("sssssssssss" +JSON.stringify( this.getbranchId.id))
      console.log("sssssssssss" + JSON.stringify(this.levels))
    })



    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      console.log("sssssssssss111111111" + this.f.class_id.value)
      console.log("sssssssssss" + this.selected)
      var arr = []
      // arr.push(this.selected)
      // console.log("sssssssssss" +arr)
      this.StudentInClassService.upgrade({ class_id: this.f.class_id.value, student_ids: this.selected })
        .pipe(first())
        .subscribe(
          data => {
            this.getAllStudents();
            this.translate.get('backend.USuccesfuly').subscribe((accept: string) => { this.update = accept });
            this.showSuccess(this.update)
            this.assignForm.reset()
          },
          error => {

            this.showDanger(error.error.data.errors)
            // this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.assignForm.reset()
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
