import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAplicationComponent } from './add-aplication.component';

describe('AddAplicationComponent', () => {
  let component: AddAplicationComponent;
  let fixture: ComponentFixture<AddAplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
