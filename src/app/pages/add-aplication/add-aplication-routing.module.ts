import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddAplicationComponent } from './add-aplication.component';

const routes: Routes = [{ path: '', component: AddAplicationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddAplicationRoutingModule { }
