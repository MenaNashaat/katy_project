import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { JsonPipe } from '@angular/common';
import { StudentService } from '../../services/student/student.service';
import { listAplications } from '../../models/listAplications'
import { WattingService, SpinnerService } from '../../services';
import { ListApplicationsService } from '../../services/list-applications.service';
import { ToastService } from '../../services/toast/toast.service';
import { TranslateService } from "@ngx-translate/core";
import { NgxSpinnerService } from "ngx-spinner";
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-add-aplication',
  templateUrl: './add-aplication.component.html',
  styleUrls: ['./add-aplication.component.css']
})


export class AddAplicationComponent implements OnInit {
  userFormData = [
    {

      radio: 'radio_1',
      input: 'input_1',
      "question": "Question 2 for child medical info"
    },
    {
      radio: 'radio_2',
      input: 'input_2',
      "question": "Question 2 for child medical info"
    }
  ];

  userGroup = {};
  myFormGroup: FormGroup;
  Medical;
  medicalArr = [];
  medicalPartAr: boolean;
  medicalPartEn: boolean;
  Nationalities: any;
  Languages: any;
  SchoolAfterList: boolean;
  date: { year: number, month: number };
  model: NgbDateStruct;
  GuardianForm: FormGroup;
  form: FormGroup;
  formGroup: FormGroup = new FormGroup({});
  checked = new FormControl(false);
  Hours: any;
  GuardianCheck: boolean;
  loading = false;
  submitted = false;
  level: any;
  level1: any = [];
  levelunique: any;
  arr: any;
  name: string;
  Branch: any;
  Createsuccessful: string;
  lang: any
  radioText: string;
  levelName: string;
  levelId: string;
  radioRes: any;
  inputRes: any;
  searchText = '';
  searchTextlang = '';
  characters: any = [];
  camp: boolean;
  summerCamp: boolean;
  winterCamp: boolean;
  showMedicalSec: boolean;
  numOfYear: any;
  showLeavelBranch: boolean;
  levels: any = [];
  levelInsideBranch: any;
  BranchID: any;
  showCamp: boolean;
  showMonthWinter: boolean;
  monthList: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  monthList1: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  showMonthSummer: boolean;
  WinterCampMonths: any;
  SummerCampMonths: any;
  Settings: any;
  showCheckWinter: boolean;
  showCheckSummer: boolean;
  ResmonthWinter: any;
  ResmonthSummer: any[];
  Parent: any;
  guardianDetails: any;
  userID: any;
  constructor(
    private calendar: NgbCalendar,
    private formBuilder: FormBuilder,
    private router: Router,
    private translate: TranslateService,
    private ToastService: ToastService,
    private ListApplicationsService: ListApplicationsService,
  ) { }


  ngOnInit() {
    this.getAllBranch()
    this.getWinterCampMonths()
    this.getSummerCampMonths()
    this.getSettings()
    this.getAllParent()
    //let currentUser = this.authenticationService.currentUserValue;
    this.translate.get('backend.Createsuccessful').subscribe((text: string) => this.Createsuccessful = text);


    // this.name = currentUser.data.user.first_name;

    this.ListApplicationsService.getMedical().subscribe(data => {
      this.Medical = data.data;

      if (this.Medical === undefined || this.Medical.length == 0) {
        // array empty or does not exist

        this.showMedicalSec = false
      }
      else {

        this.showMedicalSec = true
        if (localStorage.getItem("language") == "en") {
          this.medicalPartAr = false
          this.medicalPartEn = true
        }
        else {
          this.medicalPartAr = true
          this.medicalPartEn = false
        }
        let control = new FormControl('');
        for (let input of this.Medical) {
          this.GuardianForm.addControl(input.input, new FormControl(''));
          this.GuardianForm.addControl(input.radio, new FormControl(''));
        }
      }
    });

    this.GuardianForm = this.formBuilder.group({

      first_name: ['',],
      email: ['', [Validators.email]],
      mobile: ['',],
      Relation: ['',],
      // occupation: ['', Validators.required],
      guardianOccupation: ['',],
      child_first_name: ['', Validators.required],
      child_middle_name: ['', Validators.required],
      child_last_name: ['', Validators.required],
      child_date_of_birth: ['', Validators.required],
      child_level: ['', Validators.required],
      nationality: ['', Validators.required],
      language: ['', Validators.required],
      branch_id: ['', Validators.required],
      Mother_name: ['', Validators.required],
      Mother_Mobile: ['', Validators.required],
      Mother_email: ['', [Validators.required, Validators.email]],
      gender: ['', Validators.required],
      hour: [''],
      camp: [''],
      mother_job: ['', Validators.required],
      father_name: ['', Validators.required],
      father_job: ['', Validators.required],
      father_Mobile: ['', Validators.required],
      father_email: ['', [Validators.required, Validators.email]],
      name_ar: ['', Validators.required],
      lives_with: ['', Validators.required],
      parents_are: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue],
      summer_camp: [false],
      winter_camp: [false],
      IDIqama: [''],
      address: [''],
      Ability_eat: [''],
      Ability_hand: [''],
      Ability_toilet: [''],
      Emergency_first_name: ['', Validators.required],
      Emergency_last_name: ['', Validators.required],
      Emergency_mobile: ['', Validators.required],
      Emergency_Alternative_Mobile: ['', Validators.required],
      Emergency_Relation_Family: ['', Validators.required],
      Emergency_Address: ['', Validators.required],
      Emergency_Permission: [''],
      monthWinter: [''],
      monthSummer: [''],
      type: new FormControl(''),

      info_name: ['',],
      info_Email: ['',],
      info_mobile: ['',],
      info_Alternative: ['', Validators.required],
      info_IDIqama: ['', Validators.required],
      info_Relation: ['', Validators.required],
      info_Occupation: ['',],
      info_Phone: ['', Validators.required],
      info_address: ['', Validators.required],
    });
    this.checked.valueChanges.subscribe((bool: boolean) => {
      bool ? this.GuardianForm.get('hour').setValidators(Validators.required) : this.GuardianForm.get('hour').clearValidators();
      this.GuardianForm.get('hour').updateValueAndValidity();
    });
    this.ListApplicationsService.getLanguage().subscribe(data => { this.Languages = data.data; });

    this.ListApplicationsService.getNationality().subscribe(data => { this.Nationalities = data.data; });
    this.ListApplicationsService.getHour().subscribe(data => { this.Hours = data.data; });
    this.ListApplicationsService.getLevels().subscribe(data => { this.level = data.data });
    // this.ListApplicationsService.getBranch().subscribe(data => { this.Branch = data.data; });
    this.ListApplicationsService.sharedMessage.subscribe(message => {
      this.lang = message;

      if (this.lang == "ar") {
        console.log("ifff")
        this.medicalPartAr = true
        this.medicalPartEn = false
        this.ListApplicationsService.getLevel_ar().subscribe(data => { this.level = data.data });
      }
      else if (this.lang == "en") {
        this.ListApplicationsService.getLevel_en().subscribe(data => { this.level = data.data });
        console.log("else")
        this.medicalPartAr = false
        this.medicalPartEn = true
      }
    })




  }

  // convenience getter for easy access to form fields
  get f() { return this.GuardianForm.controls; }
  get t() { return this.myFormGroup.controls; }

  showSuccess() {
    this.ToastService.show(this.Createsuccessful, { classname: 'bg-success text-light', delay: 4000 });
  }

  showDanger(message) {
    this.ToastService.show(message, { classname: 'bg-danger text-light', delay: 3000 });
  }
  changeGender(e) {

    if (e.target.value == "Fullday and Extra" || e.target.value == "Extra") {
      this.SchoolAfterList = true;
      this.showCamp = false
      this.GuardianForm.get('hour').setValidators(Validators.required)
      this.GuardianForm.get('hour').updateValueAndValidity();
    }
    else if (e.target.value == "Fullday") {

      this.SchoolAfterList = false;
      this.showCamp = false
      this.GuardianForm.get('hour').setValidators([]); // or clearValidators()
      this.GuardianForm.get('hour').updateValueAndValidity();
    }
    else if (e.target.value == "Camp") {

      this.SchoolAfterList = false;
      this.showCamp = true
      this.GuardianForm.get('hour').setValidators([]); // or clearValidators()
      this.GuardianForm.get('hour').updateValueAndValidity();
    }
  }
  changeCamp(e) {

    if (e.target.value == "winter_camp") {
      this.winterCamp = true;
      this.summerCamp = false;

    }
    else if (e.target.value == "summer_camp") {

      this.winterCamp = true;
      this.summerCamp = false;

    }
  }
  getGurdian(e) {
    this.guardianDetails = this.Parent.filter(function (item) {
      return item.email == e.target.value;
    });
    this.userID = this.guardianDetails[0].id
    
  }
  onDateSelect(event) {

    var d = new Date();
    var getFullYear = d.getFullYear();
    this.numOfYear = getFullYear - event.year
    this.showLeavelBranch = true
    this.getLevelByDate();
  }

  validateIfChecked(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (this.checked) {
        return control.value ? null : { 'err': true };
      }
      return null;
    }
  }
  toggleGuardian(event) {
    if (event.target.checked) {
      this.GuardianCheck = true;
    }
    else
      this.GuardianCheck = false;
  }
  winterCampFunc(e) {
    if (e.target.checked) {
      this.showMonthWinter = true
    }
    else {
      this.showMonthWinter = false
    }
  }
  summerCampFunc(e) {
    if (e.target.checked) {
      this.showMonthSummer = true
    }
    else {
      this.showMonthSummer = false
    }
  }
  getAllBranch() {
    this.ListApplicationsService.getBranch().subscribe(data => { this.Branch = data.data });
  }
  getSettings() {
    this.ListApplicationsService.getSettings().subscribe(data => {
      this.Settings = data.data;
      if (this.Settings.winterCamp == '1') { this.showCheckWinter = true }
      else { this.showCheckWinter = false }
      if (this.Settings.summerCamp == '1') { this.showCheckSummer = true }
      else { this.showCheckSummer = false }
    });
  }
  getLevel(event) {
    this.BranchID = event.target.value
    this.ListApplicationsService.getLevelBy_branchID(this.BranchID).subscribe(data => {

      this.levelInsideBranch = data.data;
      for (let i = 0; i < this.levelInsideBranch.length; i++) {
        if (this.numOfYear >= this.levelInsideBranch[i].min_age && this.numOfYear <= this.levelInsideBranch[i].max_age) {
          this.levels.push(this.levelInsideBranch[i])
        }
      }
    });
  }
  getLevelByDate() {

    this.ListApplicationsService.getLevelBy_branchID(this.BranchID).subscribe(data => {
      this.levels = []
      this.levelInsideBranch = data.data;
      for (let i = 0; i < this.levelInsideBranch.length; i++) {

        if (this.numOfYear >= this.levelInsideBranch[i].min_age && this.numOfYear <= this.levelInsideBranch[i].max_age) {

          this.levels.push(this.levelInsideBranch[i])
        }
      }
    });
  }
  getWinterCampMonths() {
    this.ListApplicationsService.getWinterCampMonths().subscribe(data => { this.WinterCampMonths = data.data });
  }
  getSummerCampMonths() {
    this.ListApplicationsService.getSummerCampMonths().subscribe(data => { this.SummerCampMonths = data.data });
  }
  getAllParent() {
    this.ListApplicationsService.getParent().subscribe(data => {
      this.Parent = data.data;


    })
  }

  // getLevel(event) {
  //   this.levelName = event.target.value
  //   this.ListApplicationsService.getBranchByLevel(event.target.value).subscribe(data => { this.Branch = data.data; });
  // }
  // getBranch(event) {
  //   this.ListApplicationsService.getLevelByLevel_branch(this.levelName, event.target.value).subscribe(data => { this.levelId = data.data.id; console.log(JSON.stringify(this.levelId)) });
  // }


  onSubmit() {

    this.submitted = true;

    let date_of_birth = this.f.child_date_of_birth.value
    console.log("date_of_birth" + date_of_birth)
    if (date_of_birth != null && date_of_birth != '') {
      if (date_of_birth.month.toString().length < 2) {
        date_of_birth.month = "0" + date_of_birth.month;
      }
      else {
        date_of_birth.month = date_of_birth.month;
      }
      if (date_of_birth.day.toString().length < 2) {
        date_of_birth.day = "0" + date_of_birth.day;
      }
      else {
        date_of_birth.day = date_of_birth.day;
      }
    }

    let date = date_of_birth.year + "-" + date_of_birth.month + "-" + date_of_birth.day


    const myObject = this.GuardianForm.value;

    // var sliced = Object.keys(myObject).slice(36, myObject.length).reduce((result, key) => {
    //   result[key] = myObject[key];

    //   return result;
    // }, {});
    // console.log("sliced" + JSON.stringify(sliced))
    // Object.values(sliced).forEach((value) => {
    //   if (value == '') {
    //     if (this.GuardianForm.invalid) {
    //       return;
    //     }
    //   }

    // })


    this.medicalArr = []
    for (var i = 0; i < this.Medical.length; i++) {

      var idInpu = this.Medical[i].id
      var input = 'this.f.input_' + this.Medical[i].id + '.value'
      var radio = 'this.f.radio_' + this.Medical[i].id + '.value'
      this.medicalArr.push({ "id": idInpu, "valid": eval(radio), "comment": eval(input) })

    }

    let count = 0;
    this.medicalArr.forEach((val, i) => { if (!val.comment) delete val["comment"]; });

    console.log("JSON.stringify(aarr);" + JSON.stringify(this.medicalArr))
    var aValue = JSON.parse(localStorage.getItem("Guardian"));

    if (this.f.monthWinter.value != '') { this.ResmonthWinter = this.f.monthWinter.value }
    else { this.ResmonthWinter = [] }
    if (this.f.monthSummer.value != '') { this.ResmonthSummer = this.f.monthSummer.value }
    else { this.ResmonthSummer = [] }
    //medical:[{"id" :1 , "valid" : 0 , comment: "first question commentddddddddddd"}]

    // father_phone: this.f.father_Mobile, 
    // father_email: this.f.father_email, mother_email: this.f.Mother_email, 
    // mother_phone: this.f.Mother_Mobile, name_ar: this.f.name_ar,
    this.arr = {
      guardian: {
        first_name: this.f.first_name.value, email: this.f.email.value, mobile: this.f.mobile.value, occupation: this.f.guardianOccupation.value,
        relation: this.f.Relation.value, national_id: this.f.IDIqama.value, address: this.f.address.value
      },
      child: {
        first_name: this.f.child_first_name.value, middle_name: this.f.child_middle_name.value, last_name: this.f.child_last_name.value, level: this.levelId, date_of_birth: date,
        lives_with: this.f.lives_with.value, parents_are: this.f.parents_are.value, mother_name: this.f.Mother_name.value, mother_job: this.f.mother_job.value, father_job: this.f.father_job.value, father_name: this.f.father_name.value, language: this.f.language.value, nationality: this.f.nationality.value, gender: this.f.gender.value,
        father_phone: this.f.father_Mobile.value,
        father_email: this.f.father_email.value, mother_email: this.f.Mother_email.value,
        mother_phone: this.f.Mother_Mobile.value, name_ar: this.f.name_ar.value,
        medical: this.medicalArr
      },
      branch_id: this.f.branch_id.value,
      national_id: this.f.info_IDIqama.value, relation: this.f.info_Relation.value, work_phone: this.f.info_mobile.value, alternative_phone: this.f.info_Alternative.value, address: this.f.info_address.value, occupation: this.f.info_Occupation.value,

      contact_first_name: this.f.Emergency_first_name.value, contact_last_name: this.f.Emergency_last_name.value, contact_mobile: this.f.Emergency_mobile.value, contact_alt_mobile: this.f.Emergency_Alternative_Mobile.value,
      contact_relation: this.f.Emergency_Relation_Family.value, contact_address: this.f.Emergency_Address.value, can_pick_child: this.f.Emergency_Permission.value
      , can_eat_alone: this.f.Ability_eat.value, use_right_hand: this.f.Ability_hand.value, go_toilet_alone: this.f.Ability_toilet.value, type: this.f.type.value,

    }
    if (this.f.first_name.value == '' && this.f.email.value == '' && this.f.mobile.value == '' && this.f.Relation.value == '') {
      console.log("enter Delete")
      delete this.arr.guardian
    }
    if (this.f.first_name.value == '' && this.f.email.value == '' && this.f.mobile.value == null && this.f.Relation.value == '') {
      console.log("enter Delete")
      delete this.arr.guardian
    }
    if (this.SchoolAfterList == true) {
      Object.assign(this.arr, { after_school: this.f.hour.value });
    }
    if (this.showCamp == true) {

      Object.assign(this.arr, { winter_camp: this.ResmonthWinter, summer_camp: this.ResmonthSummer });
    }



    // stop here if form is invalid
    // if (this.GuardianForm.invalid) {
    //   console.log("stop here if form is invalid" + this.GuardianForm.invalid)
    //   return;
    // }

    this.loading = true;
    console.log("his.GuardianForm.value" + JSON.stringify(this.arr))
    this.ListApplicationsService.addApplication(this.arr, this.userID)
      .pipe(first())
      .subscribe(
        data => {
          this.showSuccess()
          // this.alertService.success(this.Createsuccessful , true);
          this.router.navigate(['/registration']);
        },
        error => {
          console.log("no" + JSON.stringify(error))
          this.showDanger(error.error.data.errors[0]);
          window.scroll({ top: 0, behavior: 'smooth' });
          this.loading = false;
        });
  }

}
