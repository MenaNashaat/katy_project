import { Component, OnInit ,  ElementRef, ViewChild} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { MealService , SpinnerService} from '../../services';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.css']
})

export class MealComponent implements OnInit {
  closeResult = '';
  meals: Meal;
  loading = false;
  submitted = false;
  MealForm: FormGroup;
  selectedFile: File;
  deleteSucc: string;

  // selectedFile: File;
  updateSuccesfuly: string;
  addSuccesfuly:string;

  constructor(
    private http:HttpClient,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private mealService: MealService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private SpinnerService:SpinnerService
  ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllMeals()
    this.MealForm = this.formBuilder.group({
      profile: [''],
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required],
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required])
     
     
    });
    
  }
 
   // convenience getter for easy access to form fields
   get f() { return this.MealForm.controls; }
 
  
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllMeals() {
    
    this.mealService.getMeal().subscribe(data => {
      this.meals = data.data;
      console.log("this.meals" + JSON.stringify(this.meals))
    })
  }
  
  delete(content, id) {
   

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.mealService.deleteMeal(id)
      .pipe(first())
      .subscribe(
        data => {
          this.getAllMeals()
          this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
          this.showDanger(this.deleteSucc)
        },

        error => {
          alert(error.error.data.errors)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);

        });
     
      

      

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
 

  onFileChanged(event) {
    this.selectedFile = event.target.files[0]
  }

  onUpload() {
    // upload code goes here
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      var formData: any = new FormData();
      if (this.selectedFile)
      {
        formData.append("name", this.f.name.value);
        formData.append("price", this.f.price.value);
        formData.append("description", this.f.description.value);
        formData.append('photo', this.selectedFile, this.selectedFile.name);
      }
      else
      {
        formData.append("name", this.f.name.value);
        formData.append("price", this.f.price.value);
        formData.append("description", this.f.description.value);
      }
     
      this.mealService.createMeal( formData)
        .pipe(first())
        .subscribe(
          data => {
           
            this.getAllMeals()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
            this.showSuccess(this.updateSuccesfuly)
            this.MealForm.reset()
          },
          error => {
            console.log(JSON.stringify(error.error.data.errors))
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
