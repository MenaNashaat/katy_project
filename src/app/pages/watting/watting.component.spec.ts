import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WattingComponent } from './watting.component';

describe('WattingComponent', () => {
  let component: WattingComponent;
  let fixture: ComponentFixture<WattingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WattingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WattingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
