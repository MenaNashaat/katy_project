import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { WattingService, SpinnerService } from '../../services';
import { Tabel } from '../../models';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-watting',
  templateUrl: './watting.component.html',
  styleUrls: ['./watting.component.css']
})
export class WattingComponent implements OnInit {
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  pagination: Tabel;
  per_page: Tabel;
  total: Tabel;
  previousPage: any;
  closeResult = '';
  meals: Meal;
  loading = false;
  submitted = false;
  questionForm: FormGroup;
  addForm: FormGroup;
  editForm: FormGroup;
  deleteSucc: string;
  users: any;
  updateSuccesfuly: string;
  Evaluations: any;
  QuestionDetails: any;
  Watting: any;
  level: any;
  WattingDetails: any;
  branch: any;
  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private WattingService: WattingService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private SpinnerService: SpinnerService) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllwaitting()
    this.getBranchs()
    this.getLevels()
    this.addForm = this.formBuilder.group({
      parent_name: ['', Validators.required],
      child_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email] ],
      mobile: ['', Validators.required],
      level_id: ['', Validators.required],
      description: [''],
      branch: []
    });
    this.editForm = this.formBuilder.group({
      parent_name: ['', Validators.required],
      child_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email] ],
      mobile: ['', Validators.required],
      level_id: ['', Validators.required],
      description: [''],
      branch: []
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.addForm.controls; }
  get e() { return this.editForm.controls; }
  
  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;

      this.getAllwaitting()
    }
  }
  selectBranch($event) {
    this.WattingService.allLevel($event.target.value).subscribe(data => this.level = data.data)

  }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getLevels(){
    this.WattingService.levelWithBranch().subscribe(data => {
      this.level = data.data; 
      
    })
  }
  getBranchs() {
    // this.WattingService.allBranch().subscribe(data => {
    //   this.branch = data.data;
    //   console.log("this.meals" + JSON.stringify(data))
    // })
  }
  getAllwaitting() {
    this.WattingService.getAllWatting(this.previousPage).subscribe(data => {
      this.Watting = data.data;
      this.pagination = data.meta;
      this.total = data.meta.total;
      this.per_page = data.meta.per_page

    })
  }
  delete(content, id) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.WattingService.deleteWatting(id)
        .pipe(first())
        .subscribe(
          data => {

            this.getAllwaitting()
            this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
            this.showDanger(this.deleteSucc)
          },

          error => {
            alert(error.error.data.errors)
            //this.showDanger(error.error.data.errors)
            // this.alertService.error(this.error);

          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  edit(content, id) {

    this.WattingService.getAllWattingID(id).subscribe(data => {
      this.WattingDetails = data.data;
      this.editForm.patchValue({

        parent_name: this.WattingDetails.parent_name,
        child_name: this.WattingDetails.child_name,
        email: this.WattingDetails.email,
        mobile: this.WattingDetails.mobile,
        level_id: this.WattingDetails.level.id,
        description: this.WattingDetails.description,
      })

    })


    // });

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

      // stop here if form is invalid
      if (this.editForm.invalid) {
        this.showDanger("Invalid Field")
        return;
      }
      this.WattingService.editWatting(this.editForm.value, id)
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });

            this.showSuccess(this.updateSuccesfuly)
            this.getAllwaitting()

            // this.showSuccess(this.updateSuccesfuly)
            this.addForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            // this.showDanger(error.error.data.errors)
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  open(content) {
    
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      // stop here if form is invalid
      if (this.addForm.invalid) {
        this.showDanger("Invalid Field")
        return;
      }
      this.WattingService.createUser({
        child_name: this.f.child_name.value,
        description: this.f.description.value,
        email: this.f.email.value,
        level_id: this.f.level_id.value,
        mobile: this.f.mobile.value,
        parent_name: this.f.parent_name.value
      })
        .pipe(first())
        .subscribe(
          data => {

            this.getAllwaitting()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });

            this.showSuccess(this.updateSuccesfuly)
            this.addForm.reset()

          },
          error => {
            console.log(JSON.stringify(error))
            this.showDanger(error.error.data.errors)
            this.loading = false;

          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
