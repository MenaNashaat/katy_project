import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WattingComponent } from './watting.component';


const routes: Routes = [{ path: '', component: WattingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WattingRoutingModule { }
