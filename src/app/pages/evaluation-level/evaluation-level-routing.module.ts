import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EvaluationLevelComponent } from '../evaluation-level/evaluation-level.component';


const routes: Routes = [{ path: '', component: EvaluationLevelComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluationLevelRoutingModule { }
