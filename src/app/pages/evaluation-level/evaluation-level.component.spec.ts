import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluationLevelComponent } from './evaluation-level.component';

describe('EvaluationLevelComponent', () => {
  let component: EvaluationLevelComponent;
  let fixture: ComponentFixture<EvaluationLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluationLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluationLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
