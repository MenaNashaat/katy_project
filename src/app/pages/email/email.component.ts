import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { EmailTemplateService, SpinnerService , ListService} from '../../services';
import { Tabel } from '../../models';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  //  pagination
  pagination: Tabel;
  itemsPerPage: number;
  totalItems: any;
  page = 1;
  per_page: Tabel;
  total: Tabel;
  previousPage: any;

  closeResult = '';
  meals: Meal;
  loading = false;
  submitted = false;
  questionForm: FormGroup;
  addForm: FormGroup;
  branchForm: FormGroup;
  addFormEmails: FormGroup;
  assignEvaluation: FormGroup;
  sendFormEmails:FormGroup;
  editEmail: FormGroup;
  deleteSucc: string;
  users: any;
  updateSuccesfuly: string;
  Emails: any;
  QuestionDetails: any;
  grade: boolean = false;
  questionOption: boolean = false;
  gradeEdit: boolean = false;
  questionOptionEdit: boolean = false;
  optionORgrade: string;
  arr: any;
  levels: any;

  Branch: any;
  level: any;
  formDetails: any;
  brannchValue: any;
  levelsInsideForm: any;
  idAssign: any[];
  Lists: any;
  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private EmailTemplateService: EmailTemplateService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private ListService:ListService,
    private SpinnerService: SpinnerService) { }


  ngOnInit(): void {
    this.getAllEmail()
    this.getAllLists()
    this.addFormEmails = this.formBuilder.group({
      name: ['', Validators.required],
      subject: ['', Validators.required],
      body: ['', Validators.required],
    });
    this.editEmail = this.formBuilder.group({
      name: ['', Validators.required],
      subject: ['', Validators.required],
      body: ['', Validators.required],
    });
    this.sendFormEmails = this.formBuilder.group({
      list: ['', Validators.required],
      email: ['', Validators.required],
     
    });
  }

  get k() { return this.addFormEmails.controls; }
  get e() { return this.editEmail.controls; }
  get f() { return this.sendFormEmails.controls; }


  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllLists() {
    this.ListService.getAllLists().subscribe(data => {
       this.Lists = data.data;
     
     })
   }
  getAllEmail() {
    this.EmailTemplateService.getAllMails().subscribe(data => {
      this.Emails = data.data;
      this.pagination = data.meta;
      this.total = data.meta.total;
      this.per_page = data.meta.per_page
    })
  }
  addFormEmail(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

      this.EmailTemplateService.createEmailForm(this.addFormEmails.value)
        .pipe(first())
        .subscribe(
          data => {

            this.getAllEmail()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });

            this.showSuccess(this.updateSuccesfuly)
            this.addFormEmails.reset()

          },
          error => {
            console.log(JSON.stringify(error))
            this.showDanger(error.error.data.errors)
            this.loading = false;

          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  editEmailForm(content, formId) {

    this.EmailTemplateService.getEmailByID(formId).subscribe(data => {
      this.formDetails = data.data;
      this.editEmail.patchValue({
        name: this.formDetails.name,
        subject:  this.formDetails.subject ,
        body: this.formDetails.body,

      })


    })


    // });

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {



      this.EmailTemplateService.editEmail(formId, { name: this.e.name.value, subject:  this.e.subject.value, body: this.e.body.value })
        .pipe(first())
        .subscribe(
          data => {
            this.translate.get('backend.USuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });

            this.showSuccess(this.updateSuccesfuly)
            this.getAllEmail()

            // this.showSuccess(this.updateSuccesfuly)
            //this.LevelForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            // this.showDanger(error.error.data.errors)
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  show(content, formId) {

    this.EmailTemplateService.getEmailByID(formId).subscribe(data => {
      this.formDetails = data.data;
    })

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  delete(content, id) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.EmailTemplateService.deleteEmail(id)
        .pipe(first())
        .subscribe(
          data => {

            this.getAllEmail()
            this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
            this.showDanger(this.deleteSucc)
          },

          error => {
            alert(error.error.data.errors)
            //this.showDanger(error.error.data.errors)
            // this.alertService.error(this.error);

          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  sendFormEmail(content ) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

      this.EmailTemplateService.sendEmailForm(this.f.email.value, {lists:[this.f.list.value]} )
        .pipe(first())
        .subscribe(
          data => {

            this.getAllEmail()
            this.translate.get('backend.SSuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });

            this.showSuccess(this.updateSuccesfuly)
            this.sendFormEmails.reset()

          },
          error => {
            console.log(JSON.stringify(error))
            this.showDanger(error.error.data.errors)
            this.loading = false;

          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
