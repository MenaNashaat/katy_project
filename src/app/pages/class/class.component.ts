import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  ToastService} from '../../services/toast/toast.service';
import { TranslateService } from "@ngx-translate/core";

import { ClassService, LevelService , SpinnerService } from '../../services';
import { first } from 'rxjs/operators';
import {Class} from '../../models';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {
  closeResult = '';
  classForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  class : any;
  classId: Class;
  branch:any;
  levelDetails :any;
   // localization
   updateSuccesfuly:string;
   addSuccesfuly:string;
  level: any;
  levelName: any;
  levelinfo: any;
  branchinfo: any;
  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private ClassService: ClassService,
    private ActivatedRoute: ActivatedRoute,
    private translate:TranslateService,
    private LevelService:LevelService,
    private SpinnerService:SpinnerService
    ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.classForm = this.formBuilder.group({
      name: ['', Validators.required],
      max_students_number: ['', Validators.required],
     
    });
    this.getAllClass()
    this.treeInfo()
    this.translate.get('backend.USuccesfuly').subscribe((accept:string) => {this.updateSuccesfuly = accept ;  console.log(this.updateSuccesfuly)});
    this.translate.get('backend.ASuccesfuly').subscribe((Succesfuly:string) => {this.addSuccesfuly = Succesfuly ; console.log(this.addSuccesfuly)});

  }
  // convenience getter for easy access to form fields
  get f() { return this.classForm.controls; }
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllClass() {
    const BranchId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.ClassService.getclass(BranchId).subscribe(data => {this.class = data.data ; 
      this.branch = data.data[0].branch
      this.levelDetails = data.data[0].level
      console.log(JSON.stringify(this.branch))})
  }
  treeInfo()
  {
    const BranchId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.ClassService.getlevelDetails(BranchId).subscribe(data => {
      this.levelinfo = data.data ; 
      this.branchinfo = data.data.branch ; 
     
      console.log(JSON.stringify(this.levelinfo))})
  }
 
  open(edit, id) {
    this.classForm.reset()
    this.ClassService.getclassById(id).subscribe(data => {this.classId = data.data ; 
      console.log("this.classId " +JSON.stringify( this.classId) )
      this.classForm.patchValue({
        name: this.classId.name,
        max_students_number: this.classId.max_students_number,
      });
     });
   
    this.modalService.open(edit, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      const BranchId = this.ActivatedRoute.snapshot.paramMap.get('id');
      const target = this.classForm.value;
      const source = {"level_id": BranchId};

      const returnedTarget = Object.assign(target, source);
      
    
      this.ClassService.editclass( returnedTarget, id)
        .pipe(first())
        .subscribe(
          data => {
           
            this.getAllClass()

            this.showSuccess(this.updateSuccesfuly)
            this.classForm.reset()
          },
          error => {
            console.log(JSON.stringify(error))
            this.showDanger(error.error.data.errors)
            this.loading = false;
          });
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  addClass(content) {
    console.log("open" + JSON.stringify(this.classForm.value))
    
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      console.log("open" + JSON.stringify(this.classForm.value))

      const levelId = this.ActivatedRoute.snapshot.paramMap.get('id');
      const target = this.classForm.value;
      const source = {"level_id": levelId};
      const returnedTarget = Object.assign(target, source);
      this.ClassService.createclass(returnedTarget)
        .pipe(first())
        .subscribe(
          data => {

            this.getAllClass()
            this.showSuccess(this.addSuccesfuly)
            this.classForm.reset()

            // this.router.navigate([this.returnUrl]);
          },
          error => {
            this.showDanger(error.error.data.errors)
            // this.alertService.error(error.error.data.errors);
            this.loading = false;
          });

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
