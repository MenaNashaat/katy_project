import { Component, OnInit } from '@angular/core';
import { BillService , SpinnerService } from '../../services';
import { Branch } from '../../models';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";
import { ToastService } from '../../services/toast/toast.service';

import { first } from 'rxjs/operators';
@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css']
})
export class BillComponent implements OnInit {
  Bills: any;
  applicationId: string;
  deleteSucc: string;
  constructor(
    private BillService: BillService,
    private ActivatedRoute: ActivatedRoute,
    private SpinnerService:SpinnerService,
    private translate: TranslateService,
    private toastService: ToastService,

  ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllBills()
  }
  getColor(Status) {
    switch (Status) {
      case 'Paid':
        return 'green';
      case 'Pending':
        return '#ffc107';
      case 'Cancelled':
        return 'red';
    }
  }
  getAllBills() {
    const Id = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.applicationId =this.ActivatedRoute.snapshot.paramMap.get('id')
    this.BillService.getBill(Id).subscribe(data => {this.Bills = data.data  })

  }
  showDanger(error) {
    this.toastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  cancel(item){
  
    this.BillService.Cancel(item.id , {'amount': item.due_amount})
      .pipe(first())
      .subscribe(
        data => {
          this.getAllBills()
          this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
          this.showDanger(this.deleteSucc)
        },

        error => {
          alert(error.error.data.errors)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);

        });
  }

}
