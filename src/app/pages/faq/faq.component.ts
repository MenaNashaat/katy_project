import { Component, OnInit , ViewChild} from '@angular/core';
import {MatAccordion} from '@angular/material/expansion';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../services/toast/toast.service';
import { MealService , FAQService , SpinnerService} from '../../services';
import { first } from 'rxjs/operators';
import { Meal } from '../../models';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FAQComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  closeResult = '';
  meals: Meal;
  loading = false;
  submitted = false;
  questionForm: FormGroup;
  answerForm: FormGroup;
  selectedFile: File;
  deleteSucc: string;

  // selectedFile: File;
  updateSuccesfuly: string;
  addSuccesfuly:string;  panelOpenState = false;
  FAQ: any;
  constructor(
   
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private ToastService: ToastService,
    private mealService: MealService,
    private translate: TranslateService,
    private ActivatedRoute: ActivatedRoute,
    private FAQService:FAQService,
    private SpinnerService:SpinnerService
    ) { }

  ngOnInit(): void {
    this.SpinnerService.loader()
    this.getAllFAQ()
    
    this.questionForm = this.formBuilder.group({
      question: [''],
      ar_question: [''],
    });
    this.answerForm = this.formBuilder.group({
      answer: [''],
      ar_answer: [''],
    });
    
  }
  

   // convenience getter for easy access to form fields
   get f() { return this.questionForm.controls; }
   get g() { return this.answerForm.controls; }
  
  showSuccess(message) {
    this.ToastService.show(message, { classname: 'bg-success text-light', delay: 4000 });
  }
  showDanger(error) {
    this.ToastService.show(error, { classname: 'bg-danger text-light', delay: 3000 });
  }
  getAllFAQ() {
    
    this.FAQService.getFAQ().subscribe(data => {
      this.FAQ = data.data;
      console.log("this.FAQ" + JSON.stringify(this.FAQ))
    })
  }
  
  delete(content, questionID , answerID) {
   

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.FAQService.deleteQuestion(questionID)
      .pipe(first())
      .subscribe(
        data => {
         
          this.getAllFAQ()
         
        },

        error => {
          alert(error.error.data.errors)
          //this.showDanger(error.error.data.errors)
          // this.alertService.error(this.error);

        });
     
      
        this.FAQService.deleteAnswer(answerID)
        .pipe(first())
        .subscribe(
          data => {
            this.getAllFAQ()
            this.translate.get('backend.deleteSucc').subscribe((acceptText: string) => { this.deleteSucc = acceptText });
            this.showDanger(this.deleteSucc)
          },
  
          error => {
            alert(error.error.data.errors)
            //this.showDanger(error.error.data.errors)
            // this.alertService.error(this.error);
  
          });
      

      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
 

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      
    console.log("{question: this.f.question.value, ar_question: this.f.ar_question.value}"+ JSON.stringify(this.questionForm.value))
      this.FAQService.createFAQ( {'question': this.f.question.value , 'ar_question': this.f.ar_question.value})
        .pipe(first())
        .subscribe(
          data => {
           
            this.getAllFAQ()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
            this.showSuccess(this.updateSuccesfuly)
            this.questionForm.reset()
            this.answerForm.reset()
          },
          error => {
            console.log(JSON.stringify(error.error.data.errors))
            this.showDanger(error.error.data.errors)
            this.loading = false;
            this.questionForm.reset()
            this.answerForm.reset()
          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  addAnswer(content , id) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      
      this.FAQService.createAnswer( {'answer': this.g.answer.value , 'ar_answer': this.g.ar_answer.value} , id)
        .pipe(first())
        .subscribe(
          data => {
           
            this.getAllFAQ()
            this.translate.get('backend.ASuccesfuly').subscribe((acceptText: string) => { this.updateSuccesfuly = acceptText });
          
            this.showSuccess(this.updateSuccesfuly)
            this.questionForm.reset()
            this.answerForm.reset()
          },
          error => {
            console.log(JSON.stringify(error.error.data.errors))
            this.showDanger(error.error.data.errors)
            this.loading = false;
            this.questionForm.reset()
            this.answerForm.reset()
          });


      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
