import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GeneralInfoService   } from '../generalInfo/general-info.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
export const darkTheme = {
  'primary-color': '#455363',
  'background-color': '#f00',
  'text-color': '#fff'
};

export const lightTheme = {
  'primary-color': '#fff',
  'background-color': '#e5e5e5',
  'text-color': '#2d2d2d'
};
@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  res: any;
  constructor(private http: HttpClient , private GeneralInfoService:GeneralInfoService) { 
    this.GeneralInfoService.sharedMessage.subscribe(message => {
            
      // this.color = message
      this.toggleLight()
      // console.log("message"+message)
      
    })
  }
  toggleLight() {
    this.http.get<any>(`${environment.apiUrl}settings?all=yes`).subscribe(data => {
      var str = JSON.stringify(data.data)
      this.res = str.replace(/-/g, "_");

      this.res = JSON.parse(this.res)
      var color = this.res.color_dashboard.en
      this.setTheme({ 'background-color': color })
      // console.log("getGeneralInfo"+ color)


    })
  }
  // toggleLight() {
  //   this.setTheme(darkTheme);
  // }
  setTheme(theme: {}) {
    Object.keys(theme).forEach(k =>
      document.documentElement.style.setProperty(`--${k}`, theme[k])
    );
  }
}
