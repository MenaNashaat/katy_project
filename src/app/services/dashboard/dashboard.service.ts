import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }
  getStatisticsById(id): Observable<any> {
    if (id  != 'all'){
      return this.http.get<any>(`${environment.apiUrl}statistics?branch_id=${id}`);
    }
    else{
      return this.http.get<any>(`${environment.apiUrl}statistics`);
    }
   
  }
  getStatistics(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}statistics`);
  }
}
