import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SpecialRequestService {

  constructor(private http: HttpClient) {}
  getallRequests(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}specialrequest?page=${id}`);
  }
  getRequestById(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}specialrequest/${id}`);
  }
}
