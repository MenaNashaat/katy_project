import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class MedicalService {

  constructor(private http:HttpClient) { }
  getMedical(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}medical?limit=100`  );
  }
  createMedical(medical): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}medical`, medical  );
  }
  editMedical(medical , id): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}medical/${id}`, medical  );
  }
  deleteMedical(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}medical/${id}`  );
  }
  deleteAnswer(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}faq/question/answer/${id}`  );
  }
  deleteMeal(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}meal/${id}`  );
  }}
