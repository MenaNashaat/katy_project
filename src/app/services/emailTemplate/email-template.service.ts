import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class EmailTemplateService {

  constructor(private http:HttpClient) { }
  getAllMails(): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}emailTemplate`  );
  }
  
  createEmailForm(email): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}emailTemplate`, email );
  }
  getEmailByID( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}emailTemplate/${id}` );
  }
  editEmail( id , email): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}emailTemplate/${id}` , email );
  }
  deleteEmail( id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}emailTemplate/${id}` );
  }
  sendEmailForm(id , list): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}emailTemplate/${id}`, list );
  }
}
