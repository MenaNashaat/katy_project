import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../../models/index';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentAdminSubject: BehaviorSubject<User>;
  myItem :any
 public currentAdmin: Observable<User>;
 private httpOptions: any;

 constructor(private http: HttpClient) {
     this.currentAdminSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentAdmin')));
     this.currentAdmin = this.currentAdminSubject.asObservable();   
 }

 public get currentAdminValue(): User {
     return this.currentAdminSubject.value;
 }

 login(email: string, password: string) {
     return this.http.post<any>(`${environment.apiUrl}auth/login`, { email, password })
         .pipe(map(user => {
             console.log("user" +JSON.stringify(user))
             // login successful if there's a jwt token in the response
             if (user.data && user.data.access_token) {
                 // store user details and jwt token in local storage to keep user logged in between page refreshes
                 localStorage.setItem('currentAdmin', JSON.stringify(user));
                 this.currentAdminSubject.next(user);
             }
             return user;
         }));
 }

 forgotPassword(email): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}auth/forget`, email);
  }
 logout() {
     // remove user from local storage to log user out
     localStorage.removeItem('currentAdmin');
     this.currentAdminSubject.next(null);
 }
}
