import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject} from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ClassService {

  constructor(private http:HttpClient) { }
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
 language:any;
 changeMessage(message: string) {
   
    this.language = message
    this.messageSource.next(message)
  }
  getclass(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}class?level_id=${id}`  );
  }
  getlevelDetails(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}level/${id}`  );
  }
  createclass(className): Observable<any> {
    console.log("branchName" +JSON.stringify(className))
    return this.http.post<any>(`${environment.apiUrl}class?lang=${this.language}`, className );
  }
  editclass(editclass, id): Observable<any> {
    console.log("sssssssss"+ JSON.stringify(editclass))
    return this.http.put<any>(`${environment.apiUrl}class/${id}?lang=${this.language}` , editclass );
  }
  getclassById( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}class/${id}` );
  }
}
