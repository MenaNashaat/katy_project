import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }
  getEvent(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}event?limit=100`  );
  }
  createEvent(event): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}event` , event );
  }
  deleteEvent(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}event/${id}`  );
  }
  getclass(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}class/full?limit=100`  );
  }
}
