import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor(private http:HttpClient) { }
  getAllLists(): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}list`  );
  }
  getLanguage(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}language?limit=100`);
   }
  createList(list): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}list`, list );
  }
  getListByID(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}list/${id}` );
  }
  editList( id , list): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}list/${id}` , list );
  }
  deleteList( id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}list/${id}` );
  } 
  
  deleteUser( id , email): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}list/${id}/users` , email  );
  } 
  
  application(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}application?limit=1000`);
  }
  
  AllUsers(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}list/users`  );
  }
  addUserToList( id , email): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}list/${id}/users` , email );
  }
  createUser( user ): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}externalCustomer` , user );
  }
}
