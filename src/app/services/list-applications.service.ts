import { Injectable } from '@angular/core';
import { listAplications } from '../models';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListApplicationsService {
  private httpOptions: any;
  constructor(private http: HttpClient) {

  }
  private message = new BehaviorSubject('First Message');
  sharedMessage = this.message.asObservable();

  changeMessage(message: string) {
    this.message.next(message)
  }
  application(id): Observable<any> {
    console.log("id" + id);
    return this.http.get<any>(`${environment.apiUrl}application?page=${id}`);
  }
  applicationFilterName(id , name): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}application?name=${name}`);
  }
  addApplication(child, id): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}application/user/${id}`, child);
  }
  applicationFilter(id, branchID, levelID , ChildName): Observable<any> {
    console.log("mennnnnnnnnnnn id" + id + "levelID" + levelID + "branchID"   + branchID + "ChildName" + ChildName);
    if (branchID == 'all' ) {
      branchID= ''
    }
    if (levelID == 'all' || levelID == null ) {
      levelID= ''
    }
    return this.http.get<any>(`${environment.apiUrl}application?branch_id=${branchID}&level_name=${levelID}&name=${ChildName}`);

    // if (  levelID != '')
    // {
    //   alert("number")
    //   return this.http.get<any>(`${environment.apiUrl}application?branch_id=${branchID}&level_id=${levelID}&name=${ChildName}`);
    // }
    // else if ( levelName != '')
    // {
    //   return this.http.get<any>(`${environment.apiUrl}application?branch_id=${branchID}&level_name=${levelName}&name=${ChildName}`);
    // }
    // if (levelID == 'all') {
    //   console.log("idLEVel");
    //   return this.http.get<any>(`${environment.apiUrl}application?page=${id}`);
    // }
    // else if (ChildName != '' && ChildName != null) {
    //   console.log("ChildName all");
    //   return this.http.get<any>(`${environment.apiUrl}application?name=${ChildName}&page=${id}`);
    // }
    // else if (branchID == 'all' && levelID == 'all'  || branchID ==  'all'  && levelID == '') {
    //   console.log("branchID all");
    //   return this.http.get<any>(`${environment.apiUrl}application?page=${id}`);
    // }
    // else if (branchID == '' && levelID != 'all' && levelID != null|| branchID == 'all' && levelID != 'all' && levelID!= null) {
    //   console.log("branchID all ");
    //   return this.http.get<any>(`${environment.apiUrl}application?level_name=${levelID}&page=${id}`);
    // }
    // else if (levelID == null) {
    //   console.log("NooooidLEVel");
    //   return this.http.get<any>(`${environment.apiUrl}application?branch_id=${branchID}&page=${id}`);
    // }
    // else {
    //   console.log("NooooidLEVel");
    //   return this.http.get<any>(`${environment.apiUrl}application?branch_id=${branchID}&level_id=${levelID}&page=${id}`);

    // }

  }
  getRegisterById(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}application/${id}`);
  }
  Reject(id): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}application/${id}/action/reject`, this.httpOptions);
  }
  accept(id): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}application/${id}/action/accept`, this.httpOptions);
  }
 
  getLevel(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}level?branch_id=${id}`);
  }
  getLevels(): Observable<any> {
    if (localStorage.getItem('language') == 'ar') {

      return this.http.get<any>(`${environment.apiUrl}level/unique?lang=ar&limit=100`);

    }
    else {
      return this.http.get<any>(`${environment.apiUrl}level/unique?lang=en&limit=100`);

    }
  }
  getuniqueLevel(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}level/unique`);
  }
  deleteApplication(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}application/${id}`);
  }
  getNationality(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}nationality?limit=100`);
   }
   getLanguage(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}language?limit=100`);
   }
   editChild(id , childID , data): Observable<any> {

   return this.http.put<any>(`${environment.apiUrl}application/${id}/child/${childID}` ,data);
  }
// **********************
getapplicationInfo(): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}application`);
}


getLevel_en(): Observable<any> {

  return this.http.get<any>(`${environment.apiUrl}level/unique?lang=en&limit=100`);
}
getLevel_ar(): Observable<any> {

  return this.http.get<any>(`${environment.apiUrl}level/unique?lang=ar&limit=100`);
}
getBranch(): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}branch?limit=100`);
}
getBranchByLevel(level): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}branch?level=${level}&limit=100`);
}
getLevelByLevel_branch(levelName, branchId): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}level/name/${levelName}/branch/${branchId}`);
}
getLevelBy_branchID(branchId): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}level?branch_id=${branchId}`);
}
getMedical(): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}medical?limit=100`);
}

getHour(): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}afterschool`);
}
getGuardian(): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}guardian`);
}

addChild(child, id): Observable<any> {
  return this.http.post<any>(`${environment.apiUrl}application/${id}/child`, child);
}

getWinterCampMonths(): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}winterCampMonths`);
}
getSummerCampMonths(): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}summerCampMonths`);
}
getSettings(): Observable<any> {
  return this.http.get<any>(`${environment.apiUrl}settings`);
}

getParent(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}user/list?limit=1000&sort=desc&role=Parent`  );
  }
  cancelApplication( id): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}application/${id}`, {});
  }


}
