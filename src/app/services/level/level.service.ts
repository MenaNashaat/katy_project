import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LevelService {

  constructor(private http:HttpClient) { }
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
 language:any;
 changeMessage(message: string) {
   
    this.language = message
    this.messageSource.next(message)
  }
  getLevel(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}level?branch_id=${id}&lang=${this.language}`  );
  }
  createLevel(LevelName): Observable<any> {
    console.log("branchName" +JSON.stringify(LevelName))
    return this.http.post<any>(`${environment.apiUrl}level`, LevelName );
  }
  editLevel(editLevel, id): Observable<any> {
    console.log("sssssssss"+ JSON.stringify(editLevel))
    return this.http.put<any>(`${environment.apiUrl}level/${id}?lang=${this.language}` , editLevel );
  }
  getLevelById( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}level/${id}` );
  }
}
