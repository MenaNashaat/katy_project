import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class EvaluationService {

  constructor(private http:HttpClient) { }
  
  getAllEvaluation(id , levelId): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}evaluation?level_id=${levelId}&page=${id}`  );
  }
  getformDetails(formId): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}evaluation?form_id=${formId}&limit=100`  );
  }
  evaluationForm(): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}evaluationForm?limit=100`  );
  }
  getEvaluationFilter( branchID , levelID): Observable<any> { 
   
    if (levelID == 'all' )
    {
      console.log("idLEVel" );
      return this.http.get<any>(`${environment.apiUrl}evaluationForm?limit=100&branch_id=${branchID}`);
      
    }
    else if (branchID ==   'all' )
    {
      console.log("branchID all" );
      return this.http.get<any>(`${environment.apiUrl}evaluationForm?limit=100`);
    }
    else if (levelID ==  null )
    {
      console.log("NooooidLEVel" );
      return this.http.get<any>(`${environment.apiUrl}evaluationForm?limit=100&branch_id=${branchID}`);
    }
    else 
    {
      console.log("NooooidLEVel" );
      return this.http.get<any>(`${environment.apiUrl}evaluationForm?limit=100&level_id=${levelID}`);

    }





  }
  getAllEvaluationID(id): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}evaluation/${id}`  );
  }
  createUser(user): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}evaluation`, user );
  }
  createEvaForm(name): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}evaluationForm`, name );
  }
  getFormByID( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}evaluationForm/${id}` );
  }
  editForm( id , name): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}evaluationForm/${id}` , name );
  }
  editEvaluation(evaluation, id): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}evaluation/${id}` , evaluation );
  }
  deleteEvaluation( id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}evaluationForm/${id}` );
  }
  deleteQuestion( id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}evaluation/${id}` );
  }
  levels(): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}level`  );
  }
  levelsByID(id): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}level?branch_id=${id}`  );
  }
  branch(): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}branch?limit=100`  );
  }
  assign(formID , levelID): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}evaluationForm/assign/${formID}`, levelID );
  }
  
  
  getLevelByBranch(branchId ): Observable<any> { 
    if (branchId == 'all'){
      return this.http.get<any>(`${environment.apiUrl}level?limit=100`  );
    }
    else
    {
      return this.http.get<any>(`${environment.apiUrl}level?branch_id=${branchId}&limit=100`  );
    }
   
  }


  // complete evaluaion
  getAllApprovalEvaluation(id): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}evaluation/completed?approved=1&page=${id}`  );
  }
  
  getAllUnApprovalEvaluation(id): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}evaluation/completed?approved=0&page=${id}`  );
  }
  approve(id): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}evaluation/${id}/approve`, {} );
  }
  
}

