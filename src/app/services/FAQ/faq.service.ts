import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class FAQService {

  constructor(private http:HttpClient) { }
  getFAQ(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}faq?limit=100`  );
  }
  createFAQ(faq): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}faq/question`, faq  );
  }
  createAnswer(answer , id): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}faq/question/${id}/answer`, answer  );
  }
  deleteQuestion(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}faq/question/${id}`  );
  }
  deleteAnswer(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}faq/question/answer/${id}`  );
  }
  deleteMeal(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}meal/${id}`  );
  }
}
