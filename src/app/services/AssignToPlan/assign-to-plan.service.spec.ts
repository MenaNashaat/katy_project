import { TestBed } from '@angular/core/testing';

import { AssignToPlanService } from './assign-to-plan.service';

describe('AssignToPlanService', () => {
  let service: AssignToPlanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssignToPlanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
