import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AssignToPlanService {

  constructor(private http:HttpClient) { }
  getPlans(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}payment?limit=100`  );
  }
  assignPlan(plan): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}payment/assign`, plan  );
  }
  customBill(bill): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}bill`, bill  );
  }
  getStudentId(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}kids?user_id=${id}`,   );
  }
 
}
