import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  language:any;
  constructor() { 
    
  }
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
 
  changeMessage(message: string) {
   
    this.language = message
    this.messageSource.next(message)
  }
  
  
}
