import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class VisitService {  

  constructor(private http:HttpClient) { }
  getVisits(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}visit?page=${id}`  );
  }
  calendar(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}visit/calendar`  );
  }
  addTime(timeAvailable): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}visit/calendar` , timeAvailable );
  }
   
}
