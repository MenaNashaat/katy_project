import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class GeneralInfoService {

  constructor(private http: HttpClient) { }
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  language: any;
  changeMessage(message: string) {

    this.language = message
    this.messageSource.next(message)
  }
  // language = localStorage.getItem('language');
  private message = new BehaviorSubject('First Message');
  sharedMessage = this.message.asObservable();

  nextMessage(message: string) {
    this.message.next(message)
  }

  settings(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}settings?all=yes`);
  }
  addSettings(setting): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}settings`, setting);
  }
  addSettingInvoice(setting): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}settings/invoice`, setting);
  }
}
