import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BranchService {

  constructor(private http:HttpClient) {
    
   }
   private messageSource = new BehaviorSubject('default message');
   currentMessage = this.messageSource.asObservable();
  language:any;
  changeMessage(message: string) {
    
     this.language = message
     this.messageSource.next(message)
   }
  getBranch(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}branch?limit=100`  );
  }
  postBranch(branchName): Observable<any> {
    console.log("branchName" +JSON.stringify(branchName))
    return this.http.post<any>(`${environment.apiUrl}branch?lang=${this.language}`, branchName);
  }
  editBranch(editBranch, id): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}branch/${id}` , editBranch );
  }
  getBranchById( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}branch/${id}` );
  }
}
