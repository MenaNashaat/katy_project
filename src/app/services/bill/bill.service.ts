import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class BillService {

  constructor(private http:HttpClient) { }
  getBill( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}bill/application/${id}` );
  }
  paid(id , paid): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}bill/${id}/pay`, paid );
  }
  getInvoice( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}bill/${id}` );
  }
  getInvoiceItem( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}payment/bill/${id}` );
  }
  getSettings(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}settings?all=yes` );
  }
  Cancel(id , amount): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}bill/${id}/cancel`, amount );
  }
  
}
