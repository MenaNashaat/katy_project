import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ManagementService {

  constructor(private http:HttpClient) { }
  getAdmin(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}user/list?page=${id}&sort=desc&role=Admin`  );
  }
  getTeacher(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}teacher?page=${id}`  );
  }
  getParent(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}user/list?page=${id}&sort=desc&role=Parent`  );
  }
  getUserById( id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}user/?user_id=${id}` );
  }
  createUser(user): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}user`, user );
  }
  editUser(user, id): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}user/${id}` , user );
  }
  deleteUser( id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}user/${id}` );
  }
  getClass( ): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}class/full?limit=100` );
  }
  assign(teacher): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}class/assign/teacher`, teacher );
  }
  addUser(user): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}auth/register`, user );
  }
}
