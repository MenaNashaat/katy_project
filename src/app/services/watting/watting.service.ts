import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class WattingService {

  constructor(private http:HttpClient) { }
  
  getAllWatting(id): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}waiting?page=${id}`  );
  }
  getAllWattingID(id): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}waiting/${id}`  );
  }
  createUser(user): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}waiting`, user );
  }
  addToWaitingList(id ): Observable<any> { 
    return this.http.put<any>(`${environment.apiUrl}application/${id}/waiting`, {}  );
  }
  editWatting(Watting, id): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}waiting/${id}` , Watting );
  }
  deleteWatting( id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}waiting/${id}` );
  }
  allLevel(id): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}level?branch_id=${id}`  );
  }
  allBranch(): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}branch`  );
  }
  levelWithBranch(): Observable<any> { 
    return this.http.get<any>(`${environment.apiUrl}level/all`  );
  }
}
