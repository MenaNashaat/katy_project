import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class MealService {

  constructor(private http:HttpClient) { }
  getMeal(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}meal?limit=100`  );
  }
  createMeal(meal): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}meal`, meal  );
  }
  deleteMeal(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}meal/${id}`  );
  }
}
 