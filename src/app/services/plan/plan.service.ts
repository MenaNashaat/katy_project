import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable , BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor(private http:HttpClient) { }
  getPlans(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}payment?limit=100`  );
  }
  createPlan(plan): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}payment`, plan  );
  }
  editPlan(plan , id): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}payment/${id}`, plan  );
  }
  deletePlan(id): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}payment/${id}`  );
  }
  settings(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}settings`  );
  }
  editVAT(vat): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}settings/vat`, vat  );
  }
  
}
