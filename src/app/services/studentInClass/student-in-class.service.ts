import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class StudentInClassService {

  constructor(private http:HttpClient) { }
  getStudent(id , paginationID): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student?class_id=${id}&page=${paginationID}`);
  }
  getClassDetails(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}class/${id}`);
  }
  getLevel(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}level?branch_id=${id}`);
  }
  getClass(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}class?level_id=${id}`);
  }
  upgrade(data): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}student/upgrade`, data);
  }
  unassign(data): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}class/unassign/teacher`, data);
  }
 
  
}
