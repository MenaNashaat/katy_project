import { TestBed } from '@angular/core/testing';

import { StudentInClassService } from './student-in-class.service';

describe('StudentInClassService', () => {
  let service: StudentInClassService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StudentInClassService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
