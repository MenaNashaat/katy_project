import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) {

  }
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  language: any;
  changeMessage(message: string) {
    this.language = message
    this.messageSource.next(message)
  }
  private message = new BehaviorSubject('First Message');
  sharedMessage = this.message.asObservable();

  nextMessage(message: string) {
    this.message.next(message)
  }
  getStudent(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&assign=yes&status=active&sort=asc`);
  }
  getStudentDetails(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student/${id}`);
  }
  getStudentPending(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&status=Pending`);
  }
  getUnassigned(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&assign=no`);
  }
  getextraHour(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&extra=yes`);
  }
  filterActiveStudent(id , name){
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&name=${name}&assign=yes&status=active&sort=asc`);
  }
 
  getStudentWinter(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&winter_camp=1`);
  }
  getStudentSummer(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&summer_camp=1`);
  }
  filterDeactivatedStudent(id , name){
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&name=${name}&status=Pending`);
  }
  filterUnassignedStudent(id , name){
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&assign=no&name=${name}`);
  }
  filterExtraStudent(id , name){
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&extra=yes&name=${name}`);
  }
  filterWinterStudent(id , name){
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&winter_camp=1&name=${name}`);
  }
  filterSummerStudent(id , name){
    return this.http.get<any>(`${environment.apiUrl}student?page=${id}&summer_camp=1&name=${name}`);
  }

  getLevel(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}level?limit=100`);
  }
  getStudentById(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}student/${id}`);
  }
  getClass(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}class?level_id=${id}`);
  }
  getClassByBranch(id): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}class?branch_id=${id}`);
  }
  assignStudent(student): Observable<any> {
    console.log("branchName" + JSON.stringify(student))
    return this.http.post<any>(`${environment.apiUrl}student/assign?lang=${this.language}`, student);
  }
  studentStatus(id, student): Observable<any> {
    return this.http.put<any>(`${environment.apiUrl}student/${id}`, student);
  }
  upgrade(data): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}student/upgrade`, data);
  }
  winterCampMonths(data): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}winterCampMonths`, data);
  }
  getWinterCampMonths(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}winterCampMonths`);
  }
  summerwintercamp(data): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}settings/summerwintercamp`, data);
  }
  summercampToggle(data): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}settings/summerCamp`, data);
  }
  wintercampToggle(data): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}settings/winterCamp`, data);
  }
  getSetting(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}settings`);
  }
  summerCampMonths(data): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}summerCampMonths`, data);
  }
  getSummerCampMonths(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}summerCampMonths`);
  }

}
