import { TestBed } from '@angular/core/testing';

import { ListApplicationsService } from './list-applications.service';

describe('ListApplicationsService', () => {
  let service: ListApplicationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListApplicationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
