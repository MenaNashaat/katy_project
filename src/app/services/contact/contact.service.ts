import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http:HttpClient) { 
  
  }
  contact(id): Observable<any> {
   return this.http.get<any>(`${environment.apiUrl}contact?page=${id}`);
 }
 getContactById(id): Observable<any> {
  
  return this.http.get<any>(`${environment.apiUrl}contact/${id}`);
}
}
