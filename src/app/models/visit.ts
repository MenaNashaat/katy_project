export class Visit {
    name: string;
    date:string;
    email:string;
    phone:number;
    from:string;
    to:string;
    available:any;
    branch:any;
}