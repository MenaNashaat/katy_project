export class Plan {
    name: string;
    amount: number;
    discount_value: number;
    discount_percent: number;
    repetation: number;
    duration: number;
    gap: number;  
}