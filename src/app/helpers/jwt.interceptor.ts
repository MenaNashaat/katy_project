import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let currentAdmin = this.authenticationService.currentAdminValue;
        let subDomain;
        switch((window.location.host).split('.')[0]){
        
            case 'admin':
                subDomain = (window.location.host).split('.')[1];
                break;
            case 'localhost:4200':
            case 'localhost:4401':
            case 'dev-admin':
            case 'dev':
                subDomain = 'staging';
                break;
            case 'www':
                switch((window.location.host).split('.')[0]){
                    case 'admin':
                        subDomain = (window.location.host).split('.')[1];
                        break;
                    case 'dev-admin':
                    case 'dev':
                        subDomain = 'staging';
                        break;
                    case 'www':
                            subDomain = (window.location.host).split('.')[1];
                            break;    
                    default:
                        subDomain = (window.location.host).split('.')[0];
                        break;
                }
                break;
            default:
                subDomain = (window.location.host).split('.')[0];
                break;
         }
         console.log('subDomain', subDomain);

        if (currentAdmin && currentAdmin.data.access_token) {
            request = request.clone({
                setHeaders: { 
                    //Authorization: `Bearer  ${"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMWJhOTk0NGUyYTY5MTE1NTA0YzEwZmQ0ODRjODk5MjlkMTUzOGIxOGE4M2EyMjkxZGQ2YzdmYTEwY2Y0MjY2M2QwYzg3NTUxYjcwYjk1NTQiLCJpYXQiOjE1OTQzOTQ3MzMsIm5iZiI6MTU5NDM5NDczMywiZXhwIjoxNjI1OTMwNzMzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.lWw3y-E6WKS--BnHsoZFmlvkIjgD_l5pXeM1IrcVdp26JX9jTX6eM3kjbdoirQlZKTJFNUb52XTxbSFgLCihn_IXMcjtCKJr20ClFII8uGrzzNKhJA_a-GF6osr5hUH1jm5R-iDSEAQxe0AsE_u4dEtAJ6-m8QYYcGwf2VWu-CxIb6K7memdH4Q1WerXJZZtcCZTsejNbiWqlotk2GAGAkwV9RYs2I45WJ0vf_KKVY-KZfpVynLv8VjAWblFktBHlrCEUyh-XxAhfhOuPCi5tb6pxBFdln1Dj6WsCQfXTxrO_FysNYGaYiw5Kez0Mxh97FN35J5HuIB7b4P77epb1WUz4UlRwcjfjdZ5EyJoL06MbuTyH7oUb_FhB9a5f50Cm0dP6eLohLfzsZnxEQJLYuYr2wzpBCcUH8Y-W9oLQrbDKX-JmwXdIXvNPNJwjqQnLNROwhsknTAYMQvJY6_7aJqLUhmqArEQZGVj7zjXjCNWivbWApoEFSG9_mIMoBNUvjI6Q6IUke3Zweyjml6kjPlUQuiDFkIX4VFFm509kdB0JQHLM0l0LINiVeIrQZDNxdfa4rcf94EXQqVP6vEyijEyqKBShtL0nfUqr3sOOWsBV1QzNAB1DM-bUAbkjr60PkgqJjlgCeyqPH9VXRZ6D_qRyNkXuGXvneClMYsBHzA"}`
                    Authorization: `Bearer ${currentAdmin.data.access_token}`,
                    subDomain: subDomain
                    //subDomain: kids-tent
                }
            });
        }
        else{
            request = request.clone({
                setHeaders: { 
                    subDomain: subDomain
                }
            });
        }
        return next.handle(request);
    }
}
