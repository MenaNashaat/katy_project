import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
// import { ROUTES } from '../../sidebar/sidebar.component';
import { Router } from '@angular/router';
import { Location} from '@angular/common';
import { AuthenticationService , StudentService, BranchService, LevelService , ClassService }  from '../../services';
import { TranslateService } from "@ngx-translate/core";
import { DOCUMENT } from "@angular/common";
import { Inject } from "@angular/core";
@Component({
    moduleId: module.id,
    selector: 'navbar-cmp',
    templateUrl: 'navbar.component.html'
})

export class NavbarComponent implements OnInit{
    private listTitles: any[];
    location: Location;
    private nativeElement: Node;
    private toggleButton;
    private sidebarVisible: boolean;

    public isCollapsed = true;
    @ViewChild("navbar-cmp", {static: false}) button;

    constructor( @Inject(DOCUMENT) private document: Document ,private translateService: TranslateService,
    private authenticationService:AuthenticationService, location:Location, 
    private renderer : Renderer2, private element : ElementRef, 
    private router: Router , public StudentService:StudentService,
    public LevelService:LevelService,
    public ClassService:ClassService,
    public BranchService : BranchService) {
        this.location = location;
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
        translateService.addLangs(['en','ar']);
      //translateService.setDefaultLang('en');
      if(localStorage.getItem('language')){
        translateService.setDefaultLang(localStorage.getItem('language'));
        translateService.use(localStorage.getItem('language'));
      }else {
        translateService.setDefaultLang('en');
        translateService.use('en');
          localStorage.setItem("language","en");
      }
      if (localStorage.getItem('language') == 'ar' )
      {
        this.document.body.classList.add('ar');
        // this.UserService.changeMessage(localStorage.getItem('language'))
      }
    }
    useLanguage(language) { 
      this.StudentService.nextMessage(language)
      this.StudentService.changeMessage(language);
      this.BranchService.changeMessage(language);
      this.LevelService.changeMessage(language);
      this.ClassService.changeMessage(language);
      this.translateService.use(language).toPromise();
      let key = 'Item 1';
       
      if(language === "ar"){
          localStorage.setItem("language", 'ar');
          this.document.body.classList.add('ar');
        }
        else {
          localStorage.setItem("language", 'en');
          this.document.body.classList.remove('ar');
          // this.document.body.classList.remove();
          // this.document.body.classList.add('en');
        }
   }

    ngOnInit(){
        // this.listTitles = ROUTES.filter(listTitle => listTitle);
        var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        this.router.events.subscribe((event) => {
          this.sidebarClose();
       });
    }
    // getTitle(){
    //   var titlee = this.location.prepareExternalUrl(this.location.path());
    //   if(titlee.charAt(0) === '#'){
    //       titlee = titlee.slice( 1 );
    //   }
    //   for(var item = 0; item < this.listTitles.length; item++){
    //       if(this.listTitles[item].path === titlee){
    //           return this.listTitles[item].title;
    //       }
    //   }
    //   return 'Dashboard';
    // }
    sidebarToggle() {
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
      }
      sidebarOpen() {
          const toggleButton = this.toggleButton;
          const html = document.getElementsByTagName('html')[0];
          const mainPanel =  <HTMLElement>document.getElementsByClassName('main-panel')[0];
          setTimeout(function(){
              toggleButton.classList.add('toggled');
          }, 500);

          html.classList.add('nav-open');
          if (window.innerWidth < 991) {
            mainPanel.style.position = 'fixed';
          }
          this.sidebarVisible = true;
      };
      sidebarClose() {
          const html = document.getElementsByTagName('html')[0];
          const mainPanel =  <HTMLElement>document.getElementsByClassName('main-panel')[0];
          if (window.innerWidth < 991) {
            setTimeout(function(){
              mainPanel.style.position = '';
            }, 500);
          }
          this.toggleButton.classList.remove('toggled');
          this.sidebarVisible = false;
          html.classList.remove('nav-open');
      };
      collapse(){
        this.isCollapsed = !this.isCollapsed;
        const navbar = document.getElementsByTagName('nav')[0];
        console.log(navbar);
        if (!this.isCollapsed) {
          navbar.classList.remove('navbar-transparent');
          navbar.classList.add('bg-white');
        }else{
          navbar.classList.add('navbar-transparent');
          navbar.classList.remove('bg-white');
        }

      }
      logout() {
        this.authenticationService.logout();
        this.router.navigate(['/Login']);
    }

}
