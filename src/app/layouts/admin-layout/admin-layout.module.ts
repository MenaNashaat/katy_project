import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { JwtInterceptor } from '../../helpers/jwt.interceptor';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { DashboardComponent }       from '../../pages/dashboard/dashboard.component';
import { TableComponent }           from '../../pages/table/table.component';
import { RegistrationComponent } from '../../pages/registration/registration.component';
import { MaterialModule } from '../../material.module';
import {ListApplicationsService} from '../../services/list-applications.service'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RegistrationDetailsComponent } from '../../pages/registration-details/registration-details.component';
import {NgxPrintModule} from 'ngx-print';
import { ReactiveFormsModule} from '@angular/forms';
import { TranslateLoader, TranslateModule,} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient} from '@angular/common/http';
// AoT requires an exported function for factories

export function childLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "./assets/i18n/", ".json");
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
    NgbModule,
    NgxPrintModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: childLoaderFactory,
        deps: [HttpClient]
      },
      extend: true,
    }),
  ],
  declarations: [
    DashboardComponent,
    TableComponent,
    RegistrationComponent,
    RegistrationDetailsComponent
   
  ],
  providers: [ListApplicationsService, { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
})

export class AdminLayoutModule {}
