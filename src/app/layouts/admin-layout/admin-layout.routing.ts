import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { TableComponent } from '../../pages/table/table.component';
import { RegistrationComponent } from '../../pages/registration/registration.component';
import { RegistrationDetailsComponent } from '../../pages/registration-details/registration-details.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'table',          component: TableComponent },
    { path: 'registration',          component: RegistrationComponent },
    { path: 'application/:id',          component: RegistrationDetailsComponent },
    { path: 'Enquires', loadChildren: () => import('../../pages/contact/contact.module').then(m => m.ContactModule) },
    { path: 'branch', loadChildren: () => import('../../pages/branch/branch.module').then(m => m.BranchModule) },
    { path: 'level/:id', loadChildren: () => import('../../pages/level/level.module').then(m => m.LevelModule) },   
    { path: 'student_details/:id', loadChildren: () => import('../../pages/student-details/student-details.module').then(m => m.StudentDetailsModule) },   
    { path: 'class/:id', loadChildren: () => import('../../pages/class/class.module').then(m => m.ClassModule) },   
    { path: 'students/:id', loadChildren: () => import('../../pages/students-in-class/students-in-class.module').then(m => m.StudentsInClassModule) },   
    { path: 'students', loadChildren: () => import('../../pages/student/student.module').then(m => m.StudentModule) }, 
    { path: 'meals', loadChildren: () => import('../../pages/meal/meal.module').then(m => m.MealModule) },     
    { path: 'Website_Settings', loadChildren: () => import('../../pages/general-info/general-info.module').then(m => m.GeneralInfoModule) },  
    { path: 'visit', loadChildren: () => import('../../pages/visit/visit.module').then(m => m.VisitModule) }, 
    { path: 'avialable_time', loadChildren: () => import('../../pages/vist-available-time/vist-available-time.module').then(m => m.VistAvailableTimeModule) },  
    { path: 'events', loadChildren: () => import('../../pages/event/event.module').then(m => m.EventModule) }, 
    { path: 'bill/:id', loadChildren: () => import('../../pages/bill/bill.module').then(m => m.BillModule) }, 
    { path: 'Invoice/:id', loadChildren: () => import('../../pages/invoice/invoice.module').then(m => m.InvoiceModule) },   
    { path: 'create-Invoice/:id', loadChildren: () => import('../../pages/create-invoice/create-invoice.module').then(m => m.CreateInvoiceModule) },   
    { path: 'assign-to-plan/:id', loadChildren: () => import('../../pages/assign-to-plan/assign-to-plan.module').then(m => m.AssignToPlanModule) },   
    { path: 'Payment_Plans', loadChildren: () => import('../../pages/plan/plan.module').then(m => m.PlanModule) },  
    { path: 'FAQ', loadChildren: () => import('../../pages/faq/faq.module').then(m => m.FAQModule) },
    { path: 'formDetails/:id', loadChildren: () => import('../../pages/evaluation-level/evaluation-level.module').then(m => m.EvaluationLevelModule) }, 
    { path: 'evaluation/:id', loadChildren: () => import('../../pages/evaluation/evaluation.module').then(m => m.EvaluationModule) }, 
    { path: 'Student_Evaluation', loadChildren: () => import('../../pages/evaluation-form/evaluation-form.module').then(m => m.EvaluationFormModule) }, 
    { path: 'EmailTemplate', loadChildren: () => import('../../pages/email/email.module').then(m => m.EmailModule) }, 
    { path: 'Tags', loadChildren: () => import('../../pages/list/list.module').then(m => m.ListModule) }, 
    { path: 'All_Customer', loadChildren: () => import('../../pages/alllists/alllists.module').then(m => m.AlllistsModule) }, 
    { path: 'ListDetails/:id', loadChildren: () => import('../../pages/listdetails/listdetails.module').then(m => m.ListdetailsModule) }, 
    { path: 'Edit_Application/:id/Child/:id', loadChildren: () => import('../../pages/edit-application/edit-application.module').then(m => m.EditApplicationModule) }, 
    { path: 'waiting_list', loadChildren: () => import('../../pages/watting/watting.module').then(m => m.WattingModule) }, 
    { path: 'Medical', loadChildren: () => import('../../pages/medical/medical.module').then(m => m.MedicalModule) }, 
    { path: 'UserManagement', loadChildren: () => import('../../pages/user-management/user-management.module').then(m => m.UserManagementModule) },  
    { path: 'special-request', loadChildren: () => import('../../pages/special-request/special-request.module').then(m => m.SpecialRequestModule) },  
    { path: 'Theme', loadChildren: () => import('../../pages/theme/theme-routing.module').then(m => m.ThemeRoutingModule) },
    { path: 'Payment', loadChildren: () => import('../../pages/payment/payment.module').then(m => m.PaymentModule) },  
    { path: 'Add_Application', loadChildren: () => import('../../pages/add-aplication/add-aplication.module').then(m => m.AddAplicationModule) },  
    { path: 'Billing_Details', loadChildren: () => import('../../pages/billing-details/billing-details.module').then(m => m.BillingDetailsModule) },  
    { path: 'Completed_Evaluation', loadChildren: () => import('../../pages/completed-evaluation/completed-evaluation.module').then(m => m.CompletedEvaluationModule) },  
    { path: 'terms', loadChildren: () => import('../../pages/terms/terms.module').then(m => m.TermsModule) },  

    { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
];
    